#pragma once

#include "AK/SoundEngine/Common/IAkPlugin.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sstream>
#include <stdio.h>
#include <math.h>

#include <string>

#include "vsengine.h"
#include "libresample.h"

#ifdef NO_THREAD_MUTEX
#else
#include <thread>
#include <mutex>
extern std::mutex global_mutex_vsengine_prepare_work_area;
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <Windows.h>
#endif 

#define USE_STATE_MAP
#define USE_BACCH

#define USE_EXTERNAL_MEMORY

//#define LOGGING_ON						//Comment out to hide log messages

#define ENABLE_RTPC						//Enable to support float to bool
#ifdef ENABLE_RTPC
#define RTPC_BOOL AkReal32
#else
#define RTPC_BOOL bool
#endif

#define uint32 unsigned int
const double	_M_PI =						3.14159265358979323846;
#define Deg2Rad(deg)				_M_PI / 180.0 * (deg)	

extern AK::IAkMixerPluginContext *	global_mixer_plugin_context;


enum eBinauralPanningMode {
	COMMON_LEFT_RIGHT,
	COMMON_DYNAMIC_PRODUCT,
	COMMON_DYNAMIC_AVG,
	FRONT_REAR_NO_PAN,
	FRONT_REAR_PAN_SIDES,
};

enum eRenderMode
{
	FAST_SPAT,
	REGULAR
};


#define MAX_ROOM_SIZE_M 100
#define CLAMP(x, high, low) ((x) > (high) ? (high) : ((x) < (low) ? (low) : (x) ) )
#define M_MAX(a, b) ((a) > (b) ? (a) : (b))
#define M_MIN(a, b) ((a) < (b) ? (a) : (b))

//Logging
#			define LOG_TAG	"Wwise-RealSpace3D"
#       if defined(__ANDROID__)
// Android
#           include <android/log.h>
#           define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG,__VA_ARGS__)
#ifdef LOGGING_ON
#           define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOG_TAG,__VA_ARGS__)
#else
#           define LOGD(...) ;
#endif
#           define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOG_TAG,__VA_ARGS__)
#           define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOG_TAG,__VA_ARGS__)
#           define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOG_TAG,__VA_ARGS__)
#           define LOGSIMPLE(...)
#       else
// Not Android
#           include <stdio.h>
#           define LOGV(...) printf("  ");printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#ifdef LOGGING_ON
#if defined(_WIN32) || defined(_WIN64)
#           define LOGD(...) MessageBoxA(NULL, __VA_ARGS__, "DEBUG", MB_OK);
#else
#           define LOGD(...) printf("  ");printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#endif
#else
#           define LOGD(...) ;
#endif
#           define LOGI(...) printf("  ");printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#           define LOGW(...) printf("  * Warning: "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);

#if defined(_WIN32) || defined(_WIN64)
#			define LOGE(...) MessageBoxA(NULL, __VA_ARGS__, "DEBUG", MB_OK);
#else
#           define LOGE(...) printf("  *** Error:  ");printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#endif
#           define LOGSIMPLE(...) printf(" ");printf(__VA_ARGS__);
#       endif // ANDROID

#			define LOGM(MSG) if(global_mixer_plugin_context && global_mixer_plugin_context->CanPostMonitorData() ){global_mixer_plugin_context->PostMonitorMessage(MSG, AK::Monitor::ErrorLevel_Message); }

#ifdef __APPLE__
    #define SSTR( x ) std::to_string((x))
#else
    #define SSTR( x ) static_cast< std::ostringstream & >(( std::ostringstream() << std::dec << (x) ) ).str()
#endif

int vs_call_check(int result, const char * msg);	//writes to log if result != 0



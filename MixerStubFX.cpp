/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerStub.cpp
//
// Mixer Plugin stub implementation.
// 
//
// Copyright 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#include "MixerStubFX.h"
#include "MixerStubFXAttachmentParams.h"
#include <AK/Tools/Common/AkAssert.h>
#include <AK/AkWwiseSDKVersion.h>

// Plugin mechanism. Dynamic create function whose address must be registered to the FX manager.
AK::IAkPlugin* CreateMixerStubFX( AK::IAkPluginMemAlloc * in_pAllocator )
{
	if(in_pAllocator){
		LOGD("AK::IAkPlugin* CreateMixerStubFX: SUCCESS, in_pAllocator");
		global_plugin_mem_alloc = in_pAllocator;
	}else{
		LOGD("AK::IAkPlugin* CreateMixerStubFX: FAILURE, in_pAllocator is NULL");
	}

	AKASSERT( in_pAllocator != NULL );
	return AK_PLUGIN_NEW( in_pAllocator, MixerStubFX() );
}

// Creation function
AK::IAkPluginParam * CreateMixerStubFXAttachmentParams(AK::IAkPluginMemAlloc * in_pAllocator)
{
	AKASSERT(in_pAllocator != NULL);
	return AK_PLUGIN_NEW(in_pAllocator, MixerStubFXAttachmentParams());
}

// Creation function
AK::IAkPluginParam * CreateMixerStubFXParams(AK::IAkPluginMemAlloc * in_pAllocator)
{
	AKASSERT(in_pAllocator != NULL);
	return AK_PLUGIN_NEW(in_pAllocator, MixerStubFXParams());
}

AK_IMPLEMENT_PLUGIN_FACTORY(MixerStubFX, AkPluginTypeMixer, AKCOMPANYID_AUDIOKINETIC, AKEFFECTID_MIXERSTUB)
AK::PluginRegistration MixerStubFXAttachmentRegistration(AkPluginTypeEffect, AKCOMPANYID_AUDIOKINETIC, AKEFFECTID_MIXERSTUB_ATTACHMENT, NULL, CreateMixerStubFXAttachmentParams);


MixerStubFX::MixerStubFX() : MixerBase(){
	RS3D_controller = NULL;
	input_buffer_scratch = NULL;
	left_buffer_scratch = NULL;
	right_buffer_scratch = NULL;
	LOGD("MixerStubFX::MixerStubFX() SUCCESS");
}


AKRESULT MixerStubFX::Init(
	AK::IAkPluginMemAlloc *		in_pAllocator,				///< Interface to memory allocator to be used by the effect
	AK::IAkMixerPluginContext *	in_pMixerPluginContext,		///< Interface to mixer plug-in's context
	AK::IAkPluginParam *		in_pParams,					///< Interface to plug-in parameters
	AkAudioFormat &				in_rFormat					///< Audio data format of the input/output signal.
	)
{
	// Set parameters.
	m_pSharedParams = static_cast<MixerStubFXParams*>(in_pParams);
	m_pSharedParams->GetParams(m_fxInfo.params);
	m_fxInfo.prevParams = m_fxInfo.params;
	m_pSharedParams->m_paramChangeHandler.ResetAllParamChanges();

	// Init base class.
	AKRESULT baseSuccess = MixerBase::Init(in_pAllocator, in_pMixerPluginContext, in_pParams, in_rFormat);

	mixer_bus_ID = in_pMixerPluginContext->GetBusID();

	if (baseSuccess == AK_Success) {
		//Update RealSpace3D allocator
		if (in_pAllocator){
			global_plugin_mem_alloc = in_pAllocator;
		}else {
			LOGD("MixerStubFX::Init FAILURE, in_pAllocator is NULL");
		}

		if(!global_plugin_mem_alloc){
			LOGD("MixerStubFX::Init FAILURE, global_plugin_mem_alloc is still NULL");
		}

		//Launch RealSpace3D controller
		if (!RS3D_controller) {
			int num_samples_per_buffer = in_pMixerPluginContext->GlobalContext()->GetMaxBufferLength();
			LOGD(std::string("MixerStubFX::Init num_samples_per_buffer " + SSTR(num_samples_per_buffer)).c_str());

			input_buffer_scratch = (AkReal32*)RS3D_ALLOC(num_samples_per_buffer * sizeof(AkReal32));
			left_buffer_scratch = (AkReal32*)RS3D_ALLOC(num_samples_per_buffer * sizeof(AkReal32));
			right_buffer_scratch = (AkReal32*)RS3D_ALLOC(num_samples_per_buffer * sizeof(AkReal32));

			RS3D_controller = RS3D_NEW(RS3DController(in_pMixerPluginContext, m_fxInfo.params, in_rFormat));
		}
	}
	return baseSuccess;
}

void MixerStubFX::OnFrameEnd(
	AkAudioBuffer *				io_pMixBuffer,		///< Output audio buffer data structure. Stored across calls to ConsumeInput().
	AK::IAkMetering *			in_pMetering		///< Interface for retrieving metering data computed on io_pMixBuffer. 
	)
{
	MixerBase::OnFrameEnd(io_pMixBuffer, in_pMetering);

	// End of frame: Prepare parameters for next frame.
	m_fxInfo.prevParams = m_fxInfo.params;
	m_pSharedParams->m_paramChangeHandler.ResetAllParamChanges();
}

bool MixerStubFX::IsCustomProcessingBypassed(
	AK::IAkMixerInputContext *	in_pInputContext	///< Context for this input. Carries non-audio data.
	)
{
	// Get attachment properties:
	_MixerStubFXAttachmentParams params;
	AK::IAkPluginParam * pParam = in_pInputContext->GetInputParam();
	if (pParam)	//Use custom parameters
	{
		((MixerStubFXAttachmentParams*)pParam)->GetParams(params);	
	}
	else { 		//Use default parameters
		MixerStubFXAttachmentParams p;
		p.Init(global_plugin_mem_alloc, NULL, 0);
		p.GetParams(params);
	}
	LOGD(std::string("pParam bBypass: " + SSTR(params.bBypass) + ", m_fxInfo.params.bypassBus " + SSTR(m_fxInfo.params.bypassBus)).c_str());

	bool bInputBypass = params.bBypass;
	return ( bInputBypass || m_fxInfo.params.bypassBus);
}

void MixerStubFX::Compute3DPanningGains(
	AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
	AkEmitterListenerPair &		in_emitListPair,	// Emitter-listener pair.
	AkUInt32					in_uPairIndex,		// Index of the emitter-listener pair.
	AkChannelConfig				in_channelConfigIn,	// Channel config of the input.
	AkChannelConfig				in_channelConfigOut,// Channel config of the output (mix).
	AK::SpeakerVolumes::MatrixPtr out_mxVolumes		// Returned volume matrix.
	)
{
	m_pEffectContext->Compute3DPositioning(
		in_emitListPair.Azimuth(),
		in_emitListPair.Elevation(),
		in_pInputContext->GetSpread(in_uPairIndex),
		in_pInputContext->GetFocus(in_uPairIndex),
		in_channelConfigIn,
		in_emitListPair.uEmitterChannelMask,
		in_channelConfigOut,
		in_pInputContext->GetCenterPerc(),
		out_mxVolumes);
}

AKRESULT MixerStubFX::Term(AK::IAkPluginMemAlloc * in_pAllocator){

	if (RS3D_controller) {
		RS3D_DELETE(RS3D_controller);
		RS3D_FREE(input_buffer_scratch);
		RS3D_FREE(left_buffer_scratch);
		RS3D_FREE(right_buffer_scratch);

		RS3D_controller = NULL;
		input_buffer_scratch = NULL;
		left_buffer_scratch = NULL;
		right_buffer_scratch = NULL;
	}

	return MixerBase::Term(in_pAllocator);
}


void MixerStubFX::channelMask2FRPairsAzim(AkUInt32 uChannelMask, AkUInt32 & FL, AkUInt32 & FR, AkUInt32 & RL, AkUInt32 & RR, float *channel_azim_rads) {
	if (uChannelMask == AK_SPEAKER_SETUP_STEREO || uChannelMask == AK_SPEAKER_SETUP_2POINT1) {
		channel_azim_rads[AK_IDX_SETUP_2_LEFT] = Deg2Rad(120);
		channel_azim_rads[AK_IDX_SETUP_2_RIGHT] = Deg2Rad(60);
		channel_azim_rads[AK_IDX_SETUP_2_LFE] = Deg2Rad(90);
		FL = AK_IDX_SETUP_2_LEFT;
		FR = AK_IDX_SETUP_2_RIGHT;
		RL = AK_IDX_SETUP_2_LEFT;
		RR = AK_IDX_SETUP_2_RIGHT;
	}else if (uChannelMask == AK_SPEAKER_SETUP_4 || uChannelMask == AK_SPEAKER_SETUP_4POINT1) {
		channel_azim_rads[AK_IDX_SETUP_4_FRONTLEFT] = Deg2Rad(120);
		channel_azim_rads[AK_IDX_SETUP_4_FRONTRIGHT] = Deg2Rad(60);
		channel_azim_rads[AK_IDX_SETUP_4_REARLEFT] = Deg2Rad(-120);		//(-160 default)
		channel_azim_rads[AK_IDX_SETUP_4_REARRIGHT] = Deg2Rad(-60);		//(-20 default)
		channel_azim_rads[AK_IDX_SETUP_4_LFE] = Deg2Rad(90);
		FL = AK_IDX_SETUP_4_FRONTLEFT;
		FR = AK_IDX_SETUP_4_FRONTRIGHT;
		RL = AK_IDX_SETUP_4_REARLEFT;
		RR = AK_IDX_SETUP_4_REARRIGHT;
	}
	else if (uChannelMask == AK_SPEAKER_SETUP_5 || uChannelMask == AK_SPEAKER_SETUP_5POINT1) {
		channel_azim_rads[AK_IDX_SETUP_5_FRONTLEFT] = Deg2Rad(120);
		channel_azim_rads[AK_IDX_SETUP_5_FRONTRIGHT] = Deg2Rad(60);
		channel_azim_rads[AK_IDX_SETUP_5_CENTER] = Deg2Rad(90);
		channel_azim_rads[AK_IDX_SETUP_5_REARLEFT] = Deg2Rad(-120);		//(-160 default)
		channel_azim_rads[AK_IDX_SETUP_5_REARRIGHT] = Deg2Rad(-60);		//(-20 default)
		channel_azim_rads[AK_IDX_SETUP_5_LFE] = Deg2Rad(90);
		FL = AK_IDX_SETUP_5_FRONTLEFT;
		FR = AK_IDX_SETUP_5_FRONTRIGHT;
		RL = AK_IDX_SETUP_5_REARLEFT;
		RR = AK_IDX_SETUP_5_REARRIGHT;
	}
	else if (uChannelMask == AK_SPEAKER_SETUP_7 || uChannelMask == AK_SPEAKER_SETUP_7POINT1) {
		channel_azim_rads[AK_IDX_SETUP_7_FRONTLEFT] = Deg2Rad(120);
		channel_azim_rads[AK_IDX_SETUP_7_FRONTRIGHT] = Deg2Rad(60);
		channel_azim_rads[AK_IDX_SETUP_7_CENTER] = Deg2Rad(90);
		channel_azim_rads[AK_IDX_SETUP_7_REARLEFT] = Deg2Rad(-120);		//(-160 default)
		channel_azim_rads[AK_IDX_SETUP_7_REARRIGHT] = Deg2Rad(-60);		//(-20 default)
		channel_azim_rads[AK_IDX_SETUP_7_SIDELEFT] = Deg2Rad(-135);
		channel_azim_rads[AK_IDX_SETUP_7_SIDERIGHT] = Deg2Rad(-45);
		channel_azim_rads[AK_IDX_SETUP_7_LFE] = Deg2Rad(90);
		FL = AK_IDX_SETUP_7_FRONTLEFT;
		FR = AK_IDX_SETUP_7_FRONTRIGHT;
		RL = AK_IDX_SETUP_7_REARLEFT;
		RR = AK_IDX_SETUP_7_REARRIGHT;
	}
}

void MixerStubFX::ConsumeInput(
	AK::IAkMixerInputContext *	in_pInputContext,	///< Context for this input. Carries non-audio data.
	AkRamp						in_baseVolume,		///< Base volume to apply to this input (prev corresponds to the beginning, next corresponds to the end of the buffer). This gain is agnostic of emitter-listener pair-specific contributions (such as distance level attenuation).
	AkRamp						in_emitListVolume,	///< Emitter-listener pair-specific gain. When there are multiple emitter-listener pairs, this volume equals 1, and pair gains are applied directly on the channel volume matrix (accessible via IAkMixerInputContext::GetSpatializedVolumes()). For custom processing of emitter-listener pairs, one should query each pair volume using IAkMixerInputContext::Get3DPosition(), then AkEmitterListenerPair::GetGainForConnectionType().
	AkAudioBuffer *				io_pInputBuffer,	///< Input audio buffer data structure. Plugins should avoid processing data in-place.
	AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored until call to GetOutput().
	)
{
	AKASSERT(io_pInputBuffer->uValidFrames == io_pMixBuffer->MaxFrames());



	const AkUInt16 uNumFrames = io_pInputBuffer->uValidFrames;
	AkUInt32 num_in_channels = io_pInputBuffer->NumChannels();
	AkUInt32 num_out_channels = io_pMixBuffer->NumChannels();

	AkRamp modified_volume = in_baseVolume * in_emitListVolume;

	//Update mixer bus parameters
	m_pSharedParams->GetParams(m_fxInfo.params);

	//Get number of emitter-listener pairs
	int number_3D_positions = in_pInputContext->GetNum3DPositions(); 

	bool valid_channels = (num_in_channels >= 1 && num_out_channels >= 2);
	bool custom_process = !IsCustomProcessingBypassed(in_pInputContext) && RS3D_controller && valid_channels;
	

	//Process if number of positions is greater than one
	if (custom_process) {
		bool is_voice = in_pInputContext->GetVoiceInfo() != NULL;				//voice as opposed to bus

		//Gain compensation from using HRTFs
		float modified_gain_multiplier = 1.5;
		modified_volume.fNext *= modified_gain_multiplier;
		modified_volume.fPrev *= modified_gain_multiplier;

		if (is_voice && (number_3D_positions > 0)) {								//voice with 3d position enabled

			AkUInt32 in_channel_config_type = io_pInputBuffer->GetChannelConfig().eConfigType;
			AkUInt32 out_channel_config_type = io_pMixBuffer->GetChannelConfig().eConfigType;

			if (in_channel_config_type == AK_ChannelConfigType_Ambisonic && out_channel_config_type == AK_ChannelConfigType_Ambisonic) {
				//Pass-through ambisonics by copying channel data
			}
			else if (in_channel_config_type == AK_ChannelConfigType_Ambisonic && out_channel_config_type != AK_ChannelConfigType_Ambisonic) {
				////////////////////////////////////////////////////////////////////////////
				///////////////////Decode to ambisonics and render to binaural via fast spatializers
				////////////////////////////////////////////////////////////////////////////
				
				RS3DVector(const AkReal32 *) input_buffer_list;
				input_buffer_list.resize(num_in_channels);
				for (int i = 0; i < num_in_channels; ++i)
					input_buffer_list[i] = io_pInputBuffer->GetChannel(i);

				RS3D_controller->process_ambisonics(in_pInputContext, m_fxInfo.params, input_buffer_list,  &left_buffer_scratch[0], &right_buffer_scratch[0]);

				LOGD("Mixing to stereo");
				m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(0), 1.f, 1.f, uNumFrames);
				m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(1), 1.f, 1.f, uNumFrames);
	

			}
			else if (in_channel_config_type != AK_ChannelConfigType_Ambisonic && out_channel_config_type == AK_ChannelConfigType_Ambisonic) {
				//Encode to ambisonics
			}
			else if (in_channel_config_type != AK_ChannelConfigType_Ambisonic && out_channel_config_type != AK_ChannelConfigType_Ambisonic) {
				////////////////////////////////////////////////////////////////////////////
				///////////////////Spatialize first channel to non-ambisonics
				////////////////////////////////////////////////////////////////////////////
				//Determine output channel config
				AkUInt32 uChannelMask = io_pMixBuffer->GetChannelConfig().uChannelMask;
				if (uChannelMask == AK_SPEAKER_SETUP_STEREO || uChannelMask == AK_SPEAKER_SETUP_2POINT1 || uChannelMask == AK_SPEAKER_SETUP_4 || uChannelMask == AK_SPEAKER_SETUP_4POINT1 || uChannelMask == AK_SPEAKER_SETUP_5 || uChannelMask == AK_SPEAKER_SETUP_5POINT1 || uChannelMask == AK_SPEAKER_SETUP_7 || uChannelMask == AK_SPEAKER_SETUP_7POINT1) {
					//Compute and apply modified gain 
					AkSampleType *input_buffer = io_pInputBuffer->GetChannel(0);			//Spatializer first input channel only

					//for (int i = 0; i < uNumFrames; ++i)
					//	input_buffer_scratch[i] = input_buffer[i] * modified_volume.fNext;

					for (int i = 0; i < uNumFrames; ++i)
						input_buffer_scratch[i] = input_buffer[i];

					//Spatialize!
					RS3D_controller->process_voice(in_pInputContext, m_fxInfo.params, &input_buffer_scratch[0], &left_buffer_scratch[0], &right_buffer_scratch[0]);

#ifdef __linux__
					uChannelMask = AK_SPEAKER_SETUP_STEREO; //Default linux to stereo output
#endif
					//Upmix binaural to non-ambisonics output config
					//Physics coordinate system, get channel positions along azimuth plane
					float channel_azim_rads[8];
					AkUInt32 FL, FR, RL, RR;
					channelMask2FRPairsAzim(uChannelMask, FL, FR, RL, RR, channel_azim_rads);

					LOGD("Before mixing into output buffers");
					if (uChannelMask == AK_SPEAKER_SETUP_STEREO || uChannelMask == AK_SPEAKER_SETUP_2POINT1) {	//Stereo, mix to first two channels only
						LOGD("Mixing to stereo");
						//m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(FL), 1.f, 1.f, uNumFrames);
						//m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(FR), 1.f, 1.f, uNumFrames);
						m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(FL), modified_volume.fPrev, modified_volume.fNext, uNumFrames);
						m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(FR), modified_volume.fPrev, modified_volume.fNext, uNumFrames);
					}
					else if (uChannelMask == AK_SPEAKER_SETUP_4 || uChannelMask == AK_SPEAKER_SETUP_4POINT1 || uChannelMask == AK_SPEAKER_SETUP_5 || uChannelMask == AK_SPEAKER_SETUP_5POINT1 || uChannelMask == AK_SPEAKER_SETUP_7 || uChannelMask == AK_SPEAKER_SETUP_7POINT1) {
						//Upmix to quadaphonic, 5.1, 7.1
						LOGD("Upmixing");

						//Compute panning weights via azimuth only (assumes speakers are locked to listener orientation) 
						float panning_wts_left[8];				//Percent of binaural left mix to add to output channel
						float panning_wts_right[8];				//Percent of binaural right mix to add to output channel
						float source_out_channel_side[8];		//positive or negative side of output channel
						float common_wt[8];						//Inner product between source-direction and output channel

						AkVector listener_source_ray = RS3D_controller->get_prev_listener_source_ray();
						AkVector listener_source_ray_sph = AkVectorExt::computeAzimuthElevationDistZXY(listener_source_ray);

						AkVector listener_orientation_sph = RS3D_controller->get_prev_listener_orientation();
						AkReal32 azim_diff = listener_source_ray_sph.X - (listener_orientation_sph.X + Deg2Rad(90));					//Radian difference between source-direction ray and listener's forward ray

						eBinauralPanningMode panning_mode = FRONT_REAR_PAN_SIDES;

						//Compute panning weights from binaural LR
						for (int i = 0; i < num_out_channels; ++i) {
							common_wt[i] = cos(azim_diff + Deg2Rad(90) - channel_azim_rads[i]);									//Inner product between source-direction and output channel
							float non_negative_common_wt = std::max<float>(0.0, common_wt[i]);
							source_out_channel_side[i] = sin(azim_diff + Deg2Rad(90) - channel_azim_rads[i]);						//Side that source-direction is on w.r.t. output channel
							float left_wt = std::max<float>(0.0, cos(Deg2Rad(180) - channel_azim_rads[i]));							//Inner product between hard left and output channel
							float right_wt = std::max<float>(0.0, cos(Deg2Rad(0) - channel_azim_rads[i]));							//Inner product between hard right and output channel

																																	//Generic panning modes
							if (panning_mode == COMMON_LEFT_RIGHT) {
								panning_wts_left[i] = non_negative_common_wt;
								panning_wts_right[i] = non_negative_common_wt;
							}
							else if (panning_mode == COMMON_DYNAMIC_PRODUCT) {
								panning_wts_left[i] = left_wt * non_negative_common_wt;
								panning_wts_right[i] = right_wt * non_negative_common_wt;
							}
							else if (panning_mode == COMMON_DYNAMIC_AVG) {
								panning_wts_left[i] = 0.5f * (left_wt + non_negative_common_wt);
								panning_wts_right[i] = 0.5f * (right_wt + non_negative_common_wt);
							}
							else {
								//Other panning mode, initialize to 0
								panning_wts_left[i] = 0;
								panning_wts_right[i] = 0;
							}
						}

						//Custom, piece-wise panning modes
						if (panning_mode == FRONT_REAR_NO_PAN) {
							if (cos(azim_diff) > 0) {	//source is in front
								panning_wts_left[FL] = 1;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//front right speaker
								panning_wts_right[FR] = 1;
								panning_wts_left[RL] = 0;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 0;
							}
							else {						//source is in back
								panning_wts_left[FL] = 0;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//right speaker
								panning_wts_right[FR] = 0;
								panning_wts_left[RL] = 1;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 1;
							}
						}
						else if (panning_mode == FRONT_REAR_PAN_SIDES) {
							if (source_out_channel_side[FL] < 0 && source_out_channel_side[FR] > 0) {	//Between front left and front right
								panning_wts_left[FL] = 1;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//front right speaker
								panning_wts_right[FR] = 1;
								panning_wts_left[RL] = 0;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 0;
							}
							else if (source_out_channel_side[RL] > 0 && source_out_channel_side[RR] < 0) {	//Between rear left and rear right
								panning_wts_left[FL] = 0;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//right speaker
								panning_wts_right[FR] = 0;
								panning_wts_left[RL] = 1;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 1;
							}
							else if (source_out_channel_side[FL] > 0 && source_out_channel_side[RL] < 0) {	//Between front left and rear left
								float angle_from_channel_a = acos(common_wt[FL]);
								float angle_from_channel_b = acos(common_wt[RL]);
								float angle_sum = angle_from_channel_a + angle_from_channel_b;
								float alpha = angle_from_channel_b / angle_sum;

								panning_wts_left[FL] = alpha;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//front right speaker
								panning_wts_right[FR] = alpha;
								panning_wts_left[RL] = 1 - alpha;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 1 - alpha;
							}
							else if (source_out_channel_side[FR] < 0 && source_out_channel_side[RR] > 0) {	//Between front right and rear right
								float angle_from_channel_a = acos(common_wt[FR]);
								float angle_from_channel_b = acos(common_wt[RR]);
								float angle_sum = angle_from_channel_a + angle_from_channel_b;
								float alpha = angle_from_channel_b / angle_sum;

								panning_wts_left[FL] = alpha;	//front left speaker	
								panning_wts_right[FL] = 0;
								panning_wts_left[FR] = 0;	//front right speaker
								panning_wts_right[FR] = alpha;
								panning_wts_left[RL] = 1 - alpha;	//rear left speaker
								panning_wts_right[RL] = 0;
								panning_wts_left[RR] = 0;	//rear right speaker
								panning_wts_right[RR] = 1 - alpha;
							}
						}

						//Mix!!!
						for (int i = 0; i < num_out_channels; ++i) {
							m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(i), panning_wts_left[i], panning_wts_left[i], uNumFrames);
							m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(i), panning_wts_right[i], panning_wts_right[i], uNumFrames);
						}
					}
				}
			}
			else {
				LOGE("MixerStubFX::ConsumeInput: Output channel config unsupported");
			}
		}
	} else {
		// Fallback. Use spatialization values as computed by the sound engine. As was mentioned above, let's use our own previous values if we decided to handle them on our side.

		if (number_3D_positions > 0) {								//voice with 3d position enabled
			//Compute modified gain 
			AkEmitterListenerPair ray;
			AKVERIFY(in_pInputContext->Get3DPosition(0, ray) == AK_Success);		//Grabs gains from first ray (assumed to be actual position of source, not meta data)
			AkReal32 fNextRayVolume = ray.GetGainForConnectionType(in_pInputContext->GetConnectionType());
			modified_volume.fNext = modified_volume.fNext * fNextRayVolume;
			modified_volume.fPrev = modified_volume.fPrev * fNextRayVolume;
		}

		//// Get volume gains computed by Wwise.
		//AkUInt32 uAllocSize = AK::SpeakerVolumes::Matrix::GetRequiredSize(io_pInputBuffer->NumChannels(), io_pMixBuffer->NumChannels());
		//AK::SpeakerVolumes::MatrixPtr volumesPrev = (AK::SpeakerVolumes::MatrixPtr)AkAlloca(uAllocSize);
		//AK::SpeakerVolumes::MatrixPtr volumesNext = (AK::SpeakerVolumes::MatrixPtr)AkAlloca(uAllocSize);
		//in_pInputContext->GetSpatializedVolumes(volumesPrev, volumesNext);
		//
		//// Mix.
		//m_pEffectContext->MixNinNChannels(
		//	io_pInputBuffer,		// Input buffer.
		//	io_pMixBuffer,			// Buffer with which the input buffer is mixed.
		//	modified_volume.fPrev,
		//	modified_volume.fNext,
		//	volumesPrev,
		//	volumesNext);

		//Easy custom mix
		if (num_out_channels >= 2) {
			if (num_in_channels == 1) {
				m_pEffectContext->GlobalContext()->MixChannel(io_pInputBuffer->GetChannel(0), io_pMixBuffer->GetChannel(0), modified_volume.fPrev, modified_volume.fNext, uNumFrames);
				m_pEffectContext->GlobalContext()->MixChannel(io_pInputBuffer->GetChannel(0), io_pMixBuffer->GetChannel(1), modified_volume.fPrev, modified_volume.fNext, uNumFrames);
			}
			else {
				for (int i = 0; i < num_in_channels; ++i) {
					for (int j = 0; j < num_out_channels && j < num_in_channels; ++j) {
						m_pEffectContext->GlobalContext()->MixChannel(io_pInputBuffer->GetChannel(i), io_pMixBuffer->GetChannel(j), modified_volume.fPrev, modified_volume.fNext, uNumFrames);
					}
				}
			}
		}
	}

	//// At least one input has been mixed. Set number of valid output samples.
	io_pMixBuffer->uValidFrames = io_pInputBuffer->uValidFrames;
	LOGD("MixerStubFX::ConsumeInput SUCCESS");

}


void MixerStubFX::OnMixDone(
	AkAudioBuffer *	io_pMixBuffer		// Output audio buffer data structure. Stored across calls to ConsumeInput().
	)
{
	AkUInt32 channel_config_type = io_pMixBuffer->GetChannelConfig().eConfigType;
	if (channel_config_type == AK_ChannelConfigType_Ambisonic) {
		//Ambisonics Encode, normalize or divide by number of sound-sources?
		LOGE("MixerStubFX::OnMixDone:  AK_ChannelConfigType_Ambisonic output unsupported");
	}
	else {//Standard or anon channel config type
	#ifdef USE_BACCH
		AkUInt32 uChannelMask = io_pMixBuffer->GetChannelConfig().uChannelMask;

		float channel_azim_rads[8];
		AkUInt32 FL, FR, RL, RR;
		channelMask2FRPairsAzim(uChannelMask, FL, FR, RL, RR, channel_azim_rads);

		if (uChannelMask == AK_SPEAKER_SETUP_STEREO || uChannelMask == AK_SPEAKER_SETUP_2POINT1 || uChannelMask == AK_SPEAKER_SETUP_4 || uChannelMask == AK_SPEAKER_SETUP_4POINT1 || uChannelMask == AK_SPEAKER_SETUP_5 || uChannelMask == AK_SPEAKER_SETUP_5POINT1 || uChannelMask == AK_SPEAKER_SETUP_7 || uChannelMask == AK_SPEAKER_SETUP_7POINT1){
			if (m_fxInfo.params.crossCancel && !m_fxInfo.params.bypassBus) {		//At least stereo output and crossCancel is enabled
				AkSampleType *input_buffer;

				const AkUInt16 uNumFrames = io_pMixBuffer->uValidFrames;

				input_buffer = io_pMixBuffer->GetChannel(FL);			//Get front left channel
				for (int i = 0; i < uNumFrames; ++i)
					left_buffer_scratch[i] = input_buffer[i];

				input_buffer = io_pMixBuffer->GetChannel(FR);			//Get front right channel
				for (int i = 0; i < uNumFrames; ++i)
					right_buffer_scratch[i] = input_buffer[i];

				//process front left, front right
				RS3D_controller->process_stereo_bus_inplace(mixer_bus_ID, FL, FR, BACCH_STEREO_FRONT, &left_buffer_scratch[0], &right_buffer_scratch[0]);

				LOGD("Before mixing into output buffers");
				memset(io_pMixBuffer->GetChannel(FL), 0, uNumFrames * sizeof(AkSampleType));
				memset(io_pMixBuffer->GetChannel(FR), 0, uNumFrames * sizeof(AkSampleType));

				m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(FL), 1.f, 1.f, uNumFrames);
				m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(FR), 1.f, 1.f, uNumFrames);

				if (uChannelMask == AK_SPEAKER_SETUP_4 || uChannelMask == AK_SPEAKER_SETUP_4POINT1 || uChannelMask == AK_SPEAKER_SETUP_5 || uChannelMask == AK_SPEAKER_SETUP_5POINT1 || uChannelMask == AK_SPEAKER_SETUP_7 || uChannelMask == AK_SPEAKER_SETUP_7POINT1) {		//apply above to rear channel pair if output is quadraphonics, 5.1 or 7.1 (with or without LFE)

					input_buffer = io_pMixBuffer->GetChannel(RL);			//Get rear left channel
					for (int i = 0; i < uNumFrames; ++i)
						left_buffer_scratch[i] = input_buffer[i];

					input_buffer = io_pMixBuffer->GetChannel(RR);			//Get rear  right channel
					for (int i = 0; i < uNumFrames; ++i)
						right_buffer_scratch[i] = input_buffer[i];

					//process front left, front right
					RS3D_controller->process_stereo_bus_inplace(mixer_bus_ID, RL, RR, BACCH_STEREO_BACK, &left_buffer_scratch[0], &right_buffer_scratch[0]);

					LOGD("Before mixing into output buffers");
					memset(io_pMixBuffer->GetChannel(RL), 0, uNumFrames * sizeof(AkSampleType));
					memset(io_pMixBuffer->GetChannel(RR), 0, uNumFrames * sizeof(AkSampleType));

					m_pEffectContext->GlobalContext()->MixChannel(&left_buffer_scratch[0], io_pMixBuffer->GetChannel(RL), 1.f, 1.f, uNumFrames);
					m_pEffectContext->GlobalContext()->MixChannel(&right_buffer_scratch[0], io_pMixBuffer->GetChannel(RR), 1.f, 1.f, uNumFrames);
				}
			}
		}
		else {
			LOGE("MixerStubFX::OnMixDone:  Output channel config unsupported");
		}
	#endif

	}
}


void MixerStubFX::OnInputDisconnected(
	AK::IAkMixerInputContext * in_pInput
	) 
{

	if (RS3D_controller) {
		RS3D_controller->destroy_source_room_spatializer(in_pInput);
		RS3D_controller->destroy_ambisonics_source(in_pInput);
	}

	MixerBase::OnInputDisconnected(in_pInput);
}

#include "RS3DController.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DController::RS3DController(){}	//Dummy constructor

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DController::RS3DController(	AK::IAkMixerPluginContext *	_mixer_plugin_context,
								const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
								AkAudioFormat &	 _Ak_audio_format) 
{

#ifdef USE_BACCH
	RS3D_BACCH_FRONT_LEFT_RIGHT = NULL;
	RS3D_BACCH_BACK_LEFT_RIGHT = NULL;
#endif

	mixer_plugin_context = _mixer_plugin_context;
	global_mixer_plugin_context = _mixer_plugin_context;

	mixer_stub_FX_params_bus = _mixer_stub_FX_params_bus;
	Ak_audio_format = _Ak_audio_format;

	//Get samples per buffer
	num_samples_per_buffer = mixer_plugin_context->GlobalContext()->GetMaxBufferLength();
	LOGD(std::string("num_samples_per_buffer: " + SSTR(num_samples_per_buffer)).c_str());

	left_channel_output.resize(num_samples_per_buffer);
	right_channel_output.resize(num_samples_per_buffer);
	
	//Get sample rate
	sample_rate = Ak_audio_format.uSampleRate;
	LOGD(std::string("sample rate: " + SSTR(sample_rate)).c_str());

	////////////////////////////////////////////////////////////////////////////
	//Get license key
	////////////////////////////////////////////////////////////////////////////
	AkUInt8 * pPluginData = NULL;
	AkUInt32 uPluginDataSize;
	mixer_plugin_context->GetPluginMedia(0, pPluginData, uPluginDataSize);
	if (pPluginData) {
		license_key = std::string((char*)pPluginData, uPluginDataSize);
		license_key.erase(license_key.find_last_not_of(" \n\r\t") + 1);		//Remove trailing white-spaces
		license_key = license_key + "\0";

		LOGD("RS3DController::RS3DController: License key found");
		LOGD(std::string((char*)pPluginData).c_str());
	}else {
		LOGE("RS3DController::RS3DController: License key not found");
		return;
	}
	////////////////////////////////////////////////////////////////////////////
	///////////////////Create defacto listener 0
	////////////////////////////////////////////////////////////////////////////
	Anthropometry anthropometry;
	anthropometry.head_radius = mixer_stub_FX_params_bus.headSize / 100.0;
	anthropometry.neck_height = mixer_stub_FX_params_bus.neckSize / 100.0;
	anthropometry.torso_radius = mixer_stub_FX_params_bus.torsoSize / 100.0;


	//For debugging
	//LOGE(std::string("bypassBus " + SSTR(mixer_stub_FX_params_bus.bypassBus) +
	//	"crossCancel " + SSTR(mixer_stub_FX_params_bus.crossCancel) +
	//	"headSize " + SSTR(mixer_stub_FX_params_bus.headSize) +
	//	"torsoSize " + SSTR(mixer_stub_FX_params_bus.torsoSize) +
	//	"neckSize " + SSTR(mixer_stub_FX_params_bus.neckSize) +
	//	"HRTF_type_state " + SSTR(mixer_stub_FX_params_bus.HRTF_type_state) +
	//	"scaleFac " + SSTR(mixer_stub_FX_params_bus.scaleFac)).c_str());

	//Get HRTF data
	if (mixer_stub_FX_params_bus.HRTF_type_state >= 0 && mixer_stub_FX_params_bus.HRTF_type_state  <= 5) {
		mixer_plugin_context->GetPluginMedia(1 + mixer_stub_FX_params_bus.HRTF_type_state, pPluginData, uPluginDataSize);
		if (pPluginData) {
			HRTFBlob HRTF_blob;
			HRTF_blob.data = pPluginData;
			HRTF_blob.data_size = uPluginDataSize;

			//Insert listener into map
			RS3DListenerKey RS3D_listener_key = 0;

			RS3D_listener_map[RS3D_listener_key] = RS3D_NEW(RS3DListener(RS3D_listener_key, anthropometry, HRTF_blob, license_key));


			bool do_resample = false;
			RS3D_listener_map[RS3D_listener_key]->finialize_HRTF(num_samples_per_buffer, sample_rate, do_resample);

			//Initialize tail mgr
			RS3D_tail_mgr = RS3D_NEW(RS3DTailMgr(*RS3D_listener_map[RS3D_listener_key], num_samples_per_buffer, sample_rate, mixer_stub_FX_params_bus.scaleFac));
		}
		else {
			LOGE(std::string("RS3DController::RS3DController: HRTF dataset " + SSTR(mixer_stub_FX_params_bus.HRTF_type_state) + " not found").c_str());
			return;
		}
	}
	else {
		LOGE(std::string("RS3DController::RS3DController: HRTF dataset parameter " + SSTR(mixer_stub_FX_params_bus.HRTF_type_state) + " corruption").c_str());
		return;
	}

	//Initialize BACCH system
#ifdef USE_BACCH
	float BACCH_gain_multplier = 2.0;
	mixer_plugin_context->GetPluginMedia(7, pPluginData, uPluginDataSize);
	if (pPluginData) {
		RS3D_BACCH_FRONT_LEFT_RIGHT = RS3D_NEW(RS3DBACCH(num_samples_per_buffer, sample_rate, BACCH_gain_multplier, pPluginData));
	}else{
		LOGE("RS3DController::RS3DController: BACCH front data not found");
	}

	mixer_plugin_context->GetPluginMedia(8, pPluginData, uPluginDataSize);
	if(pPluginData) {
		RS3D_BACCH_BACK_LEFT_RIGHT = RS3D_NEW(RS3DBACCH(num_samples_per_buffer, sample_rate, BACCH_gain_multplier, pPluginData));
	}else{
		LOGE("RS3DController::RS3DController: BACCH back data not found");
	}
#endif

	game_blob_initialized = false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DController::~RS3DController() {

	//Delete tail manager
	if (RS3D_tail_mgr) {
		RS3D_DELETE(RS3D_tail_mgr);
		RS3D_tail_mgr = NULL;
	}

	//Delete map objects from heap
#ifdef USE_STATE_MAP
	RS3DMixerInputContextStatesMap::iterator RS3D_mixer_input_context_states_iterator;
	for (RS3D_mixer_input_context_states_iterator = RS3D_mixer_input_context_states_map.begin(); RS3D_mixer_input_context_states_iterator  != RS3D_mixer_input_context_states_map.end(); RS3D_mixer_input_context_states_iterator++){
		RS3D_DELETE(RS3D_mixer_input_context_states_iterator->second);
	}

	RS3DMixerInputContextStatesAmbMap::iterator RS3D_mixer_input_context_states_amb_iterator;
	for (RS3D_mixer_input_context_states_amb_iterator = RS3D_mixer_input_context_states_amb_map.begin(); RS3D_mixer_input_context_states_amb_iterator != RS3D_mixer_input_context_states_amb_map.end(); RS3D_mixer_input_context_states_amb_iterator++) {
		RS3D_DELETE(RS3D_mixer_input_context_states_amb_iterator->second);
	}

#endif

	//Delete ambisonics map objects
	RS3DAmbisonicsMap::iterator RS3D_ambisonics_iterator;
	for (RS3D_ambisonics_iterator = RS3D_ambisonics_map.begin(); RS3D_ambisonics_iterator != RS3D_ambisonics_map.end(); RS3D_ambisonics_iterator++) {
		RS3D_DELETE(RS3D_ambisonics_iterator->second);
	}

	//Delete spatializer map objects
	RS3DSpatializerMap::iterator RS3D_spatializer_iterator;
	for (RS3D_spatializer_iterator = RS3D_spatializer_map.begin(); RS3D_spatializer_iterator  != RS3D_spatializer_map.end(); RS3D_spatializer_iterator++){
		RS3D_DELETE(RS3D_spatializer_iterator->second);
	}

	//Delete source map objects
	RS3DSourceMap::iterator RS3D_source_iterator;
	for (RS3D_source_iterator = RS3D_source_map.begin(); RS3D_source_iterator != RS3D_source_map.end(); RS3D_source_iterator++) {
		RS3D_DELETE(RS3D_source_iterator->second);
	}


	//Delete room map objects
	RS3DRoomMap::iterator RS3D_room_iterator;
	for (RS3D_room_iterator = RS3D_room_map.begin(); RS3D_room_iterator != RS3D_room_map.end(); RS3D_room_iterator++) {
		RS3D_DELETE(RS3D_room_iterator->second);
	}

	//Delete listener map objects
	RS3DListenerMap::iterator RS3D_listener_iterator;
	for (RS3D_listener_iterator = RS3D_listener_map.begin(); RS3D_listener_iterator != RS3D_listener_map.end(); RS3D_listener_iterator++) {
		RS3D_DELETE(RS3D_listener_iterator->second);
	}

	//Delete BACCH map objects
#ifdef USE_BACCH
	RS3DBACCHBusRingBufferMap::iterator RS3D_BACCH_bus_ring_buffer_iterator;
	for (RS3D_BACCH_bus_ring_buffer_iterator = RS3D_BACCH_bus_ring_buffer_map.begin(); RS3D_BACCH_bus_ring_buffer_iterator != RS3D_BACCH_bus_ring_buffer_map.end(); RS3D_BACCH_bus_ring_buffer_iterator++) {
		RS3D_DELETE(RS3D_BACCH_bus_ring_buffer_iterator->second);
	}
	//Delete BACCH system
	if(RS3D_BACCH_FRONT_LEFT_RIGHT)
		RS3D_DELETE(RS3D_BACCH_FRONT_LEFT_RIGHT);

	if(RS3D_BACCH_BACK_LEFT_RIGHT)
		RS3D_DELETE(RS3D_BACCH_BACK_LEFT_RIGHT);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::process_voice(	AK::IAkMixerInputContext *	_mixer_input_context,
									const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
									const AkReal32 * input_buffer_mono,
									AkReal32 * output_buffer_left,
									AkReal32 * output_buffer_right)
{
	process_params(_mixer_input_context, _mixer_stub_FX_params_bus);

	//Update from game data blob
	if (!voice_in_editor &&  mixer_stub_FX_attachment_params.gameDefinedParams) {
		void * out_rpData;
		AkUInt32 out_rDataSize;
		mixer_plugin_context->GetPluginCustomGameData(out_rpData, out_rDataSize);
		if (out_rpData && 	RS3D_game_blob.getData(out_rpData, out_rDataSize)) {
			game_blob_initialized = true;
			RS3D_tail_mgr->setTailsToGameBlobRooms(RS3D_game_blob);
		}
	}

	//Generate keys
	RS3DSourceKey RS3D_source_key = generate_source_key();					
	RS3DRoomKey RS3D_room_key = generate_room_key();				
	RS3DListenerKey RS3D_listener_key = generate_listener_key();
	RS3DSpatializerKey RS3D_spatializer_key = generate_spatializer_key(RS3D_source_key, RS3D_room_key, RS3D_listener_key);

#ifdef USE_STATE_MAP
	//Add spatializer key to mixer input context state map
	IAkMixerPluginContextKey mixer_input_context_key = (IAkMixerPluginContextKey)_mixer_input_context;
	if (RS3D_mixer_input_context_states_map.find(mixer_input_context_key) == RS3D_mixer_input_context_states_map.end()) {
		//mixer state not found, create one
		RS3D_mixer_input_context_states_map[mixer_input_context_key] = RS3D_NEW(RS3DSpatializerKey());
	}
	*RS3D_mixer_input_context_states_map[mixer_input_context_key] = RS3D_spatializer_key;
#endif

	//Update/create objects in map
	update_listener_map(RS3D_listener_key);
	update_room_map(RS3D_room_key);
	update_source_map(RS3D_source_key);

	update_spatializer_map(RS3D_source_key, RS3D_room_key, RS3D_listener_key, RS3D_spatializer_key);
		
	//Compute ray between listener and source
	prev_listener_source_ray = AkVectorExt::subtract(RS3D_source_map[RS3D_source_key]->get_position(), RS3D_listener_map[RS3D_listener_key]->get_position());
	prev_listener_orientation = RS3D_listener_map[RS3D_listener_key]->get_orientation();

	//Zero output buffers
	memset(&left_channel_output[0], 0,  num_samples_per_buffer * sizeof(AkReal32));
	memset(&right_channel_output[0], 0, num_samples_per_buffer * sizeof(AkReal32));

	//Process!
	RS3D_spatializer_map[RS3D_spatializer_key]->process(input_buffer_mono, left_channel_output, right_channel_output);

	//Copy to Wwise output
	memcpy(output_buffer_left, &left_channel_output[0], num_samples_per_buffer * sizeof(AkReal32));
	memcpy(output_buffer_right, &right_channel_output[0], num_samples_per_buffer * sizeof(AkReal32));

	////White noise test
	//for (int i = 0; i < num_samples_per_buffer; ++i) {
	//	output_buffer_left[i] = (rand() % 1000) / 1000.0f - 0.5f;
	//	output_buffer_right[i] = (rand() % 1000) / 1000.0f - 0.5f;
	//}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::process_ambisonics(AK::IAkMixerInputContext *	_mixer_input_context,
	const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
	const RS3DVector(const AkReal32 *) & input_buffer_list,
	AkReal32 * output_buffer_left,
	AkReal32 * output_buffer_right) {

	process_params(_mixer_input_context, _mixer_stub_FX_params_bus);
	
	//Generate keys
	RS3DSourceKey RS3D_source_key = generate_source_key();
	RS3DListenerKey RS3D_listener_key = generate_listener_key();

#ifdef USE_STATE_MAP
	//Add source key to mixer input context state amb map
	IAkMixerPluginContextKey mixer_input_context_key = (IAkMixerPluginContextKey)_mixer_input_context;
	if (RS3D_mixer_input_context_states_amb_map.find(mixer_input_context_key) == RS3D_mixer_input_context_states_amb_map.end()) {
		//mixer state not found, create one
		RS3D_mixer_input_context_states_amb_map[mixer_input_context_key] = RS3D_NEW(RS3DSourceKey());
	}
	*RS3D_mixer_input_context_states_amb_map[mixer_input_context_key] = RS3D_source_key;
#endif

	update_listener_map(RS3D_listener_key);
	int ambisonics_order = (int)sqrt((double)input_buffer_list.size()) - 1;
	update_ambisonics_map(ambisonics_order, RS3D_source_key, RS3D_listener_key);

	//Zero output buffers
	memset(&left_channel_output[0], 0, num_samples_per_buffer * sizeof(AkReal32));
	memset(&right_channel_output[0], 0, num_samples_per_buffer * sizeof(AkReal32));

	//Process!
	RS3D_ambisonics_map[RS3D_source_key]->decode_to_binaural(input_buffer_list, left_channel_output, right_channel_output);

	//Copy to Wwise output
	memcpy(output_buffer_left, &left_channel_output[0], num_samples_per_buffer * sizeof(AkReal32));
	memcpy(output_buffer_right, &right_channel_output[0], num_samples_per_buffer * sizeof(AkReal32));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::process_params(	AK::IAkMixerInputContext *	_mixer_input_context,
								const MixerStubFXParamsBus & _mixer_stub_FX_params_bus) {
	mixer_input_context = _mixer_input_context;
	mixer_stub_FX_params_bus = _mixer_stub_FX_params_bus;

	//Get voice info
	voice_info = mixer_input_context->GetVoiceInfo();
	voice_in_editor = is_voice_in_editor();

	//Update from attachment parameters
	AK::IAkPluginParam * pParam = _mixer_input_context->GetInputParam();
	if (pParam) {			//Using non-default parameters
		((MixerStubFXAttachmentParams*)pParam)->GetParams(mixer_stub_FX_attachment_params);
	}
	else {					//Use default parameters
		MixerStubFXAttachmentParams p;
		p.Init(global_plugin_mem_alloc, NULL, 0);
		p.GetParams(mixer_stub_FX_attachment_params);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef USE_BACCH
void RS3DController::process_stereo_bus_inplace(
	AkUniqueID bus_ID, int channel_left_ID, int channel_right_ID, BACCH_STEREO_TYPE BACCH_stereo_type,
	AkReal32 * input_output_buffer_left,
	AkReal32 * input_output_buffer_right) 
{

	//Generate keys from bus and channel ids
	RS3DBACCHBusKey bus_key_left = RS3DBACCHBusKey(bus_ID, channel_left_ID);
	RS3DBACCHBusKey bus_key_right = RS3DBACCHBusKey(bus_ID, channel_right_ID);

	//Copy data into left, right buffers
	memcpy(&left_channel_output[0], input_output_buffer_left, sizeof(float) * num_samples_per_buffer);
	memcpy(&right_channel_output[0], input_output_buffer_right, sizeof(float) * num_samples_per_buffer);
	
	process_BACCH_system_stereo_bus(bus_key_left, bus_key_right, BACCH_stereo_type);

	//Copy processed data out
	memcpy(input_output_buffer_left, &left_channel_output[0], sizeof(float) * num_samples_per_buffer);
	memcpy(input_output_buffer_right, &right_channel_output[0], sizeof(float) * num_samples_per_buffer);
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DController::get_prev_listener_source_ray() {
	return prev_listener_source_ray;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DController::get_prev_listener_orientation() {
	return prev_listener_orientation;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::update_source_map(RS3DSourceKey RS3D_source_key) {
	
	AkSoundPosition sound_position, sound_attenuation;
	
	double min_range = mixer_stub_FX_attachment_params.minRange;
	double max_range = mixer_stub_FX_attachment_params.maxRange;

	voice_info->GetGameObjectPosition(0, sound_position);

	if (voice_in_editor){
		//In editor, replace with preview source set at preview distance
		AkVector preview_position;
		preview_position.X = 0;
		preview_position.Y = 0;
		preview_position.Z = mixer_stub_FX_attachment_params.previewDist;
		
		sound_position.SetPosition(preview_position);
	}

	//Add to map
	if (RS3D_source_map.find(RS3D_source_key) == RS3D_source_map.end()) {
		//Source not found, create one
		RS3D_source_map[RS3D_source_key] = RS3D_NEW(RS3DSource(RS3D_source_key));
	}

	//Update
	RS3D_source_map[RS3D_source_key]->update_position(sound_position.Position(), mixer_stub_FX_attachment_params.smoothDoppler ? 0.2f : 1.0f);
	RS3D_source_map[RS3D_source_key]->update_min_dist(min_range);
	RS3D_source_map[RS3D_source_key]->update_max_dist(max_range);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::update_listener_map(RS3DListenerKey  RS3D_listener_key) {

	AkUInt32 listenerMask = voice_info->GetListenerMask();
	AkListener listener;
	voice_info->GetListenerData(1, listener);

	//Compute safe Euler angles in modified coordinate system
	float roll, pitch, yaw;
	if(voice_in_editor) {		//In editor, use preview parameters
		roll = 0;
		pitch = -mixer_stub_FX_attachment_params.previewElevation / 180.0f * _M_PI;
		yaw = -mixer_stub_FX_attachment_params.previewAzimuth / 180.0f * _M_PI;
	}else{					//In game, use listener data
		listener.position.SetOrientation(AkVectorExt::normalize(listener.position.OrientationFront()), AkVectorExt::normalize(listener.position.OrientationTop()));

		AkVector front_vec = listener.position.OrientationFront();
		AkVector top_vec = listener.position.OrientationTop();
		AkVector right_vec = AkVectorExt::cross(listener.position.OrientationFront(), listener.position.OrientationTop());

		//Matrix 2 Euler
		float m11 = front_vec.Z, m12 = right_vec.Z, m13 = top_vec.Z;
		float m21 = front_vec.X, m22 = right_vec.X, m23 = top_vec.X;
		float m31 = front_vec.Y, m32 = right_vec.Y, m33 = top_vec.Y;

		//Euler ZYX
		if (m31 >= 1) { // singularity at north pole
			yaw = atan2(-m23, -m13);
			pitch = _M_PI / 2;
			roll = 0;
		//	LOGE("North singularity");
		}
		else if (m31 <= -1) { // singularity at south pole
			yaw = atan2(m23, m13);
			pitch = -_M_PI / 2;
			roll = 0;
		//	LOGE("South singularity");
		}
		else {
			yaw = atan2(m21, m11);	//Z
			pitch = asin(m31);		//Y
			roll = atan2(m32, m33);	//X
		}

		////Matrix 2 Quaternion 2 Euler XYZ (roll, pitch, yaw)
		//float m00 = front_vec.X, m01 = right_vec.X, m02 = top_vec.X;
		//float m10 = front_vec.Z, m11 = right_vec.Z, m12 = top_vec.Z;
		//float m20 = front_vec.Y, m21 = right_vec.Y, m22 = top_vec.Y;

		////Matrix to quaternion
		//float tr = m00 + m11 + m22;
		//float qw, qx, qy, qz;
		//if (tr > 0) {
		//	float S = sqrt(tr + 1.0) * 2; // S=4*qw 
		//	qw = 0.25 * S;
		//	qx = (m21 - m12) / S;
		//	qy = (m02 - m20) / S;
		//	qz = (m10 - m01) / S;
		//}
		//else if ((m00 > m11)&(m00 > m22)) {
		//	float S = sqrt(1.0 + m00 - m11 - m22) * 2; // S=4*qx 
		//	qw = (m21 - m12) / S;
		//	qx = 0.25 * S;
		//	qy = (m01 + m10) / S;
		//	qz = (m02 + m20) / S;
		//}
		//else if (m11 > m22) {
		//	float S = sqrt(1.0 + m11 - m00 - m22) * 2; // S=4*qy
		//	qw = (m02 - m20) / S;
		//	qx = (m01 + m10) / S;
		//	qy = 0.25 * S;
		//	qz = (m12 + m21) / S;
		//}
		//else {
		//	float S = sqrt(1.0 + m22 - m00 - m11) * 2; // S=4*qz
		//	qw = (m10 - m01) / S;
		//	qx = (m02 + m20) / S;
		//	qy = (m12 + m21) / S;vr 
		//	qz = 0.25 * S;
		//}
		////Normalize Quaternion 
		//float qnorm = sqrt(qw * qw + qx * qx + qy * qy + qz * qz);
		//qw /= qnorm;
		//qx /= qnorm;
		//qy /= qnorm;
		//qz /= qnorm;
		////Quaternion 2 euler
		//float r11 = -2 * (qy*qz - qw*qx);
		//float r12 = qw*qw - qx*qx - qy*qy + qz*qz;
		//float r21 = 2 * (qx*qz + qw*qy);
		//float r31 = -2 * (qx*qy - qw*qz);
		//float r32 = qw*qw + qx*qx - qy*qy - qz*qz;

		//yaw = atan2(r31, r32);
		//pitch = asin(r21);
		//roll = atan2(r11, r12);

	}


	AkVector orientation;
	orientation.X = -yaw;
	orientation.Y = pitch;
	orientation.Z = roll;
										
	//Add to map
	if (RS3D_listener_map.find(RS3D_listener_key) == RS3D_listener_map.end()) {
		LOGD("RS3DController::Process find listener 0  FAIL");
		//TODO: Insert listener using another mechanism for passing listener data
	}

	//Update
	RS3D_listener_map[RS3D_listener_key]->update_position(listener.position.Position(), mixer_stub_FX_attachment_params.smoothDoppler ? 0.2f : 1.0f);
	RS3D_listener_map[RS3D_listener_key]->update_orientation(orientation, 1.0f);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::update_room_map(RS3DRoomKey  RS3D_room_key) {
				
	if (RS3D_room_map.find(RS3D_room_key) == RS3D_room_map.end()) {
		//Room not found, create one
		RS3D_room_map[RS3D_room_key] = RS3D_NEW(RS3DRoom(RS3D_room_key, mixer_stub_FX_params_bus.scaleFac));
	}

	//Determine where to get room parameters
	if (!voice_in_editor &&  mixer_stub_FX_attachment_params.gameDefinedParams) {
		////Retrieve room parameters from game-data
		AkSoundPosition sound_position;
		voice_info->GetGameObjectPosition(0, sound_position);

		int room_found_ID = INT_MIN;
		bool match_found = false;
		if (game_blob_initialized) {//Search rooms from game blob if available
			for (int i = 0; i < RS3D_game_blob.RS3D_game_data_state_header.num_rooms; ++i) {
				const RS3DGameDataRoom & game_data_room = RS3D_game_blob.RS3D_game_data_rooms[i];

				if(game_data_room.vertex_in_room(sound_position.Position())){//Source in room, match found! 
					if (game_data_room.room_ID >= room_found_ID) {
						room_found_ID = game_data_room.room_ID;
						match_found = true;
						RS3D_room_map[RS3D_room_key]->update_with_RS3DGameDataRoom(game_data_room);
					}
				}
			}
		}

		if (!match_found) {
			//No match, default to a large room whose center moves with source with no reflections
			RS3D_room_map[RS3D_room_key]->update_with_universe_room(sound_position.Position(), room_found_ID);
		}
	}
	else {
		////Retrieve room parameters from mixer-attachment
		AkVector room_center, room_size;
		RoomWallReflectionCoefficients reflection_coefficients;
		int order_early_reflection;
		int order_late_reflection;
		double reverb_length_seconds;

		//Room center and size
		room_center.X = mixer_stub_FX_attachment_params.roomCenterX;		room_center.Y = mixer_stub_FX_attachment_params.roomCenterY;			room_center.Z = mixer_stub_FX_attachment_params.roomCenterZ;
		room_size.X = mixer_stub_FX_attachment_params.roomSizeX;			room_size.Y = mixer_stub_FX_attachment_params.roomSizeY, room_size.Z = mixer_stub_FX_attachment_params.roomSizeZ;

		//Retrieve reflection coefficients of mixer
		reflection_coefficients.left = mixer_stub_FX_attachment_params.roomReflLeft / 100.0;
		reflection_coefficients.right = mixer_stub_FX_attachment_params.roomReflRight / 100.0;
		reflection_coefficients.front = mixer_stub_FX_attachment_params.roomReflFront / 100.0;
		reflection_coefficients.back = mixer_stub_FX_attachment_params.roomReflBack / 100.0;
		reflection_coefficients.floor = mixer_stub_FX_attachment_params.roomReflFloor / 100.0;
		reflection_coefficients.ceiling = mixer_stub_FX_attachment_params.roomReflCeil / 100.0;

		//Retrieve early and late reflection orders and reverb seconds
		order_early_reflection = (int)mixer_stub_FX_attachment_params.earlyNRefl;
		order_late_reflection = (int)mixer_stub_FX_attachment_params.maxNRefl;
		reverb_length_seconds = mixer_stub_FX_attachment_params.reverbLength / 1000.0;

		//Update room
		RS3D_room_map[RS3D_room_key]->update_center(room_center);
		RS3D_room_map[RS3D_room_key]->update_dimensions(room_size);
		RS3D_room_map[RS3D_room_key]->update_wall_reflection_coefficients(reflection_coefficients);
		RS3D_room_map[RS3D_room_key]->update_reflection_orders(order_early_reflection, order_late_reflection);
		RS3D_room_map[RS3D_room_key]->update_reverb_length_seconds(reverb_length_seconds);
	}

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::update_spatializer_map(	RS3DSourceKey  RS3D_source_key,
												RS3DRoomKey  RS3D_room_key,
												RS3DListenerKey  RS3D_listener_key,
												RS3DSpatializerKey RS3D_spatializer_key)
{
	eRenderMode render_mode = (mixer_stub_FX_attachment_params.fastSpatial ? FAST_SPAT : REGULAR);
	bool smooth_doppler = mixer_stub_FX_attachment_params.smoothDoppler;

	//Add to map
	if (RS3D_spatializer_map.find(RS3D_spatializer_key) == RS3D_spatializer_map.end()) {
		//Spatializer not found, create one
		RS3D_spatializer_map[RS3D_spatializer_key] = RS3D_NEW(RS3DSpatializer(
			*RS3D_source_map[RS3D_source_key],
			*RS3D_room_map[RS3D_room_key],
			*RS3D_listener_map[RS3D_listener_key],
			RS3D_tail_mgr,
			num_samples_per_buffer,
			sample_rate,
			render_mode,
			smooth_doppler,
			1.0 / mixer_stub_FX_params_bus.scaleFac));
	}

	//Update
	RS3D_spatializer_map[RS3D_spatializer_key]->update(render_mode, smooth_doppler);

	////Retrieve occluder parameters from game-data
	if (!voice_in_editor &&  mixer_stub_FX_attachment_params.gameDefinedParams) {
		RS3D_spatializer_map[RS3D_spatializer_key]->add_occluders(RS3D_game_blob.RS3D_game_data_single_triangle_occluders, RS3D_game_blob.RS3D_game_data_state_header.tick);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::update_ambisonics_map(	int ambisonics_order,	
											RS3DSourceKey  RS3D_source_key,
											RS3DListenerKey  RS3D_listener_key) {
	
	//Add to map
	if (RS3D_ambisonics_map.find(RS3D_source_key) == RS3D_ambisonics_map.end()) {
		//Ambisonics source not found, create one
		RS3D_ambisonics_map[RS3D_source_key] = RS3D_NEW(RS3DAmbisonics(ambisonics_order, AMBI_GRID_DODECAHEDRON, 1,
			*RS3D_listener_map[RS3D_listener_key], 
			RS3DRoom(), num_samples_per_buffer, sample_rate, 1.0 / mixer_stub_FX_params_bus.scaleFac));
	}

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef USE_BACCH
void RS3DController::process_BACCH_system_stereo_bus(RS3DBACCHBusKey RS3D_BACCH_bus_key_left, RS3DBACCHBusKey RS3D_BACCH_bus_key_right, BACCH_STEREO_TYPE BACCH_stereo_type) {
	int num_samples_in_BACCH_output;
	if (BACCH_stereo_type == BACCH_STEREO_FRONT && RS3D_BACCH_FRONT_LEFT_RIGHT) {
		num_samples_in_BACCH_output = RS3D_BACCH_FRONT_LEFT_RIGHT->get_num_samples_in_output();
	}
	else if (BACCH_stereo_type == BACCH_STEREO_BACK && RS3D_BACCH_BACK_LEFT_RIGHT) {
		num_samples_in_BACCH_output = RS3D_BACCH_BACK_LEFT_RIGHT->get_num_samples_in_output();
	}
	else {
		num_samples_in_BACCH_output = 0;
	}

	//Add to map
	if (RS3D_BACCH_bus_ring_buffer_map.find(RS3D_BACCH_bus_key_left) == RS3D_BACCH_bus_ring_buffer_map.end()) {
		RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_left] = RS3D_NEW(RS3DRingBuffer<float>(num_samples_in_BACCH_output, 0, 0));
	}
	if (RS3D_BACCH_bus_ring_buffer_map.find(RS3D_BACCH_bus_key_right) == RS3D_BACCH_bus_ring_buffer_map.end()) {
		RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_right] = RS3D_NEW(RS3DRingBuffer<float>(num_samples_in_BACCH_output, 0, 0));
	}

	//Update left and right ring buffers
	if (BACCH_stereo_type == BACCH_STEREO_FRONT && RS3D_BACCH_FRONT_LEFT_RIGHT) {
		RS3D_BACCH_FRONT_LEFT_RIGHT->process(left_channel_output, right_channel_output,
			*RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_left], *RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_right]);
	}
	else if (BACCH_stereo_type == BACCH_STEREO_BACK && RS3D_BACCH_BACK_LEFT_RIGHT) {
		RS3D_BACCH_BACK_LEFT_RIGHT->process(left_channel_output, right_channel_output,
			*RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_left], *RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_right]);
	}
	RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_left]->readAndClear(num_samples_per_buffer, left_channel_output);
	RS3D_BACCH_bus_ring_buffer_map[RS3D_BACCH_bus_key_right]->readAndClear(num_samples_per_buffer, right_channel_output);
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::destroy_source_room_spatializer(AK::IAkMixerInputContext *	_mixer_input_context) {
#ifdef USE_STATE_MAP
	IAkMixerPluginContextKey mixer_input_context_key = (IAkMixerPluginContextKey)_mixer_input_context;
	if (RS3D_mixer_input_context_states_map.find(mixer_input_context_key) != RS3D_mixer_input_context_states_map.end()) {		//input context key found

		RS3DSpatializerKey *RS3D_spatializer_key = RS3D_mixer_input_context_states_map[mixer_input_context_key];

		//Delete spatializer in map
		RS3DSpatializerMap::iterator RS3D_spatializer_iterator;
		RS3D_spatializer_iterator = RS3D_spatializer_map.find(*RS3D_spatializer_key);
		if (RS3D_spatializer_iterator != RS3D_spatializer_map.end()) {	//Spatializer key exists
			RS3D_DELETE(RS3D_spatializer_iterator->second);
			RS3D_spatializer_map.erase(RS3D_spatializer_iterator);
		}

		//Delete source in map
		RS3DSourceMap::iterator RS3D_source_iterator;
		RS3D_source_iterator = RS3D_source_map.find(RS3D_spatializer_key->source_key);
		if(RS3D_source_iterator != RS3D_source_map.end()){	//Source key exists
			RS3D_DELETE(RS3D_source_iterator->second);
			RS3D_source_map.erase(RS3D_source_iterator);
		}

		//Delete room in map
		RS3DRoomMap::iterator RS3D_room_iterator;
		RS3D_room_iterator = RS3D_room_map.find(RS3D_spatializer_key->room_key);
		if(RS3D_room_iterator != RS3D_room_map.end()) {		//Room key exists
			RS3D_DELETE(RS3D_room_iterator->second);
			RS3D_room_map.erase(RS3D_room_iterator);
		}
		
		//Delete state from map
		RS3D_DELETE(RS3D_spatializer_key);
		RS3D_mixer_input_context_states_map.erase(mixer_input_context_key);
	}
	else {
		LOGD("destroy_source_room_spatializer mixer input context in map not found ");
	}
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DController::destroy_ambisonics_source(AK::IAkMixerInputContext *	_mixer_input_context) {
#ifdef USE_STATE_MAP
	IAkMixerPluginContextKey mixer_input_context_key = (IAkMixerPluginContextKey)_mixer_input_context;
	if (RS3D_mixer_input_context_states_amb_map.find(mixer_input_context_key) != RS3D_mixer_input_context_states_amb_map.end()) {		//input context key found

		RS3DSourceKey *RS3D_source_key = RS3D_mixer_input_context_states_amb_map[mixer_input_context_key];

		//Delete ambisonics in map
		RS3DAmbisonicsMap::iterator RS3D_ambisonics_iterator;
		RS3D_ambisonics_iterator = RS3D_ambisonics_map.find(*RS3D_source_key);
		if (RS3D_ambisonics_iterator != RS3D_ambisonics_map.end()) {	//Spatializer key exists
			RS3D_DELETE(RS3D_ambisonics_iterator->second);
			RS3D_ambisonics_map.erase(RS3D_ambisonics_iterator);
		}

		//Delete state from map
		RS3D_DELETE(RS3D_source_key);
		RS3D_mixer_input_context_states_amb_map.erase(mixer_input_context_key);
	}
	else {
		LOGD("destroy_ambisonics_source mixer input context in map not found ");
	}
#endif
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSourceKey RS3DController::generate_source_key() {
	return RS3DSourceKey(voice_info->GetGameObjectID(), mixer_input_context->GetAudioNodeID());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DListenerKey RS3DController::generate_listener_key() {
	return 0;																					//Temporary: Only 1 listener for now
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoomKey RS3DController::generate_room_key() {												
	//Temporary: room key idential to source-key
	return RS3DRoomKey(voice_info->GetGameObjectID(), mixer_input_context->GetAudioNodeID());
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializerKey RS3DController::generate_spatializer_key(RS3DSourceKey  RS3D_source_key, RS3DRoomKey  RS3D_room_key, RS3DListenerKey  RS3D_listener_key) {
	return RS3DSpatializerKey(RS3D_source_key, RS3D_room_key, RS3D_listener_key);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DController::is_voice_in_editor() {		//Use gameObjectID 0 test
	if (voice_info)
		return (voice_info->GetGameObjectID() == 0);
	else
		return false;
}

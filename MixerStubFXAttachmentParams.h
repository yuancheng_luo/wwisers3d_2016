/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerStubFXParams.h
//
//////////////////////////////////////////////////////////////////////

#ifndef _MIXERSTUB_ATTACHMENT_PARAMS_H_
#define _MIXERSTUB_ATTACHMENT_PARAMS_H_

#include <AK/Plugin/PluginServices/AkFXParameterChangeHandler.h>
#include <math.h>
#include <AK/Tools/Common/AkAssert.h>
#include <AK/SoundEngine/Common/IAkPlugin.h>

#include <iostream>
#include <string>

#include "RS3DCommon.h"

#define AKEFFECTID_MIXERSTUB 146
#define AKEFFECTID_MIXERSTUB_ATTACHMENT 501


using namespace std;

// Parameters IDs for the Wwise or RTPC.
enum MixerStubAttachmentParamID
{
	MIXER_STUB_ATTACH_BBYPASS = 0,
	MIXER_STUB_ATTACH_GAMEDEFINEDPARAMS,
	MIXER_STUB_ATTACH_REVERBLENGTH,
	MIXER_STUB_ATTACH_MAXREFL,
	MIXER_STUB_ATTACH_EARLYREFL,
	MIXER_STUB_ATTACH_ROOMSIZEX,
	MIXER_STUB_ATTACH_ROOMSIZEY,
	MIXER_STUB_ATTACH_ROOMSIZEZ,
	MIXER_STUB_ATTACH_ROOMCENTERX,
	MIXER_STUB_ATTACH_ROOMCENTERY,
	MIXER_STUB_ATTACH_ROOMCENTERZ,
	MIXER_STUB_ATTACH_ROOMSREFLLEFT,
	MIXER_STUB_ATTACH_ROOMSREFLRIGHT,
	MIXER_STUB_ATTACH_ROOMSREFLFRONT,
	MIXER_STUB_ATTACH_ROOMSREFLBACK,
	MIXER_STUB_ATTACH_ROOMSREFLFLOOR,
	MIXER_STUB_ATTACH_ROOMSREFLCEIL,
	MIXER_STUB_ATTACH_MINRANGE,
	MIXER_STUB_ATTACH_MAXRANGE,
	MIXER_STUB_ATTACH_PREVAZIM,
	MIXER_STUB_ATTACH_PREVELEV,
	MIXER_STUB_ATTACH_PREVDIST,
	MIXER_STUB_ATTACH_FASTSPATIAL,
	MIXER_STUB_ATTACH_SMOOTHDOPPLER,
	MIXER_STUB_MAX_ATTACH_PARAMS	// Keep last
};

//-----------------------------------------------------------------------------
// Structures.
//-----------------------------------------------------------------------------

// Structure of Mixer Stub parameters (on bus)
struct _MixerStubFXAttachmentParams
{
	bool bBypass;
	bool gameDefinedParams;

	float reverbLength;
	float maxNRefl;
	float earlyNRefl;

	float roomSizeX, roomSizeY, roomSizeZ;
	float roomCenterX, roomCenterY, roomCenterZ;
	float roomReflLeft, roomReflRight, roomReflFront, roomReflBack, roomReflFloor, roomReflCeil;

	float minRange, maxRange;

	float previewAzimuth, previewElevation, previewDist;

	bool fastSpatial;
	bool smoothDoppler;
};

//-----------------------------------------------------------------------------
// Name: class MixerStubFXParams
// Desc: Shared parameters implementation.
//-----------------------------------------------------------------------------
class MixerStubFXAttachmentParams : public AK::IAkPluginParam
{
public:

	friend class MixerStubFX;
 
    // Constructor/destructor.
    MixerStubFXAttachmentParams();
    ~MixerStubFXAttachmentParams();
	MixerStubFXAttachmentParams( const MixerStubFXAttachmentParams & in_rCopy );

    // Create duplicate.
    IAkPluginParam * Clone( AK::IAkPluginMemAlloc * in_pAllocator );

    // Init/Term.
    AKRESULT Init(	AK::IAkPluginMemAlloc *	in_pAllocator,						    
					const void *			in_pParamsBlock, 
					AkUInt32				in_ulBlockSize 
                         );
    AKRESULT Term( AK::IAkPluginMemAlloc * in_pAllocator );

    // Blob set.
    AKRESULT SetParamsBlock(	const void * in_pParamsBlock, 
								AkUInt32 in_ulBlockSize
                                );

    // Update one parameter.
    AKRESULT SetParam(	AkPluginParamID in_ParamID,
						const void * in_pValue, 
						AkUInt32 in_ulParamSize
                        );

	// Retrieve all parameters at once
	inline void GetParams( _MixerStubFXAttachmentParams & out_params ) { out_params = m_params; }

	AK::AkFXParameterChangeHandler<MIXER_STUB_MAX_ATTACH_PARAMS> m_paramChangeHandler;

private:
    _MixerStubFXAttachmentParams m_params;	// Parameter structure.
};

#endif // _MIXERSTUB_ATTACHMENT_PARAMS_H_

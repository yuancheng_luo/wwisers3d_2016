#pragma once


#include <AK/SoundEngine/Common/AkTypes.h>

#include "RS3DAllocator.h"

#include "vsengine.h"
#include "RS3DCommon.h"

struct Anthropometry{
	double head_radius;
	double torso_radius;
	double neck_height;
};

struct HRTFBlob {
	void *data;				//ptr to raw HRTF file data
	int data_size;			//size of HRTF_data in bytes
};

typedef int RS3DListenerKey;

////////////////////////////////////////////////////////////////////////////////////////////////////
class RS3DListener {
public:
	RS3DListener();
	RS3DListener(	RS3DListenerKey _listener_key,
					const Anthropometry & _anthropometry, 
					const HRTFBlob & _HRTF_blob,
					const std::string &_license_key );
	~RS3DListener();
	//Use default assignment operator	

	void init();

	void update_anthropometry(const Anthropometry & _anthropometry);					//Makes a local copy of anthropometry and sets reflectance parameters of anthropometry
	void update_HRTF_blob(const HRTFBlob & _HRTF_blob);									//Makes a shallow copy of HRTF blob
	void update_license_key(const std::string &_license_key);							//Makes a local copy of the license key

	void finialize_HRTF(int num_sample_per_buffer, int sample_rate, bool do_resample);	//Makes calls to vsengine to create HRTF_data and HRTF_hash

	void update_position(const AkVector & _position, float learning_rate);				//learning_rate [0, 1] range
	void update_orientation(const AkVector & _orientation, float learning_rate);		//learning_rate [0, 1] range
	
	RS3DListenerKey	get_listener_key() const;
	AkVector get_position() const;
	AkVector get_orientation() const;

	_hrtf_data get_HRTF_data() const;
	_hrtf_data* get_HRTF_data_ptr() const;

	_hrtf_hash get_HRTF_hash() const;
	_hrtf_hash* get_HRTF_hash_ptr() const;

private:

	RS3DListenerKey	listener_key;

	std::string		license_key;

	Anthropometry	anthropometry;
	HRTFBlob		HRTF_blob;

	_hrtf_data		HRTF_data;
	unsigned char * external_HRTF_data_mem_ptr;
	_hrtf_hash		HRTF_hash;
	unsigned char * external_HRTF_hash_mem_ptr;
	bool			is_HRTF_finalized;

	AkVector		position;														//Cartesian coordinates
	AkVector		orientation;													//Euler angles

};

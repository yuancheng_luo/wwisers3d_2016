/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

#include "MixerStubFXAttachmentParams.h"
#include <AK/Tools/Common/AkBankReadHelpers.h>


// Constructor/destructor.
MixerStubFXAttachmentParams::MixerStubFXAttachmentParams( )
{
}

MixerStubFXAttachmentParams::~MixerStubFXAttachmentParams( )
{
}

// Copy constructor.
MixerStubFXAttachmentParams::MixerStubFXAttachmentParams( const MixerStubFXAttachmentParams & in_rCopy )
{
	m_params = in_rCopy.m_params;
	m_paramChangeHandler.SetAllParamChanges();
}

// Create duplicate.
AK::IAkPluginParam * MixerStubFXAttachmentParams::Clone( AK::IAkPluginMemAlloc * in_pAllocator )
{
	AKASSERT( in_pAllocator != NULL );
	return AK_PLUGIN_NEW( in_pAllocator, MixerStubFXAttachmentParams( *this ) );
}

// Init/Term.
AKRESULT MixerStubFXAttachmentParams::Init(	AK::IAkPluginMemAlloc *	in_pAllocator,									   
										const void *			in_pParamsBlock, 
										AkUInt32				in_ulBlockSize )
{
	if ( in_ulBlockSize == 0)
	{
		//Particular to sound-sources
		m_params.bBypass = false;
		m_params.gameDefinedParams = true;

		m_params.reverbLength = 0;
		m_params.maxNRefl = 0;
		m_params.earlyNRefl = 3;

		m_params.roomSizeX = 10;	m_params.roomSizeY = 10;	m_params.roomSizeZ = 10;
		m_params.roomCenterX = 0;	m_params.roomCenterY = 0;	m_params.roomCenterZ = 0;
		m_params.roomReflLeft = m_params.roomReflRight = m_params.roomReflFloor = m_params.roomReflCeil = m_params.roomReflFront = m_params.roomReflBack = 75;
		m_params.minRange = 0;
		m_params.maxRange = 100000;
		
		m_params.previewAzimuth = 0;
		m_params.previewElevation = 0;
		m_params.previewDist = 1;

		m_params.fastSpatial = false;
		m_params.smoothDoppler = false;
		return AK_Success;
	}
	return SetParamsBlock( in_pParamsBlock, in_ulBlockSize );
}

AKRESULT MixerStubFXAttachmentParams::Term( AK::IAkPluginMemAlloc * in_pAllocator )
{
	AKASSERT( in_pAllocator != NULL );
	AK_PLUGIN_DELETE( in_pAllocator, this );
	return AK_Success;
}

// Blob set.
AKRESULT MixerStubFXAttachmentParams::SetParamsBlock(	const void * in_pParamsBlock, 
											AkUInt32 in_ulBlockSize
											)
{  
	AKRESULT eResult = AK_Success;
	
	AkUInt8 * pParamsBlock = (AkUInt8 *)in_pParamsBlock;

	m_params.bBypass = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);
	m_params.gameDefinedParams = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);

	m_params.reverbLength = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.maxNRefl = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.earlyNRefl = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.roomSizeX = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomSizeY = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomSizeZ = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.roomCenterX = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomCenterY = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomCenterZ = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.roomReflLeft = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomReflRight = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomReflFront = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomReflBack = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomReflFloor = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.roomReflCeil = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.minRange = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.maxRange = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.previewAzimuth = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.previewElevation = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.previewDist = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	m_params.fastSpatial = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);
	m_params.smoothDoppler = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);

	CHECKBANKDATASIZE( in_ulBlockSize, eResult );

	m_paramChangeHandler.SetAllParamChanges();
	
	if (eResult != AK_Success) {
		LOGE(std::string("MixerStubFXAttachmentParams::SetParamsBlock failure with " + SSTR(eResult) + ", in_ulBlockSize " + SSTR(in_ulBlockSize)).c_str());
	}

	return eResult;
}

// Update one parameter.
AKRESULT MixerStubFXAttachmentParams::SetParam(	AkPluginParamID in_ParamID,
										const void * in_pValue, 
										AkUInt32 in_ulParamSize )
{
	AKASSERT( in_pValue != NULL );
	if ( in_pValue == NULL )
	{
		return AK_InvalidParameter;
	}
	AKRESULT eResult = AK_Success;

	LOGD("MixerStubFXAttachmentParams::SetParam");

	switch ( in_ParamID )
	{
	case MIXER_STUB_ATTACH_BBYPASS:
	//	m_params.bBypass = (*reinterpret_cast<const RTPC_BOOL*>(in_pValue) != 0);	//For RTPC
		m_params.bBypass = (*(RTPC_BOOL*)(in_pValue)) != 0;							//For RTPC
		break;
	case MIXER_STUB_ATTACH_GAMEDEFINEDPARAMS:
		//	m_params.gameDefinedParams = (*reinterpret_cast<const RTPC_BOOL*>(in_pValue) != 0);		//For RTPC
		m_params.gameDefinedParams = (*(RTPC_BOOL*)(in_pValue)) != 0;				//For RTPC
		break;
	case MIXER_STUB_ATTACH_REVERBLENGTH:
//		m_params.reverbLength = *reinterpret_cast<const float*>(in_pValue);
		m_params.reverbLength = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_MAXREFL:
//		m_params.maxNRefl = *reinterpret_cast<const float*>(in_pValue);
		m_params.maxNRefl = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_EARLYREFL:
//		m_params.earlyNRefl  = *reinterpret_cast<const float*>(in_pValue);
		m_params.earlyNRefl = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSIZEX:
//		m_params.roomSizeX = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomSizeX = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSIZEY:
//		m_params.roomSizeY = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomSizeY = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSIZEZ:
//		m_params.roomSizeZ = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomSizeZ = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMCENTERX:
//		m_params.roomCenterX = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomCenterX = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMCENTERY:
//		m_params.roomCenterY = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomCenterY = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMCENTERZ:
//		m_params.roomCenterZ = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomCenterZ = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLLEFT:
//		m_params.roomReflLeft = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflLeft = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLRIGHT:
//		m_params.roomReflRight = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflRight = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLFRONT:
//		m_params.roomReflFront = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflFront = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLBACK:
//		m_params.roomReflBack = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflBack = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLFLOOR:
//		m_params.roomReflFloor = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflFloor = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_ROOMSREFLCEIL:
//		m_params.roomReflCeil = *reinterpret_cast<const float*>(in_pValue);
		m_params.roomReflCeil = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_MINRANGE:
//		m_params.minRange = *reinterpret_cast<const float*>(in_pValue);
		m_params.minRange = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_MAXRANGE:
//		m_params.maxRange = *reinterpret_cast<const float*>(in_pValue);
		m_params.maxRange = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_PREVAZIM:
//		m_params.previewAzimuth = *reinterpret_cast<const float*>(in_pValue);
		m_params.previewAzimuth = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_PREVELEV:
//		m_params.previewElevation = *reinterpret_cast<const float*>(in_pValue);
		m_params.previewElevation = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_PREVDIST:
//		m_params.previewDist = *reinterpret_cast<const float*>(in_pValue);
		m_params.previewDist = *(float*)(in_pValue);
		break;
	case MIXER_STUB_ATTACH_FASTSPATIAL:
//		m_params.fastSpatial = (*reinterpret_cast<const RTPC_BOOL*>(in_pValue) != 0);		//For RTPC
		m_params.fastSpatial = (*(RTPC_BOOL*)(in_pValue)) != 0;								//For RTPC
		break;
	case MIXER_STUB_ATTACH_SMOOTHDOPPLER:
//		m_params.smoothDoppler = (*reinterpret_cast<const RTPC_BOOL*>(in_pValue) != 0);					//For RTPC
		m_params.smoothDoppler = (*(RTPC_BOOL*)(in_pValue)) != 0;			//For RTPC
		break;

	default:
		AKASSERT(!"Invalid parameter.");
		eResult = AK_InvalidParameter;
	}
	m_paramChangeHandler.SetParamChange( in_ParamID );

	if (eResult != AK_Success) {
		LOGE(std::string("MixerStubFXAttachmentParams::SetParam failure with " + SSTR(eResult) + ", in_ParamID " + SSTR(in_ParamID) + ", in_ulParamSize " + SSTR(in_ulParamSize)).c_str());
	}

	return eResult;

}

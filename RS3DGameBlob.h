#pragma once

#include <vector>
#include <assert.h>

#include "AK/SoundEngine/Common/AkTypes.h"

#define AUTHORING_SIDE		//Comment out for game-side

#ifdef AUTHORING_SIDE
	#include "RS3DAllocator.h"
	#define RS3DGameBlobVector(T)		RS3DVector(T)			//Supports external allocator
#else
	#define RS3DGameBlobVector(T)		std::vector<T>
#endif


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//C-style structs
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RS3DGameDataStateHeader {
	int version_number;
	int tick;
	int num_rooms;
	int num_occluders;

	int computeExpectedBlobSize();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RS3DGameDataRoom {
	int room_ID;

	int order_early_reflection;
	int order_late_reflection;
	float reverb_length_ms;

	AkVector center;
	AkVector dimensions;

	float reflection_percent[6]; //Left, right, front, back, floor, ceil 

	bool vertex_in_room(const AkVector & position) const { 
		AkVector centered_position;
		centered_position.X = position.X - center.X; 
		centered_position.Y = position.Y - center.Y;
		centered_position.Z = position.Z - center.Z;

		return !(centered_position.X > dimensions.X / 2 || centered_position.X < -dimensions.X / 2 ||
			centered_position.Y > dimensions.Y / 2 || centered_position.Y < -dimensions.Y / 2 ||
			centered_position.Z > dimensions.Z / 2 || centered_position.Z < -dimensions.Z / 2);
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct RS3DGameDataSingleTriangleOccluder {
	int occluder_ID;

	AkVector vertex_position[3];
	float absorption_percent;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
inline void BLOB_READER(void *& ptr, T & data_out) {
	memcpy(&data_out, ptr, sizeof(T));
	ptr = (char*)ptr + sizeof(T);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
inline void BLOB_WRITER(void *& ptr, T & data_in) {
	memcpy(ptr, &data_in, sizeof(T));
	ptr = (char*)ptr + sizeof(T);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//C++ style 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//is passing entire class over boundary safe??? game-side and plugin-side C++ ABI must be comptaible, what if passed from non-c++ game-engines?
//Pass raw data only!
class RS3DGameBlob {
public:
		RS3DGameBlob();

		void addRoom(const RS3DGameDataRoom & _RS3D_game_data_room);
		void addSingleTriangleOccluder(const RS3DGameDataSingleTriangleOccluder & _RS3D_game_data_single_triangle_occluder);

		void updateRoom(const RS3DGameDataRoom & _RS3D_game_data_room, int idx);
		void updateSingleTriangleOccluder(const RS3DGameDataSingleTriangleOccluder & _RS3D_game_data_single_triangle_occluder, int idx);

		void sendData(void *& ptr, int & bytes_size);
		void sendData(void *& ptr, int & bytes_size, float mtxTransform[4][4]);		//Send data after applying matrix-transform to all positional vectors
		bool getData(void * ptr, int out_rDataSize);

		RS3DGameDataStateHeader RS3D_game_data_state_header;
		
		RS3DGameBlobVector(RS3DGameDataRoom)  RS3D_game_data_rooms;
		RS3DGameBlobVector(RS3DGameDataSingleTriangleOccluder) RS3D_game_data_single_triangle_occluders;
		RS3DGameBlobVector(char) data_buffer;
};
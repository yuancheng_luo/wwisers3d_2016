#include "RS3DRoom.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoomKey::RS3DRoomKey(AkGameObjectID _Ak_game_object_ID, int _sub_object_ID) {
	Ak_game_object_ID = _Ak_game_object_ID;
	sub_object_ID = _sub_object_ID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DRoomKey::operator<(const RS3DRoomKey& RS3D_room_key) const {
	if (Ak_game_object_ID < RS3D_room_key.Ak_game_object_ID)
		return true;
	else if (Ak_game_object_ID == RS3D_room_key.Ak_game_object_ID) {
		return sub_object_ID < RS3D_room_key.sub_object_ID;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DRoomKey::operator==(const RS3DRoomKey& RS3D_room_key) const {
	return (Ak_game_object_ID == RS3D_room_key.Ak_game_object_ID && sub_object_ID == RS3D_room_key.sub_object_ID);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoom::RS3DRoom()
{

	room_key = RS3DRoomKey(0, 0);
	room_ID = 0;

	scale_fac = 1;

	AkVector  _room_center = { 0.0, 0.0, 0.0 };
	update_center(_room_center);

	AkVector _room_dimensions = { 3.0, 4.0, 5.0 };
	update_dimensions(_room_dimensions);

	RoomWallReflectionCoefficients _room_wall_reflection_coefficients = { 0.75, 0.75, 0.75, 0.75, 0.75, 0.75 };
	update_wall_reflection_coefficients(_room_wall_reflection_coefficients);

	update_reflection_orders(3, 32);
	update_reverb_length_seconds(0.5);

	LOGD("RS3DRoom::RS3DRoom: WARNING, dummy constructor");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoom::RS3DRoom(RS3DRoomKey _room_key, float _scale_fac) {
	room_key = _room_key;
	scale_fac = _scale_fac;
	LOGD("RS3DRoom::RS3DRoom: WARNING, dummy constructor");
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoom::RS3DRoom(RS3DRoomKey _room_key,
					const AkVector & _room_center,
					const AkVector & _room_dimensions,
					const RoomWallReflectionCoefficients & _room_wall_reflection_coefficients,
					int _order_early_reflections, int _order_late_reflections,
					double _reverb_length_seconds,
					float _scale_fac) {

	room_key = _room_key;
	scale_fac = _scale_fac;

	room_ID = 0;

	update_center(_room_center);
	update_dimensions(_room_dimensions);
	update_wall_reflection_coefficients(_room_wall_reflection_coefficients);
	update_reflection_orders(_order_early_reflections, _order_late_reflections);
	update_reverb_length_seconds(_reverb_length_seconds);

	LOGD("RS3DRoom::RS3DRoom: SUCCESS");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoom::RS3DRoom(RS3DRoomKey _room_key, const RS3DGameDataRoom & _RS3D_game_data_room, float _scale_fac) {
	room_key = _room_key;
	scale_fac = _scale_fac;

	update_with_RS3DGameDataRoom(_RS3D_game_data_room);

	LOGD("RS3DRoom::RS3DRoom: SUCCESS");
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DRoomKey RS3DRoom::get_room_key() const{
	return room_key;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RS3DRoom::get_room_ID() const {
	return room_ID;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_center(const AkVector & _room_center) {
	center = _room_center;
	updateBounds();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_dimensions(const AkVector & _room_dimensions) {
	dimensions = _room_dimensions;

	float max_room_size_game_units = MAX_ROOM_SIZE_M * scale_fac;
	float min_room_size_game_units = 1 * scale_fac;
	dimensions.X = CLAMP(dimensions.X, max_room_size_game_units, min_room_size_game_units);
	dimensions.Y = CLAMP(dimensions.Y, max_room_size_game_units, min_room_size_game_units);
	dimensions.Z = CLAMP(dimensions.Z, max_room_size_game_units, min_room_size_game_units);

	updateBounds();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_wall_reflection_coefficients(const RoomWallReflectionCoefficients & _room_wall_reflection_coefficients) {
	room_wall_reflection_coefficients.left = CLAMP(_room_wall_reflection_coefficients.left, 1.0, 0.0);
	room_wall_reflection_coefficients.right = CLAMP(_room_wall_reflection_coefficients.right, 1.0, 0.0);
	room_wall_reflection_coefficients.front = CLAMP(_room_wall_reflection_coefficients.front, 1.0, 0.0);
	room_wall_reflection_coefficients.back = CLAMP(_room_wall_reflection_coefficients.back, 1.0, 0.0);
	room_wall_reflection_coefficients.floor = CLAMP(_room_wall_reflection_coefficients.floor, 1.0, 0.0);
	room_wall_reflection_coefficients.ceiling = CLAMP(_room_wall_reflection_coefficients.ceiling, 1.0, 0.0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void  RS3DRoom::update_reflection_orders(int _order_early_reflections, int _order_late_reflections) {
	order_early_reflections = CLAMP(_order_early_reflections, 4, 1);
	order_late_reflections = CLAMP(_order_late_reflections, 32, 0);

	if (order_late_reflections == 0)
		order_late_reflections = compute_Sabine_late_order_reflections(dimensions, room_wall_reflection_coefficients, scale_fac);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_reverb_length_seconds(double _reverb_length_seconds) {
	reverb_length_seconds = CLAMP(_reverb_length_seconds, 2, 0);

	if (reverb_length_seconds == 0)
		reverb_length_seconds = compute_Sabine_reverb_seconds(dimensions, room_wall_reflection_coefficients, scale_fac);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_with_RS3DGameDataRoom(const RS3DGameDataRoom & _RS3D_game_data_room) {
	room_ID = _RS3D_game_data_room.room_ID;

	update_center(_RS3D_game_data_room.center);
	update_dimensions(_RS3D_game_data_room.dimensions);

	RoomWallReflectionCoefficients _reflection_coefficients;
	_reflection_coefficients.left = _RS3D_game_data_room.reflection_percent[0] / 100.0;
	_reflection_coefficients.right = _RS3D_game_data_room.reflection_percent[1] / 100.0;
	_reflection_coefficients.front = _RS3D_game_data_room.reflection_percent[2] / 100.0;
	_reflection_coefficients.back = _RS3D_game_data_room.reflection_percent[3] / 100.0;
	_reflection_coefficients.floor = _RS3D_game_data_room.reflection_percent[4] / 100.0;
	_reflection_coefficients.ceiling = _RS3D_game_data_room.reflection_percent[5] / 100.0;

	update_wall_reflection_coefficients(_reflection_coefficients);
	update_reflection_orders(_RS3D_game_data_room.order_early_reflection, _RS3D_game_data_room.order_late_reflection);
	update_reverb_length_seconds(_RS3D_game_data_room.reverb_length_ms / 1000.0);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::update_with_universe_room(AkVector _room_center, int _room_ID) {
	room_ID = _room_ID;

	update_center(_room_center);
	
	AkVector  _room_size = { 1000000.0f * scale_fac , 1000000.0f * scale_fac , 1000000.0f * scale_fac };
	update_dimensions(_room_size);

	RoomWallReflectionCoefficients _reflection_coefficients = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	update_wall_reflection_coefficients(_reflection_coefficients);

	update_reflection_orders(0, 1);
	update_reverb_length_seconds(2.0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DRoom::get_center() const {
	return center;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DRoom::get_dimensions() const {
	return dimensions;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RoomWallReflectionCoefficients RS3DRoom::get_room_reflection_coefficients() const {
	return room_wall_reflection_coefficients;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RS3DRoom::get_order_early_reflections() const {
	return order_early_reflections;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RS3DRoom::get_order_late_reflections() const {
	return order_late_reflections;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RS3DRoom::get_reverb_length_seconds() const {
	return reverb_length_seconds;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DRoom::contains_point(const AkVector & position) const {
	AkVector centered_position = AkVectorExt::subtract(position, center);

	if (centered_position.X > dimensions.X / 2 || centered_position.X < -dimensions.X / 2 ||
		centered_position.Y > dimensions.Y / 2 || centered_position.Y < -dimensions.Y / 2 ||
		centered_position.Z > dimensions.Z / 2 || centered_position.Z < -dimensions.Z / 2)
		return false;
	else
		return true;

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float RS3DRoom::vertex_min_distance(const AkVector & position) {
	AkVector centered_position = AkVectorExt::subtract(position, center);

	float min_dist_X = std::min<float>(std::abs(dimensions.X / 2 - centered_position.X), std::abs(-dimensions.X / 2 - centered_position.X));
	float min_dist_Y = std::min<float>(std::abs(dimensions.Y / 2 - centered_position.Y), std::abs(-dimensions.Y / 2 - centered_position.Y));
	float min_dist_Z = std::min<float>(std::abs(dimensions.Z / 2 - centered_position.Z), std::abs(-dimensions.Z / 2 - centered_position.Z));

	return std::min<float>(min_dist_X, std::min<float>(min_dist_Y, min_dist_Z));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
float RS3DRoom::vertex_infimum_distance() {
	return std::min<float>(dimensions.X / 2, std::min<float>(dimensions.Y / 2, dimensions.Z / 2));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DRoom::intersects_segment(const AkVector & point_A, const AkVector & point_B) const {

	//Modified from ray-box test at  
	//http://people.csail.mit.edu/amy/papers/box-jgt.pdf

	float t0 = 0;
	float t1 = AkVectorExt::norm(AkVectorExt::subtract(point_B, point_A));
	AkVector ray_origin = point_A;
	AkVector ray_direction = {point_B.X - point_A.X, point_B.Y - point_A.Y, point_B.Z - point_A.Z};
	AkVector ray_inv_direction = {1 / ray_direction.X,  1 / ray_direction.Y, 1 / ray_direction.Z};
	bool ray_sign[3] = { ray_inv_direction.X < 0, ray_inv_direction.Y < 0, ray_inv_direction.Z < 0};


	float tmin = (bounds[ray_sign[0]].X - ray_origin.X) * ray_inv_direction.X;	
	float tmax = (bounds[1 - ray_sign[0]].X - ray_origin.X) * ray_inv_direction.X;

	float tymin = (bounds[ray_sign[0]].Y - ray_origin.Y) * ray_inv_direction.Y;
	float tymax = (bounds[1 - ray_sign[0]].Y - ray_origin.Y) * ray_inv_direction.Y;

	if ((tmin > tymax) || (tymin > tmax))
		return false;

	if (tymin > tmin)
		tmin = tymin;

	if (tymax < tmax)
		tmax = tymax;

	float tzmin = (bounds[ray_sign[0]].Z - ray_origin.Z) * ray_inv_direction.Z;
	float tzmax = (bounds[1 - ray_sign[0]].Z - ray_origin.Z) * ray_inv_direction.Z;

	if ((tmin > tzmax) || (tzmin > tmax))
		return false;
	
	if (tzmin > tmin)
		tmin = tzmin;

	if (tzmax < tmax)
		tmax = tzmax;

	return ((tmin < t1) && (tmax > t0));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DRoom::updateBounds() {
	bounds[0].X = center.X - dimensions.X / 2;
	bounds[1].X = center.X + dimensions.X / 2;

	bounds[0].Y = center.Y - dimensions.Y / 2;
	bounds[1].Y = center.Y + dimensions.Y / 2;

	bounds[0].Z = center.Z - dimensions.Z / 2;
	bounds[1].Z = center.Z + dimensions.Z / 2;

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DRoom::intersects_triangle(const AkVector & point_A, const AkVector & point_B, const AkVector & point_C) const {

	float box_center[3] = {center.X, center.Y, center.Z};
	float box_half_size[3] = { dimensions.X / 2, dimensions.Y / 2, dimensions.Z / 2};
	float triangle_vertices[3][3] = {	{point_A.X, point_A.Y, point_A.Z},
										{ point_B.X, point_B.Y, point_B.Z },
										{ point_C.X, point_C.Y, point_C.Z } }; //Columm majored

	return triBoxOverlap(box_center, box_half_size, triangle_vertices);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RS3DRoom::compute_Sabine_RT60(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac) {
	double _rd[3] = { room_size.X / scale_fac, room_size.Y / scale_fac, room_size.Z / scale_fac };
	double _rf[6] = {	room_wall_reflection_coefficients.left, 
						room_wall_reflection_coefficients.right,
						room_wall_reflection_coefficients.front, 
						room_wall_reflection_coefficients.back, 
						room_wall_reflection_coefficients.floor, 
						room_wall_reflection_coefficients.ceiling};

	double denom = (_rd[1] * _rd[2] * (1.0 - _rf[0])
		+ _rd[1] * _rd[2] * (1.0 - _rf[1]) + _rd[0] * _rd[2] * (1.0 - _rf[2])
		+ _rd[0] * _rd[2] * (1.0 - _rf[3]) + _rd[0] * _rd[1] * (1.0 - _rf[4])
		+ _rd[0] * _rd[1] * (1.0 - _rf[5]));	//Surface area
	double numer = _rd[0] * _rd[1] * _rd[2];	//Volume

	return(denom == 0 ? 1000000 : (0.16 * (numer / denom)));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RS3DRoom::compute_Sabine_reverb_seconds(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac) {
	double _RT60 = compute_Sabine_RT60(room_size, room_wall_reflection_coefficients, scale_fac);
	double reverb_length_seconds = .8 * _RT60;
	return CLAMP(reverb_length_seconds, 2.0, 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RS3DRoom::compute_Sabine_late_order_reflections(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac) {
	double _RT60 = compute_Sabine_RT60(room_size, room_wall_reflection_coefficients, scale_fac);
	double _rd[3] = { room_size.X / scale_fac, room_size.Y / scale_fac, room_size.Z / scale_fac };

	int order_late_reflection = (int)(0.8 * _RT60 * 343.0 / sqrt(_rd[0] * _rd[0]  + _rd[1] * _rd[1] + _rd[2] * _rd[2]));
	return CLAMP(order_late_reflection, 32, 1);
	
}

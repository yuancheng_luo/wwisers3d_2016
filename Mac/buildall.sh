 #!/bin/bash

xcodebuild -scheme "Debug_MixerStubFXMac"
xcodebuild -scheme "Debug_StaticCRT_MixerStubFXMac"
xcodebuild -scheme "Profile_MixerStubFXMac"
xcodebuild -scheme "Profile_StaticCRT_MixerStubFXMac"
xcodebuild -scheme "Release_MixerStubFXMac"
xcodebuild -scheme "Release_StaticCRT_MixerStubFXMac"

cp ../../../Mac/Debug/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Debug
cp ../../../Mac/Debug_StaticCRT/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Debug_StaticCRT
cp ../../../Mac/Profile/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Profile
cp ../../../Mac/Profile_StaticCRT/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Profile_StaticCRT
cp ../../../Mac/Release/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Release
cp ../../../Mac/Release_StaticCRT/lib/libRS3D_WWISE_FX.a /Users/q_trian/Dropbox/Mike_Wwise/libs/Mac/x86_64/Release_StaticCRT



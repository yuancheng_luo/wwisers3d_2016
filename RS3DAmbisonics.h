#include <string>

#include <AK/SoundEngine/Common/AkTypes.h>
#include "AkVectorExt.h"

#include "RS3DCommon.h"

#include "RS3DAllocator.h"

#include "RS3DRoom.h"
#include "RS3DSource.h"
#include "RS3DListener.h"

#include "RS3DSpatializer.h"

#include "RS3DTailMgr.h"


///////////////////////////////////////////////////
struct RS3DMatrix{
	float *data;
	int M;		//Rows
	int N;		//Cols
	
	////////////////////////////////
	RS3DMatrix() :
		M(0), 
		N(0)
	{
		data = NULL;
	}
	////////////////////////////////
	RS3DMatrix(int _M = 0, int _NN = 0) : 
		M(_M), 
		N(_NN){
		data = (float*)RS3D_ALLOC(sizeof(float) * _M * _NN );
	}
	////////////////////////////////
	~RS3DMatrix() {
		if(data)
			RS3D_FREE(data);
	}
	////////////////////////////////
	inline float & dat(int i, int j) {
		return data[((i)* (N)+(j))];
	}

	////////////////////////////////
	inline float dat(int i, int j) const {
		return data[((i)* (N)+(j))];
	}
};

//Matrix math
#define M_IDX(i, j, M)		((i) * (M) + (j))		//i is row, j is column, M is columns per row			  
bool mat_cholesky_decomp(const RS3DMatrix & A, RS3DMatrix & L);
bool mat_mat_product(const RS3DMatrix & A, const RS3DMatrix & B, RS3DMatrix & C);
bool mat_gauss_elimination(const RS3DMatrix & L, const RS3DMatrix & A, RS3DMatrix & X, bool L_transposed);
bool mat_transpose(const RS3DMatrix & A, RS3DMatrix & AT);

////////////////////////////////////////////////////////////////////////////////////////
//Ambisonics
#define AMBI_MAX_ORDER				3

enum RS3D_AMBI_GRID {
	AMBI_GRID_CUBE,						//8 vertices
	AMBI_GRID_ICOSAHEDRON,				//12 vertices
	AMBI_GRID_DODECAHEDRON,				//20 vertices
	AMBI_GRID_CUSTOM
};
////////////////////////////////////////////
struct RS3DSpeakerGrid{
	RS3D_AMBI_GRID grid_type;
	int num_speakers;
	RS3DVector(AkVector) spk_vertex_list;

	RS3DSpeakerGrid(RS3D_AMBI_GRID _grid_type = AMBI_GRID_DODECAHEDRON) {
		grid_type = _grid_type;
		float u = sqrt(1.0 / 3.0);
		float gr = (1.0 + sqrt(5.0)) / 2.0;
		
		AkVector v;

		switch (grid_type) {
		case AMBI_GRID_CUBE:
			num_speakers = 8;
			v.X = u;		v.Y = u;		v.Z = u;		spk_vertex_list.push_back(v);
			v.X = u;		v.Y = u;		v.Z = -u;		spk_vertex_list.push_back(v);
			v.X = u;		v.Y = -u;		v.Z = u;		spk_vertex_list.push_back(v);
			v.X = u;		v.Y = -u;		v.Z = -u;		spk_vertex_list.push_back(v);
			
			v.X = -u;		v.Y = u;		v.Z = u;		spk_vertex_list.push_back(v);
			v.X = -u;		v.Y = u;		v.Z = -u;		spk_vertex_list.push_back(v);
			v.X = -u;		v.Y = -u;		v.Z = u;		spk_vertex_list.push_back(v);
			v.X = -u;		v.Y = -u;		v.Z = -u;		spk_vertex_list.push_back(v);

			//spk_vertex_list.push_back({ u, u, u });
			//spk_vertex_list.push_back({ u, u, -u });
			//spk_vertex_list.push_back({ u, -u, u });
			//spk_vertex_list.push_back({ u, -u, -u });
			//spk_vertex_list.push_back({ -u, u, u });
			//spk_vertex_list.push_back({ -u, u, -u });
			//spk_vertex_list.push_back({ -u, -u, u });
			//spk_vertex_list.push_back({ -u, -u, -u });
			break;
		case AMBI_GRID_ICOSAHEDRON:
			num_speakers = 12;
			v.X = 0;		v.Y = 1;		v.Z = gr;				spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = 1;		v.Z = -gr;				spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = -1;		v.Z = gr;				spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = -1;		v.Z = -gr;				spk_vertex_list.push_back(v);

			v.X = 1;		v.Y = 0;		v.Z = gr;				spk_vertex_list.push_back(v);
			v.X = 1;		v.Y = 0;		v.Z = -gr;				spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = 0;		v.Z = gr;				spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = 0;		v.Z = -gr;				spk_vertex_list.push_back(v);

			v.X = 1;		v.Y = gr;		v.Z = 0;				spk_vertex_list.push_back(v);
			v.X = 1;		v.Y = -gr;		v.Z = 0;				spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = gr;		v.Z = 0;				spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = -gr;		v.Z = 0;				spk_vertex_list.push_back(v);

			//spk_vertex_list.push_back({0, 1, gr });
			//spk_vertex_list.push_back({ 0, 1, -gr });
			//spk_vertex_list.push_back({ 0, -1, gr });
			//spk_vertex_list.push_back({ 0, -1, -gr });

			//spk_vertex_list.push_back({ 1, 0, gr });
			//spk_vertex_list.push_back({ 1, 0, -gr });
			//spk_vertex_list.push_back({ -1, 0, gr });
			//spk_vertex_list.push_back({ -1, 0, -gr });

			//spk_vertex_list.push_back({ 1, gr, 0 });
			//spk_vertex_list.push_back({ 1, -gr, 0 });
			//spk_vertex_list.push_back({ -1, gr, 0 });
			//spk_vertex_list.push_back({ -1, -gr, 0 });
			break;
		case AMBI_GRID_DODECAHEDRON:
			num_speakers = 20;
			v.X = 1;		v.Y = 1;		v.Z = 1;		spk_vertex_list.push_back(v);
			v.X = 1;		v.Y = 1;		v.Z = -1;		spk_vertex_list.push_back(v);
			v.X = 1;		v.Y = -1;		v.Z = 1;		spk_vertex_list.push_back(v);
			v.X = 1;		v.Y = -1;		v.Z = -1;		spk_vertex_list.push_back(v);

			v.X = -1;		v.Y = 1;		v.Z = 1;		spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = 1;		v.Z = -1;		spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = -1;		v.Z = 1;		spk_vertex_list.push_back(v);
			v.X = -1;		v.Y = -1;		v.Z = -1;		spk_vertex_list.push_back(v);

			v.X = 0;		v.Y = 1 / gr;		v.Z = gr;		spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = 1 / gr;		v.Z = -gr;		spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = -1 / gr;		v.Z = gr;		spk_vertex_list.push_back(v);
			v.X = 0;		v.Y = -1 / gr;		v.Z = -gr;		spk_vertex_list.push_back(v);

			v.X = gr;		v.Y = 0;		v.Z = 1 / gr;		spk_vertex_list.push_back(v);
			v.X = gr;		v.Y = 0;		v.Z = -1 / gr;		spk_vertex_list.push_back(v);
			v.X = -gr;		v.Y = 0;		v.Z = 1 / gr;		spk_vertex_list.push_back(v);
			v.X = -gr;		v.Y = 0;		v.Z = -1 / gr;		spk_vertex_list.push_back(v);

			v.X = 1 / gr;		v.Y = gr;		v.Z = 0;		spk_vertex_list.push_back(v);
			v.X = 1 / gr;		v.Y = -gr;		v.Z = 0;		spk_vertex_list.push_back(v);
			v.X = -1 / gr;		v.Y = gr;		v.Z = 0;		spk_vertex_list.push_back(v);
			v.X = -1 / gr;		v.Y = -gr;		v.Z = 0;		spk_vertex_list.push_back(v);

/*
			spk_vertex_list.push_back({ 1, 1, 1 });
			spk_vertex_list.push_back({1, 1, -1});
			spk_vertex_list.push_back({1, -1, 1});
			spk_vertex_list.push_back({1, -1, -1});

			spk_vertex_list.push_back({-1, 1, 1});
			spk_vertex_list.push_back({-1, 1, -1});
			spk_vertex_list.push_back({-1, -1, 1});
			spk_vertex_list.push_back({-1, -1, -1});

			spk_vertex_list.push_back({0, 1 / gr, gr});
			spk_vertex_list.push_back({0, 1 / gr, -gr});
			spk_vertex_list.push_back({0, -1 / gr, gr});
			spk_vertex_list.push_back({0, -1 / gr, -gr});

			spk_vertex_list.push_back({gr, 0, 1 / gr});
			spk_vertex_list.push_back({gr, 0, -1 / gr});
			spk_vertex_list.push_back({-gr, 0, 1 / gr}); 
			spk_vertex_list.push_back({-gr, 0, -1 / gr});

			spk_vertex_list.push_back({1 / gr, gr, 0});
			spk_vertex_list.push_back({1 / gr, -gr, 0});
			spk_vertex_list.push_back({-1 / gr, gr, 0});
			spk_vertex_list.push_back({-1 / gr, -gr, 0});*/
			break;
		case AMBI_GRID_CUSTOM:
			num_speakers = 0;
			LOGE("RS3DSpeakerGrid::RS3DSpeakerGrid AMBI_GRID_CUSTOM unsuported");
			break;
		};
	}
};


//Creates a variable number of virtual loudspeakers to render decoded Ambisonic channels to RS3DSpatializers
class RS3DAmbisonics {	
	public:
		RS3DAmbisonics(	int _ambisonics_order = 1, RS3D_AMBI_GRID _amb_spk_grid_type = AMBI_GRID_DODECAHEDRON, float _speaker_radius = 1,
						const RS3DListener & _RS3D_listener = RS3DListener(),
						const RS3DRoom & _RS3D_room = RS3DRoom(),
						int _num_samples_buffer = 1024,
						int _sample_rate = 48000,
						double _unit_scale_factor = 1,
						RS3DTailMgr * _RS3D_tail_mgr = NULL);
		~RS3DAmbisonics();


		void decode_to_binaural(const RS3DVector(const AkReal32 *) & input_buffer_list,
			RS3DFloatVector & output_buffer_left, RS3DFloatVector & output_buffer_right);


		void set_ambisonics_format(int _ambisonics_order, RS3D_AMBI_GRID _amb_spk_grid_type);
		void set_room(const RS3DRoom & _RS3D_room);

	private:

		const int num_samples_buffer;
		const int sample_rate;
		float unit_scale_factor;

		float speaker_radius;

		RS3DTailMgr * RS3D_tail_mgr;

		RS3DVector(RS3DSource*) RS3D_source_list;
		const RS3DListener & RS3D_listener;
		RS3DRoom RS3D_room;
		RS3DVector(RS3DSpatializer*) RS3D_spatializer_list;


		int ambisonics_order;	//	0 to 3 for now
		int num_ambisonics_channels;
		int num_speakers;		//	>= # ambisonics input channels

		RS3DSpeakerGrid spk_grid;

		RS3DMatrix *CTCCTinv;
		RS3DMatrix *input_matrix;
		RS3DMatrix *decoded_matrix;

		bool decode(const RS3DVector(const AkReal32 *) & input_buffer_list);

		void center_sources_to_listener();

		void assemble_decoding_coefficient_mat();			//Compute decoding coefficient matrix using current source info

		float compute_FuMa_coeffs(float azim, float elev, char channel_label);	//azim, elev are speaker directions w.r.t. listener position and default head-orientation
		float compute_FuMa_coeffs(AkVector spk_dir, char channel_label);		

		void clear_source_spatializer_lists();

};

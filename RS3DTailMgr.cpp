#include "RS3DTailMgr.h"

//////////////////////////////////////////////////////////////////////
RS3DTail::RS3DTail() : 
	tail_descriptor()
{

}

//////////////////////////////////////////////////////////////////////
RS3DTail::RS3DTail(const RS3DGameDataRoom & _RS3D_game_data_room, const _source_information & _source_info, const _source_work_area & _work_area){
	setTail(_RS3D_game_data_room, _source_info, _work_area);
}

//////////////////////////////////////////////////////////////////////
void RS3DTail::setTail(const RS3DGameDataRoom & _RS3D_game_data_room, const _source_information & _source_info, const _source_work_area & _work_area) {
	
	RS3D_game_data_room = _RS3D_game_data_room;
	
	//Setup tail descriptor and copy tail data from work_area
	tail_descriptor._data_chunk_size = _source_info._data_chunk_size;
	tail_descriptor._rir_length = _source_info._rir_length;
	tail_descriptor._rir_realtime_order = _source_info._rir_realtime_order;
	tail_descriptor._rir_fixdtail_order = _source_info._rir_fixdtail_order;

	for (int i = 0; i < 3; ++i) {
		tail_descriptor._listnr_ixyz[i] = _source_info._listnr_ixyz[i];
		tail_descriptor._roomxe_size[i] = _source_info._roomxe_size[i];
	}

	for (int i = 0; i < 6; ++i) {
		tail_descriptor._rflctn_coef[i] = _source_info._rflctn_coef[i][0];
	}

	//Delete and copy blocks of rir into tail_data
	int num_blocks = _source_info._rir_length / _source_info._data_chunk_size;
	tail_data.resize(2 * _source_info._rir_length);
	for (int i = 0; i < num_blocks; ++i) {
		memcpy(&tail_data[2 * i * _source_info._data_chunk_size], &_work_area._rirx[i][0], 2 * sizeof(fftwf_complex) * _source_info._data_chunk_size);
	}
}


//////////////////////////////////////////////////////////////////////
void RS3DTail::getTailBlob(unsigned char *& tail_blob, int & tail_blob_size) {
	tail_blob_size = 256 + 2 * tail_descriptor._rir_length * sizeof(fftwf_complex);

	tail_blob = (unsigned char *)RS3D_ALLOC(tail_blob_size);

	memset(tail_blob, 1, tail_blob_size);
	memcpy(&tail_blob[0], &tail_descriptor, sizeof(TailDescriptor));
	memcpy(&tail_blob[256], &tail_data[0], 2 * tail_descriptor._rir_length * sizeof(fftwf_complex));
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
RS3DTailMgr::RS3DTailMgr(const RS3DListener & _RS3D_listener,
	int _num_samples_in_buffer,
	int _sample_rate,
	float _unit_scale_factor) :
	RS3D_listener(_RS3D_listener),
	num_samples_per_buffer(_num_samples_in_buffer),
	sample_rate(_sample_rate),
	unit_scale_factor(_unit_scale_factor)
{
	update_tails = false;

#ifdef NO_THREAD_MUTEX
#else
	thread_active = false;
	create_thread = NULL;
#endif

}
//////////////////////////////////////////////////////////////////////
RS3DTailMgr::~RS3DTailMgr() {

#ifdef NO_THREAD_MUTEX
#else
	if (create_thread && create_thread->joinable())
		create_thread->join();
#endif

	//Delete tail map
	RS3DTailMap::iterator RS3D_tail_iterator;
	for (RS3D_tail_iterator = threaded_RS3D_tail_map.begin(); RS3D_tail_iterator != threaded_RS3D_tail_map.end(); RS3D_tail_iterator++) {
		RS3D_DELETE(RS3D_tail_iterator->second);
	}

	//Delete game room data maps
	delete_game_data_room_map();

}
//////////////////////////////////////////////////////////////////////
void RS3DTailMgr::delete_game_data_room_map() {
	RS3DGameDataRoomMap::iterator RS3D_game_tail_iterator;
	for (RS3D_game_tail_iterator = RS3D_game_data_room_map.begin(); RS3D_game_tail_iterator != RS3D_game_data_room_map.end(); RS3D_game_tail_iterator++) {
		RS3D_DELETE(RS3D_game_tail_iterator->second);
	}
	RS3D_game_data_room_map.clear();
	for (RS3D_game_tail_iterator = threaded_RS3D_game_data_room_map.begin(); RS3D_game_tail_iterator != threaded_RS3D_game_data_room_map.end(); RS3D_game_tail_iterator++) {
		RS3D_DELETE(RS3D_game_tail_iterator->second);
	}
	threaded_RS3D_game_data_room_map.clear();
}


//////////////////////////////////////////////////////////////////////
bool RS3DTailMgr::setTailsToGameBlobRooms(const RS3DGameBlob & RS3D_game_blob) {
	
	//Check if tails need to updated
	if (RS3D_game_blob.RS3D_game_data_state_header.num_rooms != RS3D_game_data_room_map.size())
		update_tails = true;

	for (int i = 0; i < RS3D_game_blob.RS3D_game_data_state_header.num_rooms; ++i) {
		RS3DTailKey RS3D_tail_key = RS3D_game_blob.RS3D_game_data_rooms[i].room_ID;
		if (RS3D_game_data_room_map.find(RS3D_tail_key) == RS3D_game_data_room_map.end()) {
			//Room ID not found, add room
			update_tails = true;
			break;
		}
		else {
			//Room ID found, check room equivalence
			if (memcmp(RS3D_game_data_room_map[RS3D_tail_key], &RS3D_game_blob.RS3D_game_data_rooms[i], sizeof(RS3DGameDataRoom)) != 0) {
				//Room mismatch
				update_tails = true;
				break;
			}
		}
	}


#ifdef NO_THREAD_MUTEX
	//Set game_data_room_map and threaded game data maps to game data rooms
	delete_game_data_room_map();
	for (int i = 0; i < RS3D_game_blob.RS3D_game_data_state_header.num_rooms; ++i) {
		RS3DTailKey RS3D_tail_key = RS3D_game_blob.RS3D_game_data_rooms[i].room_ID;
		RS3D_game_data_room_map[RS3D_tail_key] = RS3D_NEW(RS3DGameDataRoom());
		memcpy(RS3D_game_data_room_map[RS3D_tail_key], &RS3D_game_blob.RS3D_game_data_rooms[i], sizeof(RS3DGameDataRoom));

		threaded_RS3D_game_data_room_map[RS3D_tail_key] = RS3D_NEW(RS3DGameDataRoom());
		memcpy(threaded_RS3D_game_data_room_map[RS3D_tail_key], &RS3D_game_blob.RS3D_game_data_rooms[i], sizeof(RS3DGameDataRoom));
	}
	update_tails = false;	//Reset flag 

	threadedAddTailToMap();
	return true;
#else
	if (update_tails && !thread_active) {
		if (mgr_mutex.try_lock()) {//Acquired lock
			mgr_mutex.unlock();

			if (create_thread && create_thread->joinable())
				create_thread->join();

			//Set game_data_room_map and threaded game data maps to game data rooms
			delete_game_data_room_map();
			for (int i = 0; i < RS3D_game_blob.RS3D_game_data_state_header.num_rooms; ++i) {
				RS3DTailKey RS3D_tail_key = RS3D_game_blob.RS3D_game_data_rooms[i].room_ID;
				RS3D_game_data_room_map[RS3D_tail_key] = RS3D_NEW(RS3DGameDataRoom());
				memcpy(RS3D_game_data_room_map[RS3D_tail_key], &RS3D_game_blob.RS3D_game_data_rooms[i], sizeof(RS3DGameDataRoom));

				threaded_RS3D_game_data_room_map[RS3D_tail_key] = RS3D_NEW(RS3DGameDataRoom());
				memcpy(threaded_RS3D_game_data_room_map[RS3D_tail_key], &RS3D_game_blob.RS3D_game_data_rooms[i], sizeof(RS3DGameDataRoom));
			}
			update_tails = false;	//Reset flag 

			if (create_thread) {
				RS3D_DELETE(create_thread);
				create_thread = NULL;
			}

			thread_active = true;
			create_thread = RS3D_NEW(std::thread(&RS3DTailMgr::threadedAddTailToMap, this));
			if (!create_thread) {
				LOGM(std::string("RealSpace3D Plugin room thread new failed. Reverting to system new. Please expand Wwise memory pool.").c_str());
				create_thread = new std::thread(&RS3DTailMgr::threadedAddTailToMap, this);
			}
			LOGD("RS3DTailMgr::setTailsToGameBlobRooms updating tails");
			return true;
		}
	}
#endif


	return false;
}


//////////////////////////////////////////////////////////////////////
bool RS3DTailMgr::getTailBlob(RS3DTailKey RS3D_tail_key, unsigned char *& tail_blob, int & tail_blob_size) {
#ifdef NO_THREAD_MUTEX
	if (threaded_RS3D_tail_map.find(RS3D_tail_key) != threaded_RS3D_tail_map.end()) { 	//Tail found
		threaded_RS3D_tail_map[RS3D_tail_key]->getTailBlob(tail_blob, tail_blob_size);
		return true;
}else {	//Tail not found
		tail_blob = NULL;
		tail_blob_size = 0;
		return false;
	}
#else
	if (mgr_mutex.try_lock()) { //Acquired lock
		mgr_mutex.unlock();

		//if (create_thread && create_thread->joinable())
		//	create_thread->join();

		if (threaded_RS3D_tail_map.find(RS3D_tail_key) != threaded_RS3D_tail_map.end()) { 	//Tail found
			threaded_RS3D_tail_map[RS3D_tail_key]->getTailBlob(tail_blob, tail_blob_size);
			return true;
		}
		else {	//Tail not found
			tail_blob = NULL;
			tail_blob_size = 0;
			return false;
		}
	}
	else {	//Could not acquire lock
		tail_blob = NULL;
		tail_blob_size = 0;
		return false;
	}
#endif
}


//////////////////////////////////////////////////////////////////////
void RS3DTailMgr::threadedAddTailToMap()
{
#ifdef NO_THREAD_MUTEX
#else
	mgr_mutex.lock();
#endif

	//Iterate over all tails in threaded_RS3D_tail_map and compare game blobs with threaded_RS3D_game_data_room_map
	RS3DTailMap::iterator RS3D_tail_iterator;
	for (RS3D_tail_iterator = threaded_RS3D_tail_map.begin(); RS3D_tail_iterator != threaded_RS3D_tail_map.end(); RS3D_tail_iterator++) {
		RS3DTailKey RS3D_tail_key = RS3D_tail_iterator->first;
		if (threaded_RS3D_game_data_room_map.find(RS3D_tail_key) != threaded_RS3D_game_data_room_map.end()) {
			//Key found in game data map, compare game data rooms
			if (memcmp(&threaded_RS3D_game_data_room_map[RS3D_tail_key], &threaded_RS3D_tail_map[RS3D_tail_key]->RS3D_game_data_room, sizeof(RS3DGameDataRoom)) != 0) {
				//Rooms are different, delete old tail and create new one using new game data room
				RS3D_DELETE(threaded_RS3D_tail_map[RS3D_tail_key]);
				threaded_RS3D_tail_map[RS3D_tail_key] = RS3DTailMgr::threadedCreateTail(RS3D_tail_key);
			}
		}
		else {
			//Key not found in game data blob, erase this tail
			RS3D_DELETE(threaded_RS3D_tail_map[RS3D_tail_key]);
			threaded_RS3D_tail_map.erase(RS3D_tail_key);
		}
	}


	//Iterate over all tails in threaded_RS3D_game_data_room_map and compare game blobs with threaded_RS3D_tail_map
	RS3DGameDataRoomMap::iterator RS3D_game_tail_iterator;
	for (RS3D_game_tail_iterator = threaded_RS3D_game_data_room_map.begin(); RS3D_game_tail_iterator != threaded_RS3D_game_data_room_map.end(); RS3D_game_tail_iterator++) {
		RS3DTailKey RS3D_tail_key = RS3D_game_tail_iterator->first;
		if (threaded_RS3D_tail_map.find(RS3D_tail_key) == threaded_RS3D_tail_map.end()) {
			//Key not found, add tail
			threaded_RS3D_tail_map[RS3D_tail_key] = RS3DTailMgr::threadedCreateTail(RS3D_tail_key);
		}
	}


#ifdef NO_THREAD_MUTEX
#else
	thread_active = false;
	mgr_mutex.unlock();
#endif

}
//////////////////////////////////////////////////////////////////////
RS3DTail * RS3DTailMgr::threadedCreateTail(RS3DTailKey RS3D_tail_key) {
	RS3DRoom RS3D_room(RS3DRoomKey(1, 2), *threaded_RS3D_game_data_room_map[RS3D_tail_key], unit_scale_factor);
	RS3D_room.update_with_RS3DGameDataRoom(*threaded_RS3D_game_data_room_map[RS3D_tail_key]);

	RS3DSource RS3D_source(RS3DSourceKey(1, 2));
	RS3D_source.update_position(RS3D_room.get_center(), 1.0);
	RS3D_source.update_min_dist(0);
	RS3D_source.update_max_dist(100000);

	RS3DSpatializer RS3D_spatializer(
		RS3D_source,
		RS3D_room,
		RS3D_listener,
		NULL,
		num_samples_per_buffer,
		sample_rate,
		REGULAR,
		false,
		1.0 / unit_scale_factor);

	//////Get RS3DSpatializer's source_info and tail_data
	_source_information source_info = RS3D_spatializer.get_copy_active_source_info();
	_source_work_area work_area = RS3D_spatializer.get_copy_active_work_area();

	return RS3D_NEW(RS3DTail(*threaded_RS3D_game_data_room_map[RS3D_tail_key], source_info, work_area));
}

#include "RS3DAmbisonics.h"

//////////////////////////////////////////
RS3DAmbisonics::RS3DAmbisonics(int _ambisonics_order, RS3D_AMBI_GRID _amb_spk_grid_type, float _speaker_radius,
	const RS3DListener & _RS3D_listener,
	const RS3DRoom & _RS3D_room,
	int _num_samples_buffer, int _sample_rate, double _unit_scale_factor,
	RS3DTailMgr *_RS3D_tail_mgr) :
	speaker_radius(_speaker_radius),
	RS3D_listener(_RS3D_listener),
	num_samples_buffer(_num_samples_buffer),
	sample_rate(_sample_rate),
	unit_scale_factor(_unit_scale_factor),
	RS3D_tail_mgr(_RS3D_tail_mgr)
{

	CTCCTinv = NULL;
	input_matrix = NULL;
	decoded_matrix = NULL;

	set_room(_RS3D_room);
	set_ambisonics_format(_ambisonics_order, _amb_spk_grid_type);
}
//////////////////////////////////////////
RS3DAmbisonics::~RS3DAmbisonics() {
	if (CTCCTinv)
		RS3D_DELETE(CTCCTinv);

	if(input_matrix)
		RS3D_DELETE(input_matrix);
	
	if (decoded_matrix)
		RS3D_DELETE(decoded_matrix);

	clear_source_spatializer_lists();
}
//////////////////////////////////////////
void RS3DAmbisonics::set_room(const RS3DRoom & _RS3D_room) {
	RS3D_room = _RS3D_room;
}

//////////////////////////////////////////
void RS3DAmbisonics::set_ambisonics_format(int _ambisonics_order, RS3D_AMBI_GRID _amb_spk_grid_type) {

	ambisonics_order = CLAMP(_ambisonics_order, AMBI_MAX_ORDER, 0);
	num_ambisonics_channels = (ambisonics_order + 1) * (ambisonics_order + 1);

	spk_grid = RS3DSpeakerGrid(_amb_spk_grid_type);
	num_speakers = spk_grid.num_speakers;

	if (input_matrix)
		RS3D_DELETE(input_matrix);
	input_matrix = RS3D_NEW(RS3DMatrix(num_ambisonics_channels, num_samples_buffer));

	if(decoded_matrix)
		RS3D_DELETE(decoded_matrix);
	decoded_matrix = RS3D_NEW(RS3DMatrix(num_speakers, num_samples_buffer));

	if (CTCCTinv)
		RS3D_DELETE(CTCCTinv);
	CTCCTinv = RS3D_NEW(RS3DMatrix(num_speakers, num_ambisonics_channels));


	clear_source_spatializer_lists();
	//Create sound-source and spatializers from speaker grid
	for (int i = 0; i < spk_grid.num_speakers; ++i) 
		RS3D_source_list.push_back(RS3D_NEW(RS3DSource(RS3DSourceKey(i, 0))));

	for (int i = 0; i < spk_grid.num_speakers; ++i)
		RS3D_spatializer_list.push_back(RS3D_NEW(RS3DSpatializer(*RS3D_source_list[i], RS3D_room, RS3D_listener, RS3D_tail_mgr, num_samples_buffer, sample_rate, 
			FAST_SPAT, false, unit_scale_factor)));

	center_sources_to_listener();
	assemble_decoding_coefficient_mat();
}
//////////////////////////////////////////
void RS3DAmbisonics::center_sources_to_listener() {
	//Center sources and rooms to listener position
	AkVector listener_position = RS3D_listener.get_position();

	RS3D_room.update_center(listener_position);
	for (int i = 0; i < RS3D_source_list.size(); ++i) {
		RS3D_source_list[i]->update_position(AkVectorExt::add(listener_position, AkVectorExt::scalar_mult(spk_grid.spk_vertex_list[i], speaker_radius)), 1.0);
	}
}

//////////////////////////////////////////
void RS3DAmbisonics::assemble_decoding_coefficient_mat() {

	RS3DMatrix C(num_ambisonics_channels, num_speakers);
	RS3DMatrix CT(num_speakers, num_ambisonics_channels);
	RS3DMatrix CCT(num_ambisonics_channels, num_ambisonics_channels);
	RS3DMatrix L(num_ambisonics_channels, num_ambisonics_channels);
	RS3DMatrix U(num_ambisonics_channels, num_ambisonics_channels);
	RS3DMatrix Q(num_ambisonics_channels, num_speakers);
	RS3DMatrix CTCCTinv_T(num_ambisonics_channels, num_speakers);

	////////////////////////////////////////////////////////////////////////////////
	char channel_order[] = "WXYZRSTUVKLMNOPQ";

	for (int i = 0; i < num_ambisonics_channels; ++i) {
		for (int j = 0; j < num_speakers; ++j) {
			AkVector spk_dir = AkVectorExt::subtract(RS3D_source_list[i]->get_position(), RS3D_listener.get_position());
			C.dat(i, j) = compute_FuMa_coeffs(spk_dir, channel_order[i]) / (float)(num_speakers);
		}
	}

	if (!mat_transpose(C, CT))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_transpose CT error");
	if (!mat_mat_product(C, CT, CCT))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_mat_product CCT error");
	if (!mat_cholesky_decomp(CCT, L))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_cholesky_decomp L error");
	if (!mat_transpose(L, U))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_transpose U error");
	if (!mat_gauss_elimination(L, C, Q, false))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_gauss_elimination Q error");
	if (!mat_gauss_elimination(U, Q, CTCCTinv_T, true))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_gauss_elimination CTCCTinv_T error");
	if (!mat_transpose(CTCCTinv_T, *CTCCTinv))
		LOGE("RS3DAmbisonics::assemble_decoding_coefficient_mat mat_transpose CTCCTinv error");
}

//////////////////////////////////////////
bool RS3DAmbisonics::decode(const RS3DVector(const AkReal32 *) & input_buffer_list) {

	if (input_buffer_list.size() != num_ambisonics_channels) {
		LOGE("RS3DAmbisonics::decode num input_buffer_list mismatch num_ambisonics_channels");
		return false;
	}

	//Copy input_buffer_list into input_matrix
	for (int i = 0; i < num_ambisonics_channels; ++i) {
		for (int j = 0; j < num_samples_buffer; ++j) {
			input_matrix->dat(i, j) = input_buffer_list[i][j];
		}
	}

	//Decode into input streams
	mat_mat_product(*CTCCTinv, *input_matrix, *decoded_matrix);
	return true;
}
//////////////////////////////////////////
void RS3DAmbisonics::clear_source_spatializer_lists() {
	for (int i = 0; i < RS3D_spatializer_list.size(); ++i) 
		RS3D_DELETE(RS3D_spatializer_list[i]);

	RS3D_spatializer_list.clear();

	for (int i = 0; i < RS3D_source_list.size(); ++i)
		RS3D_DELETE(RS3D_source_list[i]);

	RS3D_source_list.clear();
}

//////////////////////////////////////////
void RS3DAmbisonics::decode_to_binaural(const RS3DVector(const AkReal32 *) & input_buffer_list,
	RS3DFloatVector & output_buffer_left, RS3DFloatVector & output_buffer_right) {

	decode(input_buffer_list);	//Transform ambisonic data into input streams

	center_sources_to_listener();
	//update spatializers
	for (int i = 0; i < num_speakers; ++i)
		RS3D_spatializer_list[i]->update(FAST_SPAT, false);

	//process through each speaker with data from decoded_matrix
	for (int i = 0; i < num_speakers; ++i) {
		RS3D_spatializer_list[i]->process(&decoded_matrix->dat(i, 0),
			output_buffer_left, output_buffer_right);
	}
}

//////////////////////////////////////////
float RS3DAmbisonics::compute_FuMa_coeffs(float azim, float elev, char channel_label) {
	switch (channel_label) {
		case 'W':
			return sqrt(1.0 / 2.0);
		case 'X':
			return cos(azim) * cos(elev);
		case 'Y':
			return sin(azim) * cos(elev);
		case 'Z':
			return sin(elev);
		case 'R':
			return (1.0 / 2.0) * (3.0 * sin(elev) * sin(elev) - 1.0);
		case 'S':
			return cos(azim) * sin(2.0 * elev);
		case 'T':
			return sin(azim) * sin(2.0 * elev);
		case 'U':
			return cos(2.0 * azim) * cos(elev) * cos(elev);
		case 'V':
			return sin(2.0 * azim) * cos(elev) * cos(elev);
		case 'K':
			return (1.0 / 2.0) * sin(elev) * (5.0 * sin(elev) * sin(elev) - 3.0);
		case 'L':
			return sqrt(135.0 / 256.0) * cos(azim) * cos(elev) * (5.0 * sin(elev) * sin(elev) - 1.0);
		case 'M':
			return sqrt(135.0 / 256.0) * sin(azim) * cos(elev) * (5.0 * sin(elev) * sin(elev) - 1.0);
		case 'N':
			return sqrt(27.0 / 4.0) * cos(2.0 * azim) * sin(elev) * cos(elev) * cos(elev);
		case 'O':
			return sqrt(27.0 / 4.0) * sin(2.0 * azim) * sin(elev) * cos(elev) * cos(elev);
		case 'P':
			return cos(3.0 * azim) * cos(elev) * cos(elev) * cos(elev);
		case 'Q':
			return sin(3.0 * azim) * cos(elev) * cos(elev) * cos(elev);
		default:
			return 0;
	};
}


//////////////////////////////////////////
float RS3DAmbisonics::compute_FuMa_coeffs(AkVector spk_dir, char channel_label) {

	spk_dir = AkVectorExt::convert2Ambisonics(spk_dir);
	spk_dir = AkVectorExt::normalize(spk_dir);	//Unit norm directions only

	switch (channel_label) {
	case 'W':
		return sqrt(1.0 / 2.0);
	case 'X':
		return spk_dir.X;
	case 'Y':
		return spk_dir.Y;
	case 'Z':
		return spk_dir.Z;
	case 'R':
		return (1.0 / 2.0) * (3.0 * spk_dir.Z * spk_dir.Z - 1.0);
	case 'S':
		return 2.0 * spk_dir.Z * spk_dir.X;
	case 'T':
		return 2.0 * spk_dir.Y * spk_dir.Z;
	case 'U':
		return spk_dir.X * spk_dir.X - spk_dir.Y * spk_dir.Y;
	case 'V':
		return 2.0 * spk_dir.X * spk_dir.Y;
	case 'K':
		return (1.0 / 2.0) * spk_dir.Z * (5.0 * spk_dir.Z * spk_dir.Z - 3.0);
	case 'L':
		return sqrt(135.0 / 256.0) * spk_dir.X * (5.0 * spk_dir.Z * spk_dir.Z - 1.0);
	case 'M':
		return sqrt(135.0 / 256.0) * spk_dir.Y * (5.0 * spk_dir.Z * spk_dir.Z - 1.0);
	case 'N':
		return sqrt(27.0 / 4.0) * spk_dir.Z * (spk_dir.X * spk_dir.X - spk_dir.Y * spk_dir.Y);
	case 'O':
		return sqrt(27.0) * spk_dir.X * spk_dir.Y * spk_dir.Z;
	case 'P':
		return spk_dir.X * (spk_dir.X * spk_dir.X - 3.0 * spk_dir.Y * spk_dir.Y);
	case 'Q':
		return spk_dir.Y * (3.0 * spk_dir.X * spk_dir.X - spk_dir.Y * spk_dir.Y);
	default:
		return 0;
	};
}

//////////////////////////////////////////
bool mat_cholesky_decomp(const RS3DMatrix & A, RS3DMatrix & L) {

	if (A.M != A.N)	//A must be square, hopefully full rank
		return false;

	if (L.M != L.N)	//L must be square
		return false;

	if (A.M != L.M)	//A and L must be identically sized
		return false;

	int M = A.M;
	memset(L.data, 0, sizeof(float) * M * M);

	for (int i = 0; i < M; ++i) {
		for (int j = i; j < M; ++j) {
			float inner_product = 0;
			for (int k = 0; k < i - 1; ++k)
				inner_product += L.dat(i, k) + L.dat(j, k);
			if (j == i)
				L.dat(i, i) = sqrt(A.dat(i, i) - inner_product);
			else 
				L.dat(j, i) = (A.dat(i, j) - inner_product) / L.dat(i, i);
		}
	}
	
	return true;
}

//////////////////////////////////////////
bool mat_mat_product(const RS3DMatrix & A, const RS3DMatrix & B, RS3DMatrix & C) {

	if (A.N != B.M)	//Cols of A disagree with rows of M
		return false;

	if (C.M != A.M || C.N != B.N)	//Size of C disagrees with sizes of A and B
		return false;

	for (int i = 0; i < A.M; ++i) {
		for (int j = 0; j < B.N; ++j) {
			float inner_product = 0;
			for (int k = 0; k < A.N; ++k)
				inner_product += A.dat(i, k) * B.dat(k, j);
			C.dat(i, j) = inner_product;
		}
	}

	return true;
}

//////////////////////////////////////////
bool mat_gauss_elimination(const RS3DMatrix & L, const RS3DMatrix & A, RS3DMatrix & X, bool L_transposed) {
	
	if (L.M != L.N) //L must be square
		return false;

	if (A.M != L.M)		//Rows of A must agree with rows of L
		return false;

	if (X.M != L.N || X.N != A.N)	//X must be correctly sized
		return false;

	if (!L_transposed) {
		for (int i = 0; i < A.N; ++i) {
			for (int j = 0; j < L.M; ++j) {
				float inner_product = 0;
				for (int k = 0; k < j - 1; ++k)
					inner_product += L.dat(j,k) * X.dat(k, i);
				X.dat(j, i) = (A.dat(j,i) - inner_product) / L.dat(j,j);
			}
		}
	}
	else {
		for (int i = 0; i < A.N; ++i) {
			for (int j = L.M - 1; j >= 0; --j) {
				float inner_product = 0;
				for (int k = j; k < L.M; ++k)
					inner_product += L.dat(j,k) * X.dat(k,i);
				X.dat(j,i) = (A.dat(j,i) - inner_product) / L.dat(j,j);
			}
		}
	}

	return true;
}

//////////////////////////////////////////
bool mat_transpose(const RS3DMatrix & A, RS3DMatrix & AT) {
	if (A.M != AT.N || A.N != AT.M)
		return false;

	for (int i = 0; i < A.M; ++i) {
		for (int j = 0; j < A.N; ++j)
			AT.dat(j, i) = A.dat(i,j);
	}

	return true;
}

/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerStubFXParams.h
//
//////////////////////////////////////////////////////////////////////

#ifndef _MIXERSTUB_PARAMS_H_
#define _MIXERSTUB_PARAMS_H_

#include <AK/SoundEngine/Common/IAkMixerPlugin.h>
#include <AK/Plugin/PluginServices/AkFXParameterChangeHandler.h>
#include <math.h>
#include <AK/Tools/Common/AkAssert.h>

#include <iostream>
#include <string>

#include "RS3DCommon.h"

using namespace std;

// Parameters IDs for the Wwise or RTPC.
enum MixerStubParamID
{
	MIXER_STUB_BBYPASS = 0,
	MIXER_STUB_CROSSCANCEL,
	MIXER_STUB_HEADSIZE,
	MIXER_STUB_TORSOSIZE,
	MIXER_STUB_NECKSIZE,
	MIXER_STUB_HRTFTYPE,
	MIXER_STUB_SCALEFAC,
	MIXER_STUB_MAX_PARAMS	// Keep last
};

//-----------------------------------------------------------------------------
// Structures.
//-----------------------------------------------------------------------------

// Structure of Mixer Stub parameters (on bus)
struct MixerStubFXParamsBus
{
	bool bypassBus;
	bool crossCancel;

	float headSize, torsoSize, neckSize;
	AkInt32 HRTF_type_state;
	float scaleFac;
};

//-----------------------------------------------------------------------------
// Name: class MixerStubFXParams
// Desc: Shared parameters implementation.
//-----------------------------------------------------------------------------
class MixerStubFXParams : public AK::IAkPluginParam
{
public:

	friend class MixerStubFX;
 
    // Constructor/destructor.
    MixerStubFXParams();
    ~MixerStubFXParams();
	MixerStubFXParams( const MixerStubFXParams & in_rCopy );

    // Create duplicate.
    IAkPluginParam * Clone( AK::IAkPluginMemAlloc * in_pAllocator );

    // Init/Term.
    AKRESULT Init(	AK::IAkPluginMemAlloc *	in_pAllocator,						    
					const void *			in_pParamsBlock, 
					AkUInt32				in_ulBlockSize 
                         );
    AKRESULT Term( AK::IAkPluginMemAlloc * in_pAllocator );

    // Blob set.
    AKRESULT SetParamsBlock(	const void * in_pParamsBlock, 
								AkUInt32 in_ulBlockSize
                                );

    // Update one parameter.
    AKRESULT SetParam(	AkPluginParamID in_ParamID,
						const void * in_pValue, 
						AkUInt32 in_ulParamSize
                        );

	// Retrieve all parameters at once
	inline void GetParams( MixerStubFXParamsBus & out_params ) { out_params = m_params; }

	AK::AkFXParameterChangeHandler<MIXER_STUB_MAX_PARAMS> m_paramChangeHandler;

private:
    MixerStubFXParamsBus m_params;	// Parameter structure.
};

#endif // _MIXERSTUB_PARAMS_H_

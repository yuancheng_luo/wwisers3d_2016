#pragma once

#include <math.h>

#include <stdio.h>

int planeBoxOverlap(float normal[3], float vert[3], float maxbox[3]);

int triBoxOverlap(float boxcenter[3], float boxhalfsize[3], float triverts[3][3]);
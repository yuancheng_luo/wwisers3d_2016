#include "RS3DBACCH.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DBACCHBusKey::RS3DBACCHBusKey(AkUniqueID _Ak_unique_ID, int _channel_ID) {
	Ak_unique_ID = _Ak_unique_ID;
	channel_ID = _channel_ID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DBACCHBusKey ::operator<(const RS3DBACCHBusKey & RS3D_BACCH_bus_key) const {
	if (Ak_unique_ID < RS3D_BACCH_bus_key.Ak_unique_ID)
		return true;
	else if (Ak_unique_ID == RS3D_BACCH_bus_key.Ak_unique_ID) {
		return channel_ID < RS3D_BACCH_bus_key.channel_ID;
	}
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DBACCHBusKey ::operator==(const RS3DBACCHBusKey & RS3D_BACCH_bus_key) const {
	return (Ak_unique_ID == RS3D_BACCH_bus_key.Ak_unique_ID && channel_ID == RS3D_BACCH_bus_key.channel_ID);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DBACCH::RS3DBACCH() {
	ready_to_process = false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DBACCH::RS3DBACCH(int _num_samples_in_buffer,					//number of samples in the input buffer to be convolved with BACCH filter
	int _sampling_rate,
	float _gain_multiplier,
	void * data_chunk) {

	//Artificial (for testing purposes)
//	_sampling_rate = _sampling_rate * 16;
//	_sampling_rate = _sampling_rate / 16;

	unsigned char * data_chunk_uchar = (unsigned char *) data_chunk;

	num_samples_in_buffer = _num_samples_in_buffer;
	BACCH_filter.num_samples_in_filter = *(int*)data_chunk_uchar;		data_chunk_uchar += sizeof(int);
	BACCH_filter.sampling_rate = *(int*)data_chunk_uchar;				data_chunk_uchar += sizeof(int);

	//Resample to _sampling_rate
	float resample_factor = (float)_sampling_rate / (float)BACCH_filter.sampling_rate;
	BACCH_filter.num_samples_in_resampled_filter = ceil(BACCH_filter.num_samples_in_filter * resample_factor);

	num_samples_in_output = BACCH_filter.num_samples_in_resampled_filter + num_samples_in_buffer;
	//TODO: Find next largest size with low-order prime factorization

	//Setup FFT plans
	real_scratch.resize(num_samples_in_output, 0);
	complex_scratch.resize(num_samples_in_output);
	complex_scratch_left.resize(num_samples_in_output);
	complex_scratch_right.resize(num_samples_in_output);

	plan_forward = fftwf_plan_dft_r2c_1d(num_samples_in_output, &real_scratch[0], (fftwf_complex*)&complex_scratch[0], FFTW_ESTIMATE);
	plan_backward = fftwf_plan_dft_c2r_1d(num_samples_in_output, (fftwf_complex*)&complex_scratch[0], &real_scratch[0], FFTW_ESTIMATE);

	//Read time-domain filters from file, take FFT, and store in array
	int common_filter_size = sizeof(float) * BACCH_filter.num_samples_in_filter;

	BACCH_filter.left_2_left_filter.resize(num_samples_in_output);
	BACCH_filter.right_2_left_filter.resize(num_samples_in_output);
	BACCH_filter.left_2_right_filter.resize(num_samples_in_output);
	BACCH_filter.right_2_right_filter.resize(num_samples_in_output);

	void *dst_ptrs[4] = { &BACCH_filter.left_2_left_filter[0],
		&BACCH_filter.right_2_left_filter[0],
		&BACCH_filter.left_2_right_filter[0],
		&BACCH_filter.right_2_right_filter[0] };
	
	//Create scratch for loading input filter
	RS3DFloatVector real_scratch_filter;
	real_scratch_filter.resize(BACCH_filter.num_samples_in_filter, 0);

	for (int i = 0; i < 4; ++i) {

		memset(&real_scratch[0], 0, sizeof(float) * num_samples_in_output);	//Clear scratch
		memcpy(&real_scratch_filter[0], data_chunk_uchar, common_filter_size);		data_chunk_uchar += common_filter_size;	//Grab filter data
		for (int j = 0; j < BACCH_filter.num_samples_in_filter; ++j) {		//Apply gain
			real_scratch_filter[j] *= _gain_multiplier;
		}

		if (_sampling_rate != BACCH_filter.sampling_rate) {//Use resampled filter
			void *resample_handle = NULL;
			RS3DFloatVector real_scratch_filter_resampled;
			real_scratch_filter_resampled.resize(BACCH_filter.num_samples_in_resampled_filter, 0);

			resample_handle = resample_open(1, resample_factor, resample_factor);
			int inBufferUsed;
			int resample_result = resample_process(resample_handle, resample_factor,
				&real_scratch_filter[0], BACCH_filter.num_samples_in_filter,
				true, &inBufferUsed,
				&real_scratch_filter_resampled[0], BACCH_filter.num_samples_in_resampled_filter);
			resample_close(resample_handle);

			memcpy(&real_scratch[0], &real_scratch_filter_resampled[0], sizeof(float)*BACCH_filter.num_samples_in_resampled_filter);	//Copy into FFTW scratch
		}
		else {//Use original filter
			memcpy(&real_scratch[0], &real_scratch_filter[0], common_filter_size);
		}

		fftwf_execute(plan_forward);	//Get frequency response
		memcpy(dst_ptrs[i], &complex_scratch[0], sizeof(fftwf_complex) * num_samples_in_output);
	}

	LOGD("RS3DBACCH::RS3DBACCH Success");
	ready_to_process = true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DBACCH::~RS3DBACCH() {
	//Destroy FFTW plan
	fftwf_destroy_plan(plan_forward);
	fftwf_destroy_plan(plan_backward);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DBACCH::process(const RS3DFloatVector & left_channel_input_buffer,
						const RS3DFloatVector  & right_channel_input_buffer,
						RS3DRingBuffer<float>  & left_channel_output_ring_buffer,
						RS3DRingBuffer<float> & right_channel_output_ring_buffer) {
	if (ready_to_process) {
		//Store FFT(left input channel) in complex_scratch_left
		memset(&real_scratch[0], 0, sizeof(float) *  num_samples_in_output);
		memcpy(&real_scratch[0], &left_channel_input_buffer[0], sizeof(float) * num_samples_in_buffer);
		fftwf_execute(plan_forward);
		memcpy(&complex_scratch_left[0], &complex_scratch[0], sizeof(std::complex<float>) * num_samples_in_output);

		//Store FFT(right input channel) in complex_scratch_right
		memset(&real_scratch[0], 0, sizeof(float) *  num_samples_in_output);
		memcpy(&real_scratch[0], &right_channel_input_buffer[0], sizeof(float) * num_samples_in_buffer);
		fftwf_execute(plan_forward);
		memcpy(&complex_scratch_right[0], &complex_scratch[0], sizeof(std::complex<float>) * num_samples_in_output);

		//Cross-filter for left output channel
		for (int i = 0; i < num_samples_in_output; ++i) {
			complex_scratch[i] = complex_scratch_left[i] * BACCH_filter.left_2_left_filter[i] + complex_scratch_right[i] * BACCH_filter.right_2_left_filter[i];
		}
		complexScratchToRingBuffer(left_channel_output_ring_buffer);

		//Cross-filter for right output channel
		for (int i = 0; i < num_samples_in_output; ++i) {
			complex_scratch[i] = complex_scratch_right[i] * BACCH_filter.right_2_right_filter[i] + complex_scratch_left[i] * BACCH_filter.left_2_right_filter[i];
		}
		complexScratchToRingBuffer(right_channel_output_ring_buffer);
	}else {
		//Bypass
		left_channel_output_ring_buffer.write(left_channel_input_buffer);
		right_channel_output_ring_buffer.write(right_channel_input_buffer);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DBACCH::complexScratchToRingBuffer(RS3DRingBuffer<float>  & ring_buffer) {
	fftwf_execute(plan_backward);
	for (int i = 0; i < num_samples_in_output; ++i) {
		real_scratch[i] /= num_samples_in_output;
	}
	ring_buffer.writeAdd(real_scratch, num_samples_in_buffer);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int RS3DBACCH::get_num_samples_in_output() const{
	return num_samples_in_output;
}

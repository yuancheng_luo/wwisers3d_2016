#include "RS3DAllocator.h"

AK::IAkPluginMemAlloc *		global_plugin_mem_alloc = NULL;

void *AkPlugin_malloc(size_t size) {
	if (global_plugin_mem_alloc)
		return global_plugin_mem_alloc->Malloc(size);
	else
		return malloc(size);
}

void AkPlugin_free(void *ptr) {
	if (global_plugin_mem_alloc)
		global_plugin_mem_alloc->Free(ptr);
	else
		free(ptr);
}

#include "RS3DCommon.h"

#ifdef NO_THREAD_MUTEX
#else
std::mutex global_mutex_vsengine_prepare_work_area;
#endif

int vs_call_check(int result, const char * msg) {
	
	if (result && result != _VSENGINE_SRCC_OUT_OF_RANGE) {
		//If error, report it
		std::string custom_error_msg = "";
		switch (result) {
		case _VSENGINE_INVALID_LICENSE:
			custom_error_msg = "License key invalid. Check to remove leading/trailing white space/tabs/carriage return";	break;
		case _VSENGINE_NACTIVE_LICENSE:
			custom_error_msg = "Non-active license key.";	break;
		case _VSENGINE_EXPIRED_LICENSE:
			custom_error_msg = "Expired license key.";	break;
		case _VSENGINE_MALLOC_FAILURE:
			custom_error_msg = "Internal malloc failure.";	break;
		case _VSENGINE_HRTF_OPEN_FAILURE:
			custom_error_msg = "HRTF open failure.";	break;
		case _VSENGINE_HRTF_READ_FAILURE:
			custom_error_msg = "HRTF read failure.";	break;
		case _VSENGINE_INVALID_PARAMETER:
			custom_error_msg = "Bad sound-source parameter.";	break;
		case 	_VSENGINE_ILLEGAL_UPDATE:
			custom_error_msg = "Bad sound-source update.";	break;
		}
		LOGE(std::string(std::string(msg) + " FAILED: " + SSTR(result) + "\n" + custom_error_msg).c_str());
	}
	else {
		LOGD(std::string(std::string(msg) + " SUCCESS").c_str());	
	}

	return result;
}

AK::IAkMixerPluginContext *	global_mixer_plugin_context = NULL;


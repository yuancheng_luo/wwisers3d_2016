#include "RS3DTailMgr.h"
#include "RS3DSpatializer.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializerKey::RS3DSpatializerKey() {
	source_key = RS3DSourceKey(0, 0);
	room_key = 0;
	listener_key = 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializerKey::RS3DSpatializerKey( RS3DSourceKey _source_key,
										RS3DRoomKey _room_key,
										RS3DListenerKey _listener_key)
{
	source_key = _source_key;
	room_key = _room_key;
	listener_key = _listener_key;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializerKey::RS3DSpatializerKey(	const RS3DSource & RS3D_source,
										const RS3DRoom & RS3D_room,
										const RS3DListener & RS3D_listener) 
{
	source_key = RS3D_source.get_RS3D_source_key();
	room_key = RS3D_room.get_room_key();
	listener_key = RS3D_listener.get_listener_key();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DSpatializerKey::operator<(const RS3DSpatializerKey& RS3D_spatializer_key) const {
	if (source_key < RS3D_spatializer_key.source_key)
		return true;
	else  if(source_key == RS3D_spatializer_key.source_key){
		if (room_key < RS3D_spatializer_key.room_key)
			return true;
		else if (room_key == RS3D_spatializer_key.room_key)
			return listener_key < RS3D_spatializer_key.listener_key;
		else
			return false;
	}
	else 
		return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializer::RS3DSpatializer(const RS3DSource & _RS3D_source,
	const RS3DRoom & _RS3D_room,
	const RS3DListener & _RS3D_listener,
	RS3DTailMgr *  _RS3D_tail_mgr,
	int _num_samples_in_buffer,
	int _sample_rate,
	eRenderMode _render_mode,
	bool _smooth_doppler,
	double _unit_scale_factor) :
	RS3D_spatializer_key(_RS3D_source, _RS3D_room, _RS3D_listener),
	RS3D_source(_RS3D_source),
	RS3D_room(_RS3D_room),
	RS3D_listener(_RS3D_listener),
	RS3D_tail_mgr(_RS3D_tail_mgr),
	num_samples_in_buffer(_num_samples_in_buffer),
	sample_rate(_sample_rate),
	render_mode(_render_mode),
	smooth_doppler(_smooth_doppler),
	unit_scale_factor(_unit_scale_factor),
	source_info_A(),
	source_work_area_A(),
	external_work_area_ptr_A(NULL),
	source_initialized_A(false),
	source_info_B(),
	source_work_area_B(),
	external_work_area_ptr_B(NULL),
	source_initialized_B(false),
	first_create_vsengine_source(true)
{

	//Allocate buffers
	left_channel_output_buffer.resize(num_samples_in_buffer, 0.0f);
	right_channel_output_buffer.resize(num_samples_in_buffer, 0.0f);

	active_source = SOURCE_A;

#ifdef NO_THREAD_MUTEX
#else
	thread_active = false;
	create_thread = NULL;
	active_threaded_source_swapped = false;
#endif

	//Update vsengine data struct from RS3D objects
	update(_render_mode, _smooth_doppler);

	//Occluder tick ID
	occluder_tick_ID = 0;

	source_AB_fade_frames = 0;


	LOGD("RS3DSpatializer::RS3DSpatializer: SUCCESS");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializer::~RS3DSpatializer() {
#ifdef NO_THREAD_MUTEX
#else
	if (create_thread){
		if(create_thread->joinable()) 
			create_thread->join();
		RS3D_DELETE(create_thread);
	}
#endif


	if (source_initialized_A) {
		clear_occluders(source_info_A);
		destroy_vsengine_source(source_info_A, source_work_area_A, external_work_area_ptr_A);
	}
	if (source_initialized_B) {
		clear_occluders(source_info_B);
		destroy_vsengine_source(source_info_B, source_work_area_B, external_work_area_ptr_B);
	}
	LOGD("RS3DSpatializer::~RS3DSpatializer SUCCESS");
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::prepare_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref) {

	//Set constants
	source_info_ref._data_chunk_size = num_samples_in_buffer;
	source_info_ref._interpolate_hrtf = 0;
	source_info_ref._randomize_reverb = 1;
	source_info_ref._xrelativity_mode = 2;		// 2 = sound source is stationary and listener moves (but our sound source can move. 0 = sound source moves and listener is stationary

	source_info_ref._ffdirection_enbl = 0;
	source_info_ref._ffdirection_angl = 0;
	source_info_ref._intpst_blend_len = 128;
	source_info_ref._distance_lqlimit = 0.25;

	//Early reflection and tail gains
	for (int p = 0; p < _VSENGINE_MAXRFORDER; p++)
		source_info_ref._rfomlf_coef[p] = 1.0;
	source_info_ref._tail_mult_factor = 1.0;

	source_info_ref._rshape_polyhedral = NULL;
	source_info_ref._ugoccluder_xchain = NULL;


	//Override source position, listener position/orientations with these defaults

	//Initial source location
//	source_info_ref._source_ixyz[0] = 0.75  * unit_scale_factor;
//	source_info_ref._source_ixyz[1] = -1.00 * unit_scale_factor;
//	source_info_ref._source_ixyz[2] = 0.00  * unit_scale_factor;

	source_info_ref._source_ixyz[0] = 0.3  * unit_scale_factor;
	source_info_ref._source_ixyz[1] = -0.45 * unit_scale_factor;
	source_info_ref._source_ixyz[2] = 0.01  * unit_scale_factor;

	//Initial listener position and orientation
	source_info_ref._listnr_ixyz[0] = 0.011782948  * unit_scale_factor;
	source_info_ref._listnr_ixyz[1] = -0.028654623 * unit_scale_factor;
	source_info_ref._listnr_ixyz[2] = 0.004721574  * unit_scale_factor;


	source_info_ref._listnr_iypr[0] = 0.0;
	source_info_ref._listnr_iypr[1] = 0.0;
	source_info_ref._listnr_iypr[2] = 0.0;

	source_info_ref._listnr_dxyz[0] = 0.0;
	source_info_ref._listnr_dxyz[1] = 0.0;
	source_info_ref._listnr_dxyz[2] = 0.0;

	source_info_ref._listnr_dypr[0] = 0.0;
	source_info_ref._listnr_dypr[1] = 0.0;
	source_info_ref._listnr_dypr[2] = 0.0;

	source_info_ref._offset_ixyz[0] = 0.0;
	source_info_ref._offset_ixyz[1] = 0.0;
	source_info_ref._offset_ixyz[2] = 0.0;

	//For Debugging purposes
	LOGD(std::string("_data_chunk_size " + SSTR(source_info_ref._data_chunk_size)).c_str());
	LOGD(std::string("_rir_realtime_order " + SSTR(source_info_ref._rir_realtime_order)).c_str());
	LOGD(std::string("_rir_fixdtail_order " + SSTR(source_info_ref._rir_fixdtail_order)).c_str());
	LOGD(std::string("_rir_maxallwd_order " + SSTR(source_info_ref._rir_maxallwd_order)).c_str());
	LOGD(std::string("_rir_length "			+ SSTR(source_info_ref._rir_length)).c_str());
	LOGD(std::string("_highquality_mode " + SSTR(source_info_ref._highquality_mode)).c_str());
	LOGD(std::string("_interpolate_hrtf " + SSTR(source_info_ref._interpolate_hrtf)).c_str());
	LOGD(std::string("_randomize_reverb " + SSTR(source_info_ref._randomize_reverb)).c_str());
	LOGD(std::string("_xrelativity_mode " + SSTR(source_info_ref._xrelativity_mode)).c_str());
	LOGD(std::string("_ffdirection_enbl " + SSTR(source_info_ref._ffdirection_enbl)).c_str());
	LOGD(std::string("_ffdirection_angl " + SSTR(source_info_ref._ffdirection_angl)).c_str());
	LOGD(std::string("_distance_lqlimit " + SSTR(source_info_ref._distance_lqlimit)).c_str());
	LOGD(std::string("_source_range_min " + SSTR(source_info_ref._source_range_min)).c_str());
	LOGD(std::string("_source_range_max " + SSTR(source_info_ref._source_range_max)).c_str());
	LOGD(std::string("_source_range_asp " + SSTR(source_info_ref._source_range_asp)).c_str());
	LOGD(std::string("_source_decay_cut " + SSTR(source_info_ref._source_decay_cut)).c_str());
	LOGD(std::string("_intpst_blend_len " + SSTR(source_info_ref._intpst_blend_len)).c_str());
	LOGD(std::string("_smooth_blend_mod " + SSTR(source_info_ref._smooth_blend_mod)).c_str());

	LOGD(std::string("_roomxe_size " + SSTR(source_info_ref._roomxe_size[0]) + " " + SSTR(source_info_ref._roomxe_size[1]) + " "+SSTR(source_info_ref._roomxe_size[2])).c_str());
	LOGD(std::string("_source_ixyz " + SSTR(source_info_ref._source_ixyz[0]) + " " + SSTR(source_info_ref._source_ixyz[1]) + " "+SSTR(source_info_ref._source_ixyz[2])).c_str());
	LOGD(std::string("_listnr_ixyz " + SSTR(source_info_ref._listnr_ixyz[0]) + " " + SSTR(source_info_ref._listnr_ixyz[1]) + " "+SSTR(source_info_ref._listnr_ixyz[2])).c_str());
	LOGD(std::string("_listnr_iypr " + SSTR(source_info_ref._listnr_iypr[0]) + " " + SSTR(source_info_ref._listnr_iypr[1]) + " "+SSTR(source_info_ref._listnr_iypr[2])).c_str());
	LOGD(std::string("_listnr_iypr " + SSTR(source_info_ref._listnr_iypr[0]) + " " + SSTR(source_info_ref._listnr_iypr[1]) + " "+SSTR(source_info_ref._listnr_iypr[2])).c_str());
	LOGD(std::string("_listnr_dxyz " + SSTR(source_info_ref._listnr_dxyz[0]) + " " + SSTR(source_info_ref._listnr_dxyz[1]) + " "+SSTR(source_info_ref._listnr_dxyz[2])).c_str());
	LOGD(std::string("_listnr_dypr " + SSTR(source_info_ref._listnr_dypr[0]) + " " + SSTR(source_info_ref._listnr_dypr[1]) + " "+SSTR(source_info_ref._listnr_dypr[2])).c_str());
	LOGD(std::string("_offset_ixyz " + SSTR(source_info_ref._offset_ixyz[0]) + " " + SSTR(source_info_ref._offset_ixyz[1]) + " "+SSTR(source_info_ref._offset_ixyz[2])).c_str());
	LOGD(std::string("_rflctn_coef[0] " + SSTR(source_info_ref._rflctn_coef[0][0]) + " " + SSTR(source_info_ref._rflctn_coef[0][1]) + " "+SSTR(source_info_ref._rflctn_coef[0][2])+" "+SSTR(source_info_ref._rflctn_coef[0][3])+" "+SSTR(source_info_ref._rflctn_coef[0][4]) + " "+SSTR(source_info_ref._rflctn_coef[0][5])).c_str());

//	_hrtf_data HRTF_data = RS3D_listener.get_HRTF_data();
	_hrtf_data *HRTF_data = RS3D_listener.get_HRTF_data_ptr();

	int bytes_to_allocate;
	if (vs_call_check(_vsengine_query_work_area_mempool_size(&source_info_ref, HRTF_data, &bytes_to_allocate), "RS3DSpatializer::create_vsengine_source _vsengine_query_work_area_mempool_size"))
		return;

	LOGD(std::string("_vsengine_query_work_area_mempool_size data bytes to alloc: " + SSTR(bytes_to_allocate)).c_str());

#ifdef USE_EXTERNAL_MEMORY
	external_work_area_ptr_ref = (unsigned char*)RS3D_ALLOC(bytes_to_allocate);
	if (!external_work_area_ptr_ref) {
		LOGD(std::string("_RS3DSpatializer::create_vsengine_source: external_work_area_ptr MALLOC FAILURE, alloc bytes : " + SSTR(bytes_to_allocate)).c_str());
		LOGM(std::string("RealSpace3D Plugin sound source malloc failure (" + SSTR(bytes_to_allocate) + " bytes): Reverting to system malloc. Please expand Wwise memory pool." ).c_str());
		return;
	}else{
		LOGD("RS3DSpatializer::create_vsengine_source: external_work_area_ptr SUCCESS");
	}
#endif

	LOGD("RS3DSpatializer::prepare_vsengine_source SUCCESS");

	//memset(external_work_area_ptr_ref, 0, bytes_to_allocate);
	//memset(&source_work_area_ref, 0, sizeof(_source_work_area));	//Clear work-area

	//if (vs_call_check(_vsengine_prepare_work_area(&source_info_ref, &source_work_area_ref, HRTF_data, external_work_area_ptr_ref), "RS3DSpatializer::create_vsengine_source _vsengine_prepare_work_area"))
	//	return;

	//LOGD(std::string("RS3DSpatializer::create_vsengine_source _vsengine_verify_work_area " + SSTR(_vsengine_verify_work_area(&source_work_area_ref))).c_str());

	//if (vs_call_check(_vsengine_setup_source(&source_info_ref, &source_work_area_ref, HRTF_data), "RS3DSpatializer::create_vsengine_source: _vsengine_setup_source"))
	//	return;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::create_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref) {

	_hrtf_data *HRTF_data = RS3D_listener.get_HRTF_data_ptr();

#ifdef NO_THREAD_MUTEX
	if (vs_call_check(_vsengine_prepare_work_area(&source_info_ref, &source_work_area_ref, HRTF_data, external_work_area_ptr_ref), "RS3DSpatializer::create_vsengine_source _vsengine_prepare_work_area")) {
		return;
	}
#else
	global_mutex_vsengine_prepare_work_area.lock();
	if (vs_call_check(_vsengine_prepare_work_area(&source_info_ref, &source_work_area_ref, HRTF_data, external_work_area_ptr_ref), "RS3DSpatializer::create_vsengine_source _vsengine_prepare_work_area")) {
		global_mutex_vsengine_prepare_work_area.unlock();
		return;
	}
	else {
		global_mutex_vsengine_prepare_work_area.unlock();
	}
#endif
	
	LOGD(std::string("RS3DSpatializer::create_vsengine_source _vsengine_verify_work_area " + SSTR(_vsengine_verify_work_area(&source_work_area_ref))).c_str());

	//Original setup source call
	//if (vs_call_check(_vsengine_setup_source(&source_info_ref, &source_work_area_ref, HRTF_data, NULL, 0), "RS3DSpatializer::create_vsengine_source: _vsengine_setup_source"))
	//	return;

	//With tail mgr
	unsigned char * tail_blob = NULL;
	int tail_size = 0;
	//Make a query to tail mgr
	bool tail_success = (RS3D_tail_mgr && RS3D_tail_mgr->getTailBlob(RS3D_room.get_room_ID(), tail_blob, tail_size));

	if (tail_success) {
		if (vs_call_check(_vsengine_setup_source(&source_info_ref, &source_work_area_ref, HRTF_data, tail_blob, tail_size), "RS3DSpatializer::create_vsengine_source: _vsengine_setup_source"))
			return;
		RS3D_FREE(tail_blob);
	}
	else {//Tail not in manager, have vsengine compute it
		if (vs_call_check(_vsengine_setup_source(&source_info_ref, &source_work_area_ref, HRTF_data, NULL, 0), "RS3DSpatializer::create_vsengine_source: _vsengine_setup_source"))
			return;
	}

	//Set initial reflection and tail gains to impossible values 
	for (int p = 0; p < _VSENGINE_MAXRFORDER; p++)
		source_info_ref._rfomlf_coef[p] = -1.0;
	source_info_ref._tail_mult_factor = -1.0;



}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::destroy_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref) {
	
	if (vs_call_check(_vsengine_cleanup_work_area(&source_info_ref, &source_work_area_ref), "RS3DSpatializer::destroy_vsengine_source _vsengine_cleanup_work_area"))
		return;

	if (external_work_area_ptr_ref) {
		RS3D_FREE(external_work_area_ptr_ref);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::clear_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref) {
	memset(&source_info_ref, 0, sizeof(_source_information));
	memset(&source_work_area_ref, 0, sizeof(_source_work_area));
	external_work_area_ptr_ref = NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DSpatializerKey RS3DSpatializer::get_key() const {
	return RS3D_spatializer_key;
}
#ifdef NO_THREAD_MUTEX
#else
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::swap_active_threaded_sources() {
	active_source = (active_source == SOURCE_A ? SOURCE_B : SOURCE_A);
	active_threaded_source_swapped = true;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::update(eRenderMode _render_mode, bool _smooth_doppler) {

	//Update source info
	render_mode = _render_mode;
	smooth_doppler = _smooth_doppler;
	
	_source_information &		source_info_ref = (active_source == SOURCE_A ? source_info_A : source_info_B);
	_source_work_area &			source_work_area_ref = (active_source == SOURCE_A ? source_work_area_A : source_work_area_B);
	unsigned char * & external_work_area_ptr_ref = (active_source == SOURCE_A ? external_work_area_ptr_A : external_work_area_ptr_B);
	bool & source_initialized_ref = (active_source == SOURCE_A ? source_initialized_A : source_initialized_B);


	//Make snapshot of source_info before update 
	_source_information			prev_source_info = source_info_ref;

	//Updates vsengine source info from RS3D listener, room, and source data 
	source_info_ref._smooth_blend_mod = (smooth_doppler ? 1 : 0);		//enable smooth doppler

	update_vsengine_source_info_listener(source_info_ref);
	update_vsengine_source_info_room(source_info_ref);
	update_vsengine_source_info_source(source_info_ref);

#ifdef NO_THREAD_MUTEX
	//Old non-threaded way
	if (first_create_vsengine_source || !vsengine_source_comparator(source_info_ref, prev_source_info)) {	//Previous source-info differs from current
		/////////////////////////////////////////////////////////////////////////////
		//Make calls to VisiSonics engine
		/////////////////////////////////////////////////////////////////////////////
		if (!first_create_vsengine_source) {
			clear_occluders();
			destroy_vsengine_source(prev_source_info, source_work_area_ref, external_work_area_ptr_ref );
			source_initialized_ref = false;
		}
		prepare_vsengine_source(source_info_ref, source_work_area_ref, external_work_area_ptr_ref);
		create_vsengine_source(source_info_ref, source_work_area_ref, external_work_area_ptr_ref );
		source_initialized_ref = true;
		occluder_tick_ID = 0;		//Reset occluder
		first_create_vsengine_source = false;
	}
#else
	////New Threaded way!
	if (first_create_vsengine_source) {
		prepare_vsengine_source(source_info_ref, source_work_area_ref, external_work_area_ptr_ref);
		create_vsengine_source(source_info_ref, source_work_area_ref, external_work_area_ptr_ref);
		source_initialized_ref = true;
		occluder_tick_ID = 0;
		first_create_vsengine_source = false;
	}
	else {

		//Threaded refs
		_source_information &		threaded_source_info_ref = (active_source == SOURCE_A ? source_info_B : source_info_A);
		_source_work_area &			threaded_source_work_area_ref = (active_source == SOURCE_A ? source_work_area_B : source_work_area_A);
		unsigned char * & threaded_external_work_area_ptr_ref = (active_source == SOURCE_A ? external_work_area_ptr_B : external_work_area_ptr_A);
		bool & threaded_source_initialized_ref = (active_source == SOURCE_A ? source_initialized_B : source_initialized_A);

		if (!vsengine_source_comparator(source_info_ref, prev_source_info)) {
			//source_info differs from prev_source enough, spawn new source in threaded_source_info_ref

			if (!thread_active && create_vsengine_source_mutex.try_lock()) {		//Acquired lock, check if threaded_source_info differs from source_info
				create_vsengine_source_mutex.unlock();

				if(create_thread && create_thread->joinable())
					create_thread->join();

				//At this point, thread is not in use
				if (vsengine_source_comparator(source_info_ref, threaded_source_info_ref)) {	//current source_info matches threaded_source_info, flip active_source

					source_info_ref = prev_source_info;
					threaded_source_initialized_ref = true;

					//Swap/Flip the active-source
					swap_active_threaded_sources();

					occluder_tick_ID = 0;		//Force occluders to be readded

					//create_vsengine_source_mutex.unlock();
				}else {	//current source_info mismatchs threaded_source_info, spawn source_info source in new thread
					if (threaded_source_initialized_ref) {	//Delete threaded source
						clear_occluders(threaded_source_info_ref);
						destroy_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);
						clear_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);
						threaded_source_initialized_ref = false;
					}

					//Set threaded source to new source_info
					threaded_source_info_ref = source_info_ref;
					threaded_source_info_ref._ugoccluder_xchain = NULL;			//Disable occluders
					source_info_ref = prev_source_info;			//Revert source_info to previous before

					//Launch new thread
					if (create_thread) {
						RS3D_DELETE(create_thread);
						create_thread = NULL;
					}
					thread_active = true;
					prepare_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);
					create_thread = RS3D_NEW(std::thread(&RS3DSpatializer::threaded_create_vsengine_source, this));
					if (!create_thread) {
						LOGM(std::string("RealSpace3D Plugin thread new failed. Reverting to system new. Please expand Wwise memory pool.").c_str());
						create_thread = new std::thread(&RS3DSpatializer::threaded_create_vsengine_source, this);
					}
				}
			}
			else {//Can't acquire lock, thread is still computing vsengine source
				source_info_ref = prev_source_info; //Keep source_info as before
			}
		}
	}
#endif
}


#ifdef NO_THREAD_MUTEX
#else
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::threaded_create_vsengine_source() {
	
	create_vsengine_source_mutex.lock();

	_source_information &		threaded_source_info_ref = (active_source == SOURCE_A ? source_info_B : source_info_A);
	_source_work_area &			threaded_source_work_area_ref = (active_source == SOURCE_A ? source_work_area_B : source_work_area_A);
	unsigned char * & threaded_external_work_area_ptr_ref = (active_source == SOURCE_A ? external_work_area_ptr_B : external_work_area_ptr_A);
	bool & threaded_source_initialized_ref = (active_source == SOURCE_A ? source_initialized_B : source_initialized_A);
	//Also possible to restrict this call to just setup source in future?
	create_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);

	thread_active = false;

	////Unlock when done
	create_vsengine_source_mutex.unlock();
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DSpatializer::vsengine_source_comparator(const _source_information & curr_source_info, const _source_information & prev_source_info) {
	//returns false if specific parameters between prev_source_info and source_info has changed 
	if (curr_source_info._highquality_mode != prev_source_info._highquality_mode)
		return false;

	if (curr_source_info._smooth_blend_mod != prev_source_info._smooth_blend_mod)
		return false;
	
	if (curr_source_info._rir_length != prev_source_info._rir_length)
		return false;

	if (curr_source_info._rir_fixdtail_order != prev_source_info._rir_fixdtail_order)
		return false;

	if (memcmp(curr_source_info._roomxe_size, prev_source_info._roomxe_size, sizeof(prev_source_info._roomxe_size)) != 0)		//room size
		return false;

	if (memcmp(curr_source_info._rflctn_coef, prev_source_info._rflctn_coef, sizeof(prev_source_info._rflctn_coef)) != 0)		//room reflection coefficients
		return false;

	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::update_vsengine_source_info_source(_source_information & source_info_ref) {
	
	AkVector source_position = RS3D_source.get_position();
	AkVector center =	RS3D_room.get_center();

	source_info_ref._source_range_min = RS3D_source.get_min_dist() * unit_scale_factor;
	source_info_ref._source_range_max = RS3D_source.get_max_dist() * unit_scale_factor;

	if (render_mode == REGULAR) {
		source_info_ref._highquality_mode = REGULAR;

		source_info_ref._source_ixyz[0] = (source_position.X - center.X)        * unit_scale_factor;
		source_info_ref._source_ixyz[1] = ((-1 * source_position.Z) + center.Z) * unit_scale_factor;
		source_info_ref._source_ixyz[2] = ((-1 * source_position.Y) + center.Y) * unit_scale_factor;

	}else if(render_mode == FAST_SPAT){
		source_info_ref._highquality_mode = FAST_SPAT;

		double _psi, _tht, _phi, _tx, _ty, _tz, _esx, _esy, _esz;
		double _a11, _a12, _a13, _a21, _a22, _a23, _a31, _a32, _a33;
		double _i11, _i12, _i13, _i21, _i22, _i23, _i31, _i32, _i33;


		_esx = (source_position.X - center.X)        * unit_scale_factor;
		_esy = ((-1 * source_position.Z) + center.Z) * unit_scale_factor;
		_esz = ((-1 * source_position.Y) + center.Y) * unit_scale_factor;

		AkVector listener_position = RS3D_listener.get_position();
		AkVector listener_orientation = RS3D_listener.get_orientation();

		_tx = (listener_position.X - center.X)        * unit_scale_factor;
		_ty = ((-1 * listener_position.Z) + center.Z) * unit_scale_factor;
		_tz = ((-1 * listener_position.Y) + center.Y) * unit_scale_factor;

		_psi = -listener_orientation.X;
		_tht = listener_orientation.Z;
		_phi = listener_orientation.Y;


		_a11 = cos(_psi) * cos(_tht);
		_a21 = sin(_psi) * cos(_tht);
		_a31 = -sin(_tht);

		_a12 = cos(_psi) * sin(_tht) * sin(_phi) - sin(_psi) * cos(_phi);
		_a22 = sin(_psi) * sin(_tht) * sin(_phi) + cos(_psi) * cos(_phi);
		_a32 = cos(_tht) * sin(_phi);

		_a13 = cos(_psi) * sin(_tht) * cos(_phi) + sin(_psi) * sin(_phi);
		_a23 = sin(_psi) * sin(_tht) * cos(_phi) - cos(_psi) * sin(_phi);
		_a33 = cos(_tht) * cos(_phi);

		_i11 = _a22 * _a33 - _a32 * _a23;
		_i12 = _a32 * _a13 - _a12 * _a33;
		_i13 = _a12 * _a23 - _a22 * _a13;

		_i21 = _a31 * _a23 - _a21 * _a33;
		_i22 = _a11 * _a33 - _a31 * _a13;
		_i23 = _a21 * _a13 - _a11 * _a23;

		_i31 = _a21 * _a32 - _a31 * _a22;
		_i32 = _a31 * _a12 - _a11 * _a32;
		_i33 = _a11 * _a22 - _a21 * _a12;

		source_info_ref._source_ixyz[0] = _i11 * (_esx - _tx) + _i12 * (_esy - _ty) + _i13 * (_esz - _tz);
		source_info_ref._source_ixyz[1] = _i21 * (_esx - _tx) + _i22 * (_esy - _ty) + _i23 * (_esz - _tz);
		source_info_ref._source_ixyz[2] = _i31 * (_esx - _tx) + _i32 * (_esy - _ty) + _i33 * (_esz - _tz);

	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RS3DSpatializer::compute_attenuation() const
{
	if (render_mode == REGULAR) {
		return 1.0;
	}
	else if (render_mode == FAST_SPAT) {
		double distance_m = compute_source_listener_distance_m();

		if (distance_m < RS3D_source.get_max_dist() * unit_scale_factor)
			return std::min<double>((1.0 / std::max<double>(distance_m - RS3D_source.get_min_dist() * unit_scale_factor, 0.0)), 1.0);
		else
			return 0.0;
	}
	else
		return 1.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double RS3DSpatializer::compute_attenuation(const _source_information & source_info_ref) const
{
	if (source_info_ref._highquality_mode == REGULAR) {
		return 1.0;
	}
	else if (source_info_ref._highquality_mode == FAST_SPAT) {
		double distance_m = compute_source_listener_distance_m();
		double max_dist_m = RS3D_source.get_max_dist() * unit_scale_factor;
		double cutoff_percent = .75; //Start to roll quickly to 0 after cutoff_percent

		if (distance_m < max_dist_m * cutoff_percent)
			return std::min<double>((1.0 / std::max<double>(distance_m - RS3D_source.get_min_dist() * unit_scale_factor, 0.0)), 1.0);
		else if (distance_m >= max_dist_m * cutoff_percent && distance_m <= max_dist_m) {
			double alpha = (max_dist_m - distance_m) / ((1.0 - cutoff_percent) * max_dist_m);
			return std::min<double>((1.0 / std::max<double>(distance_m - RS3D_source.get_min_dist() * unit_scale_factor, 0.0)), 1.0) * alpha;
		}
		else
			return 0.0;
	}
	else
		return 1.0;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
double	RS3DSpatializer::compute_attenuation_dist_to_box(ATTENUATION_FUNC attenuation_func) const {
	double distance_m = compute_box_listener_distance_m();

	float min_dist_m = RS3D_source.get_min_dist() * unit_scale_factor;

	if (distance_m < RS3D_source.get_max_dist() * unit_scale_factor)
		if (attenuation_func == ATTENUATE_ONE_OVER_R)
			return std::min<double>((1.0 / std::max<double>(distance_m - min_dist_m, 0.0)), 1.0);
		else if (attenuation_func == ATTENUATE_ONE_OVER_SQRT_R)
			return std::min<double>((1.0 / std::max<double>(sqrt(distance_m - min_dist_m), 0.0)), 1.0);
		else if (attenuation_func == ATTENUATE_NONE)
			return 1.0;
		else
			return 1.0;
	else
		return 0.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double	RS3DSpatializer::compute_attenuation_dist_to_box(const _source_information & source_info_ref, ATTENUATION_FUNC attenuation_func) const {
	double distance = compute_box_listener_distance_m(source_info_ref);

	if (distance < source_info_ref._source_range_max)
		if (attenuation_func == ATTENUATE_ONE_OVER_R)
			return std::min<double>((1.0 / std::max<double>(distance - source_info_ref._source_range_min, 0.0)), 1.0);
		else if (attenuation_func == ATTENUATE_ONE_OVER_SQRT_R)
			return std::min<double>((1.0 / std::max<double>(sqrt(distance - source_info_ref._source_range_min), 0.0)), 1.0);
		else if (attenuation_func == ATTENUATE_NONE)
			return 1.0;
		else
			return 1.0;
	else
		return 0.0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double	RS3DSpatializer::compute_source_listener_distance_m() const {

	AkVector source_position = RS3D_source.get_position();
	AkVector listener_position = RS3D_listener.get_position();

	AkVector ray;
	ray.X = (listener_position.X - source_position.X) * unit_scale_factor;
	ray.Y = (listener_position.Y - source_position.Y) * unit_scale_factor;
	ray.Z = (listener_position.Z - source_position.Z) * unit_scale_factor;

	double ray_mag = ray.X * ray.X + ray.Y * ray.Y + ray.Z * ray.Z;
	return sqrt(ray_mag);	//in meters
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double	RS3DSpatializer::compute_box_listener_distance_m() const {
	AkVector room_size = RS3D_room.get_dimensions();
	AkVector listener_position = RS3D_listener.get_position();
	AkVector listener_position_centered = AkVectorExt::subtract(listener_position, RS3D_room.get_center());

	double dx = std::max<double>(std::max<double>(-room_size.X / 2.0 - listener_position_centered.X, 0), listener_position_centered.X - room_size.X / 2.0) * unit_scale_factor;
	double dy = std::max<double>(std::max<double>(-room_size.Y / 2.0 - listener_position_centered.Y, 0), listener_position_centered.Y - room_size.Y / 2.0) * unit_scale_factor;
	double dz = std::max<double>(std::max<double>(-room_size.Z / 2.0 - listener_position_centered.Z, 0), listener_position_centered.Z - room_size.Z / 2.0) * unit_scale_factor;

	return sqrt(dx * dx + dy * dy + dz * dz);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
double	RS3DSpatializer::compute_box_listener_distance_m(const _source_information & source_info_ref) const {
	double dx = std::max<double>(std::max<double>(-source_info_ref._roomxe_size[0] / 2.0 - source_info_ref._listnr_ixyz[0], 0), source_info_ref._listnr_ixyz[0] - source_info_ref._roomxe_size[0] / 2.0);
	double dy = std::max<double>(std::max<double>(-source_info_ref._roomxe_size[1] / 2.0 - source_info_ref._listnr_ixyz[1], 0), source_info_ref._listnr_ixyz[1] - source_info_ref._roomxe_size[1] / 2.0);
	double dz = std::max<double>(std::max<double>(-source_info_ref._roomxe_size[2] / 2.0 - source_info_ref._listnr_ixyz[2], 0), source_info_ref._listnr_ixyz[2] - source_info_ref._roomxe_size[2] / 2.0);

	return sqrt(dx * dx + dy * dy + dz * dz);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::update_vsengine_source_info_room(_source_information & source_info_ref) {

	if (render_mode == REGULAR) {
		//Set room dimensions
		AkVector room_dimensions = RS3D_room.get_dimensions();
		source_info_ref._roomxe_size[0] = room_dimensions.X * unit_scale_factor;
		source_info_ref._roomxe_size[1] = room_dimensions.Z * unit_scale_factor;
		source_info_ref._roomxe_size[2] = room_dimensions.Y * unit_scale_factor;

		//Set reflection orders
		source_info_ref._rir_realtime_order = (smooth_doppler ? 1 : RS3D_room.get_order_early_reflections());
		source_info_ref._rir_maxallwd_order = 32;
		source_info_ref._rir_fixdtail_order = RS3D_room.get_order_late_reflections();
		source_info_ref._rir_fixdtail_order = (source_info_ref._rir_fixdtail_order > source_info_ref._rir_maxallwd_order ? source_info_ref._rir_maxallwd_order : source_info_ref._rir_fixdtail_order);

//		LOGE(SSTR(source_info_ref._rir_realtime_order).c_str());

		//Set room impulse length
		_hrtf_data HRTF_data = RS3D_listener.get_HRTF_data();
		int sampling_rate = HRTF_data._hsfq;
		int num_blocks = (int)(RS3D_room.get_reverb_length_seconds() * sampling_rate) / num_samples_in_buffer + 1;		//Number of blocks
		source_info_ref._rir_length = num_blocks * num_samples_in_buffer;

		//Set reflection coefficients
		RoomWallReflectionCoefficients room_wall_reflection_coefficients = RS3D_room.get_room_reflection_coefficients();
		for (int p = 0; p < _VSENGINE_RFNBANDS; p++) {
			source_info_ref._rflctn_coef[0][p] = room_wall_reflection_coefficients.left;
			source_info_ref._rflctn_coef[1][p] = room_wall_reflection_coefficients.right;
			source_info_ref._rflctn_coef[2][p] = room_wall_reflection_coefficients.front;
			source_info_ref._rflctn_coef[3][p] = room_wall_reflection_coefficients.back;
			source_info_ref._rflctn_coef[4][p] = room_wall_reflection_coefficients.ceiling;
			source_info_ref._rflctn_coef[5][p] = room_wall_reflection_coefficients.floor;
		}

	}else if (render_mode == FAST_SPAT) {

		source_info_ref._rir_realtime_order = 1;
		source_info_ref._rir_fixdtail_order = 1;
		source_info_ref._rir_maxallwd_order = 1;
		source_info_ref._rir_length = 16 * num_samples_in_buffer;

		// create absurdly large room
		source_info_ref._roomxe_size[0] = 1000000.0f;
		source_info_ref._roomxe_size[1] = 1000000.0f;
		source_info_ref._roomxe_size[2] = 1000000.0f;

		//Set reflection coefficients
		for (int p = 0; p < _VSENGINE_RFNBANDS; p++) {
			source_info_ref._rflctn_coef[0][p] = 0;
			source_info_ref._rflctn_coef[1][p] = 0;
			source_info_ref._rflctn_coef[2][p] = 0;
			source_info_ref._rflctn_coef[3][p] = 0;
			source_info_ref._rflctn_coef[4][p] = 0;
			source_info_ref._rflctn_coef[5][p] = 0;
		}

	}



}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::update_vsengine_source_info_listener(_source_information & source_info_ref) {


	//Listener position
	AkVector position = RS3D_listener.get_position();
	AkVector center = RS3D_room.get_center();

	source_info_ref._listnr_ixyz[0] = (position.X - center.X)        * unit_scale_factor;
	source_info_ref._listnr_ixyz[1] = ((-1 * position.Z) + center.Z) * unit_scale_factor;
	source_info_ref._listnr_ixyz[2] = ((-1 * position.Y) + center.Y) * unit_scale_factor;
	
	//Listener orientation in euler angles
	AkVector orientation = RS3D_listener.get_orientation();
	source_info_ref._listnr_dypr[0] = -orientation.X;
	source_info_ref._listnr_dypr[1] = orientation.Z;
	source_info_ref._listnr_dypr[2] = orientation.Y;
}

void RS3DSpatializer::process(	const AkReal32 * input_buffer_mono,
								RS3DFloatVector & output_buffer_left,
								RS3DFloatVector & output_buffer_right){

	//Refactor soon!
	_source_information &		source_info_ref = (active_source == SOURCE_A ? source_info_A : source_info_B);
	_source_work_area &			source_work_area_ref = (active_source == SOURCE_A ? source_work_area_A : source_work_area_B);
	unsigned char * & external_work_area_ptr_ref = (active_source == SOURCE_A ? external_work_area_ptr_A : external_work_area_ptr_B);
	bool & source_initialized_ref = (active_source == SOURCE_A ? source_initialized_A : source_initialized_B);

	_source_information &		threaded_source_info_ref = (active_source == SOURCE_A ? source_info_B : source_info_A);
	_source_work_area &			threaded_source_work_area_ref = (active_source == SOURCE_A ? source_work_area_B : source_work_area_A);
	unsigned char * & threaded_external_work_area_ptr_ref = (active_source == SOURCE_A ? external_work_area_ptr_B : external_work_area_ptr_A);
	bool & threaded_source_initialized_ref = (active_source == SOURCE_A ? source_initialized_B : source_initialized_A);

#ifdef NO_THREAD_MUTEX
	process_source_ref(source_info_ref, source_work_area_ref,
		NO_FADE, 0, 0,
		false,
		input_buffer_mono, output_buffer_left, output_buffer_right);
	source_AB_fade_frames = 0;	//Reset fader
#else
	if (source_initialized_ref && !threaded_source_initialized_ref) {
		//Only one source active
		process_source_ref(source_info_ref, source_work_area_ref,
			NO_FADE, 0, 0,
			false,
			input_buffer_mono, output_buffer_left, output_buffer_right);
		source_AB_fade_frames = 0;	//Reset fader
	}
	else if (source_initialized_ref && threaded_source_initialized_ref) {
		//Both source and threaded source are active, start fading out threaded source (was the source before moving to new room)

		float transition_mix_seconds = 0.5;
		int num_fade_frames = ceil(transition_mix_seconds * sample_rate / num_samples_in_buffer);

		//Compute wait and fade frames using source-listener distance
		int source_AB_wait_frames;
		int source_AB_max_fade_frames;
		if (source_info_ref._highquality_mode == REGULAR) {
			//Compute number of frames via distance
			float seconds_to_arrival = compute_source_listener_distance_m() / 340.0f;
			float samples_to_arrival = sample_rate * seconds_to_arrival;
			source_AB_wait_frames = ceil(samples_to_arrival / num_samples_in_buffer);
			source_AB_max_fade_frames = source_AB_wait_frames + num_fade_frames;
		}
		else if (source_info_ref._highquality_mode == FAST_SPAT) {
			source_AB_wait_frames = 0;//no wait time, start fading immediately
			source_AB_max_fade_frames = num_fade_frames;
		}

		if (active_threaded_source_swapped) {//Fix for when sound-source rapidly moves between rooms
			if (source_AB_fade_frames != 0) {
				source_AB_fade_frames = source_AB_max_fade_frames - source_AB_fade_frames;
				source_AB_fade_frames = (source_AB_fade_frames < 0 ? 0 : source_AB_fade_frames);
			}
			active_threaded_source_swapped = false;
		}


		if (source_AB_fade_frames < source_AB_wait_frames) {
			//Continue procecssing with outdated source
			process_source_ref(threaded_source_info_ref, threaded_source_work_area_ref,
				NO_FADE, 0, 0,
				false,
				input_buffer_mono, output_buffer_left, output_buffer_right);

			//Process with updated source till it fills up delay but don't output updated source
			process_source_ref(source_info_ref, source_work_area_ref,
				NO_FADE, 0, 0,
				true,
				input_buffer_mono, output_buffer_left, output_buffer_right);
		}
		else {
			//Start fading between outdated and updated sources
			process_source_ref(source_info_ref, source_work_area_ref,
				FADE_IN, source_AB_fade_frames - source_AB_wait_frames, source_AB_max_fade_frames - source_AB_wait_frames,
				false,
				input_buffer_mono, output_buffer_left, output_buffer_right);
			process_source_ref(threaded_source_info_ref, threaded_source_work_area_ref,
				FADE_OUT, source_AB_fade_frames - source_AB_wait_frames, source_AB_max_fade_frames - source_AB_wait_frames,
				false,
				input_buffer_mono, output_buffer_left, output_buffer_right);
		}



		//Move source frames forward and revert to 0 if exceed
		++source_AB_fade_frames;
		if (source_AB_fade_frames >= source_AB_max_fade_frames)
			source_AB_fade_frames = 0;

		if (source_AB_fade_frames == 0) {
			//Destroy threaded_sound_source
			clear_occluders(threaded_source_info_ref);
			destroy_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);
			clear_vsengine_source(threaded_source_info_ref, threaded_source_work_area_ref, threaded_external_work_area_ptr_ref);
			threaded_source_initialized_ref = false;
		}
	}
	else {
		//Error, bypass instead
		double volume_factor = compute_attenuation();
		for (int i = 0; i < num_samples_in_buffer; ++i)
			output_buffer_left[i] += input_buffer_mono[i] * volume_factor;

		for (int i = 0; i < num_samples_in_buffer; ++i)
			output_buffer_right[i] += input_buffer_mono[i] * volume_factor;
	}
#endif

	

	LOGD("RS3DSpatializer::process SUCCESS");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DSpatializer::listener_in_room(const _source_information & curr_source_info) {
	return	std::abs(curr_source_info._listnr_ixyz[0]) <= curr_source_info._roomxe_size[0] / 2 &&
			std::abs(curr_source_info._listnr_ixyz[1]) <= curr_source_info._roomxe_size[1] / 2 &&
			std::abs(curr_source_info._listnr_ixyz[2]) <= curr_source_info._roomxe_size[2] / 2;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::process_source_ref(
	_source_information & source_info_ref, _source_work_area & source_work_area_ref,
	RS3D_SOURCE_FADE fade_mode, int fade_frame, int max_fade_frames,
	bool bypass_output,
	const AkReal32 * input_buffer_mono,
	RS3DFloatVector & output_buffer_left,
	RS3DFloatVector & output_buffer_right) {

	//Modify early-reflection gain to decay to 0 over time if listener outside room
	//Modify late-tail gain to decay towards 1/dist(from source) if listener outside room (room becomes a reverberator?)
	float refl_rate_change  = .15;
	float tail_rate_change = .15;

	float attenuation_factor_tail = compute_attenuation_dist_to_box(source_info_ref, ATTENUATE_ONE_OVER_R);		//To box

	if (!listener_in_room(source_info_ref)) {	//Listener outside room
		//Leak early refl gains towards 0
		for (int i = 1; i <= source_info_ref._rir_realtime_order; ++i) {
			if(source_info_ref._rfomlf_coef[i] == -1)
				source_info_ref._rfomlf_coef[i] = 0;
			else
				source_info_ref._rfomlf_coef[i] -= (source_info_ref._rfomlf_coef[i]) * refl_rate_change;
		}
		//Leak tail gain to attenuation factor
		if(source_info_ref._tail_mult_factor == -1)
			source_info_ref._tail_mult_factor = attenuation_factor_tail;
		else
			source_info_ref._tail_mult_factor -= (source_info_ref._tail_mult_factor - attenuation_factor_tail) * tail_rate_change;
	}
	else {//Listener inside room
		//Leak early refl gains towards 1
		for (int i = 1; i <= source_info_ref._rir_realtime_order; ++i) {
			if (source_info_ref._rfomlf_coef[i] == -1)
				source_info_ref._rfomlf_coef[i] = 1;
			else
				source_info_ref._rfomlf_coef[i] += (1 - source_info_ref._rfomlf_coef[i]) * refl_rate_change;
		}
		//Leak tail gain to 1
		if(source_info_ref._tail_mult_factor == -1)
			source_info_ref._tail_mult_factor = 1;
		else
			source_info_ref._tail_mult_factor += (1 - source_info_ref._tail_mult_factor) * tail_rate_change;
	}
//	source_info_ref._rir_realtime_order = 1;	//Set rir order to 1
//	source_info_ref._tailml_coef = 0;		//Lock late tail gain

	if(source_info_ref._highquality_mode == FAST_SPAT)
		source_info_ref._fast_mode_volume = compute_attenuation(source_info_ref);
	
	//Submit data
	LOGD("RS3DSpatializer::process_source_ref: _vsengine_submit_idata");
	if (vs_call_check(_vsengine_submit_idata(&source_info_ref, &source_work_area_ref, input_buffer_mono), "RS3DSpatializer::process: _vsengine_submit_idata"))
		return;


	//Produce supports
	LOGD("RS3DSpatializer::process_source_ref: _vsengine_produce_support");
	if (vs_call_check(_vsengine_produce_support(&source_info_ref, &source_work_area_ref), "RS3DSpatializer::process: _vsengine_produce_support"))
		return;

	//Produce output
	_hrtf_data * HRTF_data = RS3D_listener.get_HRTF_data_ptr();
	_hrtf_hash * HRTF_hash = RS3D_listener.get_HRTF_hash_ptr();
	LOGD("RS3DSpatializer::process_source_ref _vsengine_produce_odata");
	if (vs_call_check(_vsengine_produce_odata(&source_info_ref, &source_work_area_ref, HRTF_data,
		(source_info_ref._highquality_mode == FAST_SPAT ? HRTF_hash : NULL),
		&left_channel_output_buffer[0], &right_channel_output_buffer[0]),
		"RS3DSpatializer::process_source_ref: _vsengine_produce_odata"))
		return;

	if (!bypass_output) {
		//Apply gain and write to output buffers
		//double volume_factor = compute_attenuation(source_info_ref);
//		double volume_factor = 1.0;

		if (fade_mode == NO_FADE) {
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_left[i] += left_channel_output_buffer[i] ;
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_right[i] += right_channel_output_buffer[i] ;
		}
		else if (fade_mode == FADE_IN) {
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_left[i] += left_channel_output_buffer[i]  * (((float)(i + fade_frame * num_samples_in_buffer)) / (max_fade_frames * num_samples_in_buffer));
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_right[i] += right_channel_output_buffer[i]  * (((float)(i + fade_frame * num_samples_in_buffer)) / (max_fade_frames * num_samples_in_buffer));
		}
		else if (fade_mode == FADE_OUT) {
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_left[i] += left_channel_output_buffer[i]  * (1.0f - ((float)(i + fade_frame * num_samples_in_buffer)) / (max_fade_frames * num_samples_in_buffer));
			for (int i = 0; i < num_samples_in_buffer; ++i)
				output_buffer_right[i] += right_channel_output_buffer[i]  * (1.0f - ((float)(i + fade_frame * num_samples_in_buffer)) / (max_fade_frames * num_samples_in_buffer));
		}
	}

	////Bypass
	//for (int i = 0; i < num_samples_in_buffer; ++i)	{
	//	output_buffer_left[i] += input_buffer_mono[i] * volume_factor;
	//}

	//for (int i = 0; i < num_samples_in_buffer; ++i) {
	//	output_buffer_right[i] += input_buffer_mono[i] * volume_factor;
	//}

	LOGD("RS3DSpatializer::process_source_ref SUCCESS");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::clear_occluders() {
	_source_information &		source_info_ref = (active_source == SOURCE_A ? source_info_A : source_info_B);
	clear_occluders(source_info_ref);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::clear_occluders(_source_information & source_info_ref) {
	struct	_ocugly * curr_occluder = source_info_ref._ugoccluder_xchain;
	while (curr_occluder) {
		struct	_ocugly * _ocnext = curr_occluder->_ocnext;
		RS3D_DELETE(curr_occluder);
		curr_occluder = _ocnext;
	}
	source_info_ref._ugoccluder_xchain = NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DSpatializer::add_occluders(const RS3DVector(RS3DGameDataSingleTriangleOccluder) & occluder_list, int _occluder_tick_ID) {
	if (occluder_tick_ID != _occluder_tick_ID) {

		_source_information &		source_info_ref = (active_source == SOURCE_A ? source_info_A : source_info_B);

		occluder_tick_ID = _occluder_tick_ID;

		//Clear spatializer's occluder list
		clear_occluders();

		AkVector center = RS3D_room.get_center();

		struct	_ocugly * head_occluder = NULL;
		struct	_ocugly * prev_occluder = NULL;

		//Scan input occluder_list for any whose vertex (one or more) is contained in room
		int N_occluders = occluder_list.size();
		for (int i = 0; i < N_occluders; ++i) {
			int N_vertices = 3;
			N_vertices = (N_vertices < _VSENGINE_OCCLPMAX ? N_vertices : _VSENGINE_OCCLPMAX);		//clamp n-vertices to max of vsengine

			bool contains_occluder = false;
			////Vertex-box intersect test
			//for (int j = 0; j < N_vertices; ++j) {
			//	if (RS3D_room.contains_point(occluder_list[i].vertex_position[j])) {
			//		contains_occluder = true;
			//		break;
			//	}
			//}

			//Ray-box intersect test
			//for (int j = 0; j < N_vertices; ++j) {
			//	if (RS3D_room.intersects_segment(occluder_list[i].vertex_position[j], occluder_list[i].vertex_position[(j + 1)%N_vertices])) {
			//		contains_occluder = true;
			//		break;
			//	}
			//}

			//triangle-box intersect test
			contains_occluder = RS3D_room.intersects_triangle(occluder_list[i].vertex_position[0], occluder_list[i].vertex_position[1], occluder_list[i].vertex_position[2]);

			if (contains_occluder) {
				//Add to spatializer's occluder list
				head_occluder = RS3D_NEW(struct _ocugly);
				head_occluder->_npnt = N_vertices;
				head_occluder->_tvalue = (1 - occluder_list[i].absorption_percent / 100.0f);

				for (int j = 0; j < N_vertices; ++j) {
					AkVector vertex_position = occluder_list[i].vertex_position[j];
					//Recenter to room-center and flip coordinates
					head_occluder->_x[0][j] = (vertex_position.X - center.X) * unit_scale_factor;
					head_occluder->_x[1][j] = ((-1 * vertex_position.Z) + center.Z) * unit_scale_factor;
					head_occluder->_x[2][j] = ((-1 * vertex_position.Y) + center.Y) * unit_scale_factor;
				}
				head_occluder->_ocnext = prev_occluder;
				prev_occluder = head_occluder;
			}
		}

		//Set chain head
		source_info_ref._ugoccluder_xchain = head_occluder;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_source_information RS3DSpatializer::get_copy_active_source_info() {
	return  (active_source == SOURCE_A ? source_info_A : source_info_B);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_source_work_area RS3DSpatializer::get_copy_active_work_area() {
	return  (active_source == SOURCE_A ? source_work_area_A : source_work_area_B);
}

#pragma once

#include <algorithm>

#include <AK/SoundEngine/Common/AkTypes.h>
#include "AkVectorExt.h"

#include "boxTriangleIntersection.h"

#include "RS3DGameBlob.h"
#include "RS3DCommon.h"

struct RoomWallReflectionCoefficients {
	double left;
	double right;
	double front;
	double back;
	double ceiling;
	double floor;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RS3DRoomKey {
public:
	RS3DRoomKey(AkGameObjectID _Ak_game_object_ID = 0, int _sub_object_ID = 0);

	AkGameObjectID Ak_game_object_ID;
	int			   sub_object_ID;

	bool operator<(const RS3DRoomKey& RS3D_room_key) const;
	bool operator==(const RS3DRoomKey& RS3D_room_key) const;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RS3DRoom {
public:
	RS3DRoom();
	RS3DRoom(RS3DRoomKey _room_key, float _scale_fac);
	RS3DRoom(RS3DRoomKey _room_key,
				const AkVector & _room_center,
				const AkVector & _room_dimensions,
				const RoomWallReflectionCoefficients & _room_wall_reflection_coefficients,
				int _order_early_reflections, int _order_late_reflections, 
				double _reverb_length_seconds,
				float _scale_fac);
	RS3DRoom(RS3DRoomKey _room_key, const RS3DGameDataRoom & _RS3D_game_data_room, float _scale_fac);

	void update_center(const AkVector & _room_center);
	void update_dimensions(const AkVector & _room_dimensions);
	void update_wall_reflection_coefficients(const RoomWallReflectionCoefficients & _room_wall_reflection_coefficients);
	void update_reflection_orders(int _order_early_reflections, int _order_late_reflections);
	void update_reverb_length_seconds(double _reverb_length_seconds);
	
	void update_with_RS3DGameDataRoom(const RS3DGameDataRoom & _RS3D_game_data_room);
	void update_with_universe_room(AkVector _room_center, int _room_ID);

	RS3DRoomKey get_room_key() const;
	int get_room_ID() const;
	AkVector get_center() const;
	AkVector get_dimensions() const;
	RoomWallReflectionCoefficients get_room_reflection_coefficients() const;
	int get_order_early_reflections() const;
	int get_order_late_reflections() const;
	double get_reverb_length_seconds() const;

	bool contains_point(const AkVector & position) const;
	bool intersects_segment(const AkVector & point_A, const AkVector & point_B) const;
	bool intersects_triangle(const AkVector & point_A, const AkVector & point_B, const AkVector & point_C) const;

	float vertex_min_distance(const AkVector & position);	//returns minimum distance between vertex and wall
	float vertex_infimum_distance();						//greatest {minimum distance to any wall within room}

	static double compute_Sabine_RT60(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac);	//1 game unit / scale_fac = 1 meter
	static double compute_Sabine_reverb_seconds(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac);
	static int compute_Sabine_late_order_reflections(const AkVector & room_size, const RoomWallReflectionCoefficients & room_wall_reflection_coefficients, float scale_fac);

private:
	RS3DRoomKey						room_key;

	int								room_ID;

	AkVector						center;
	AkVector						dimensions;

	AkVector						bounds[2];	//Min, max along x,y,z dimensions

	RoomWallReflectionCoefficients	room_wall_reflection_coefficients;

	int order_early_reflections;
	int order_late_reflections;

	double reverb_length_seconds;

	float scale_fac;

	void updateBounds();			//Update bounds from room center and dimensions
};
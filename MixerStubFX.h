/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerStub.h
//
// Mixer Plugin stub implementation.
//
// Copyright 2013 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#ifndef _MIXERSTUB_H_
#define _MIXERSTUB_H_

#include "MixerBase.h"
#include "MixerStubFXParams.h"

#include "RS3DAllocator.h"
#include "RS3DController.h"

#include "RS3DCommon.h"

//-----------------------------------------------------------------------------
// Name: class MixerStubFX
// Sample implementation of a mixer.
//-----------------------------------------------------------------------------

struct MixerStubFXInfo
{
	MixerStubFXParamsBus		params;
	MixerStubFXParamsBus		prevParams;
};

class MixerStubFX : public MIXERSTUB::MixerBase
{
public:
	MixerStubFX();

	// Override Init: set parameters.
	virtual AKRESULT Init(
		AK::IAkPluginMemAlloc *		in_pAllocator,				///< Interface to memory allocator to be used by the effect
		AK::IAkMixerPluginContext *	in_pMixerPluginContext,		///< Interface to mixer plug-in's context
		AK::IAkPluginParam *		in_pParams,					///< Interface to plug-in parameters
		AkAudioFormat &				in_rFormat					///< Audio data format of the input/output signal.
		) override;

	// Override OnEffectsProcessed: update parameters on frame end.
	virtual void OnFrameEnd(
		AkAudioBuffer *				io_pMixBuffer,		///< Output audio buffer data structure. Stored across calls to ConsumeInput().
		AK::IAkMetering *			in_pMetering		///< Interface for retrieving metering data computed on io_pMixBuffer. 
		);

	// Free memory used by effect and effect termination
	virtual AKRESULT Term(AK::IAkPluginMemAlloc * in_pAllocator) override;

protected:

	// MixerBase specialization.
	
	// Returns true if "custom" processing is bypassed for this input.
	virtual bool IsCustomProcessingBypassed(
		AK::IAkMixerInputContext *	in_pInputContext	// Context for this input. Carries non-audio data.
		);

	virtual void Compute3DPanningGains(
		AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
		AkEmitterListenerPair &		in_emitListPair,	// Emitter-listener pair.
		AkUInt32					in_uPairIndex,		// Index of the emitter-listener pair.
		AkChannelConfig				in_channelConfigIn,	// Channel config of the input.
		AkChannelConfig				in_channelConfigOut,// Channel config of the output (mix).
		AK::SpeakerVolumes::MatrixPtr out_mxVolumes		// Returned volume matrix.
		);

	// Shared parameter interface
	MixerStubFXParams *			m_pSharedParams;
	MixerStubFXInfo				m_fxInfo;


	virtual void ConsumeInput(
		AK::IAkMixerInputContext *	in_pInputContext,	///< Context for this input. Carries non-audio data.
		AkRamp						in_baseVolume,		///< Base volume to apply to this input (prev corresponds to the beginning, next corresponds to the end of the buffer). This gain is agnostic of emitter-listener pair-specific contributions (such as distance level attenuation).
		AkRamp						in_emitListVolume,	///< Emitter-listener pair-specific gain. When there are multiple emitter-listener pairs, this volume equals 1, and pair gains are applied directly on the channel volume matrix (accessible via IAkMixerInputContext::GetSpatializedVolumes()). For custom processing of emitter-listener pairs, one should query each pair volume using IAkMixerInputContext::Get3DPosition(), then AkEmitterListenerPair::GetGainForConnectionType().
		AkAudioBuffer *				io_pInputBuffer,	///< Input audio buffer data structure. Plugins should avoid processing data in-place.
		AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored until call to GetOutput().
		) override;

	virtual void OnMixDone(
		AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored across calls to ConsumeInput().
		) override;

	virtual void OnInputDisconnected(
		AK::IAkMixerInputContext * in_pInput
		) override;

	///////////////////////////////////////////////////////////////////////////////////////////////
	//RealSpace3D
	///////////////////////////////////////////////////////////////////////////////////////////////
	RS3DController * RS3D_controller;
	AkReal32 *input_buffer_scratch;
	AkReal32 *left_buffer_scratch;
	AkReal32 *right_buffer_scratch;

	AkUniqueID mixer_bus_ID;

	void channelMask2FRPairsAzim(AkUInt32 uChannelMask, AkUInt32 & FL, AkUInt32 & FR, AkUInt32 & RL, AkUInt32 & RR, float *channel_azim_rads);	//channel_azim_rads size 8

};

#endif // _MIXERSTUB_H_

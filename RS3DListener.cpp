#include "RS3DListener.h"

/////////////////////////////////////////////////////////////////////////////////
RS3DListener::RS3DListener() 
	:
	HRTF_data(),
	HRTF_hash(),
	is_HRTF_finalized(false),
	external_HRTF_data_mem_ptr(NULL),
	external_HRTF_hash_mem_ptr(NULL)
{

	listener_key = 0;

	Anthropometry  _anthropometry = { 0.093, 0.17, 0.053 };
	update_anthropometry(_anthropometry);
	HRTFBlob _HRTF_blob = { NULL, 0 };
	update_HRTF_blob(_HRTF_blob);
	update_license_key("");

	init();

	LOGD("RS3DListener::RS3DListener: WARNING, Dummy constructor");
}

/////////////////////////////////////////////////////////////////////////////////
RS3DListener::RS3DListener(	RS3DListenerKey _listener_key,
							const Anthropometry & _anthropometry,
							const HRTFBlob & _HRTF_blob,
							const std::string &_license_key)
	:
	HRTF_data(),
	HRTF_hash(),
	is_HRTF_finalized(false),
	external_HRTF_data_mem_ptr(NULL),
	external_HRTF_hash_mem_ptr(NULL)
{
	listener_key = _listener_key;

	update_anthropometry(_anthropometry);
	update_HRTF_blob(_HRTF_blob);
	update_license_key(_license_key);

	init();
	LOGD("RS3DListener::RS3DListener: SUCCESS");
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::init() {
	position.X = 0;
	position.Y = 0;
	position.Z = 0;

	orientation.X = 0;
	orientation.Y = 0;
	orientation.Z = 0;

	memset(&HRTF_hash, 0, sizeof(_hrtf_hash));
	memset(&HRTF_data, 0, sizeof(_hrtf_data));
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DListener::~RS3DListener() {
	if (is_HRTF_finalized) {

		if (vs_call_check(_vsengine_cleanup_hrtf(&HRTF_data), "RS3DListener::~RS3DListener _vsengine_cleanup_hrtf"))
			return;
	
		if (vs_call_check(_vsengine_cleanup_hrtf_hash(&HRTF_hash), "RS3DListener::~RS3DListener _vsengine_cleanup_hrtf_hash"))
			return;
	}

	if (external_HRTF_data_mem_ptr)
		RS3D_FREE(external_HRTF_data_mem_ptr);

	if (external_HRTF_hash_mem_ptr)
		RS3D_FREE(external_HRTF_hash_mem_ptr);
		
	LOGD("RS3DListener::~RS3DListener SUCCESS");
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::update_anthropometry(const Anthropometry & _anthropometry) {

	anthropometry = _anthropometry;

	HRTF_data._alpha_min = 0.1;
	HRTF_data._theta_min = 5.0 * _M_PI / 6.0;
	HRTF_data._trso_rflct = 0.3;

	HRTF_data._head_radius = anthropometry.head_radius;	
	HRTF_data._trso_radius = anthropometry.torso_radius;	
	HRTF_data._neck_height = anthropometry.neck_height;	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::update_HRTF_blob(const HRTFBlob & _HRTF_blob) {
	
	////Make copy
	//HRTF_blob.data_size = _HRTF_blob.data_size;
	//if (_HRTF_blob.data){
	//	if(HRTF_blob.data){
	//		RS3D_FREE(HRTF_blob.data);		
	//	}
	//	HRTF_blob.data = RS3D_ALLOC(HRTF_blob.data_size);
	//	if(HRTF_blob.data){	
	//		memcpy(HRTF_blob.data, _HRTF_blob.data, HRTF_blob.data_size);
	//	}else{
	//		LOGE("RS3DListener::update_HRTF_blob ALLOC FAILURE");
	//	}
	//}


	//Shallow copy	
	HRTF_blob = _HRTF_blob;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::update_license_key(const std::string &_license_key) {
	license_key = _license_key;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::finialize_HRTF(int num_sample_per_buffer, int sample_rate, bool do_resample) {

	int num_days_till_expiration;
	int bytes_to_allocate;

	if (vs_call_check(_vsengine_query_hrtf_mempool_size((unsigned char*)HRTF_blob.data, HRTF_blob.data_size, sample_rate, do_resample, &bytes_to_allocate),
		"RS3DListener::finialize_listener: _vsengine_query_hrtf_mempool_size"))
		return;

	LOGD(std::string("HRTF data bytes to alloc: " + SSTR(bytes_to_allocate)).c_str());

#ifdef USE_EXTERNAL_MEMORY
	external_HRTF_data_mem_ptr = (unsigned char*)RS3D_ALLOC(bytes_to_allocate);
	if (!external_HRTF_data_mem_ptr) {
		LOGD("RS3DListener::finalize_HRTF: external_HRTF_data_mem_ptr MALLOC FAILURE");
		LOGM(std::string("RealSpace3D Plugin HRTF dataset malloc failure (" + SSTR(bytes_to_allocate) + " bytes): Reverting to system malloc. Please expand Wwise memory pool.").c_str());
	}
#endif

	if (vs_call_check(_vsengine_prepare_hrtf(	(unsigned char*)HRTF_blob.data,
												HRTF_blob.data_size,
												sample_rate,
												do_resample,
												&HRTF_data,
												//(unsigned char*)std::string("s4tsf").c_str(),	//fake key
												//reinterpret_cast<const unsigned char *>(license_key.c_str()),
												(const unsigned char*)license_key.c_str(),
												external_HRTF_data_mem_ptr,
												&num_days_till_expiration), 
		"RS3DListener::finialize_listener: _vsengine_prepare_hrtf"))
		return;

	LOGD(std::string("RS3DListener::finialize_HRTF _vsengine_verify_hrtf_data " + SSTR(_vsengine_verify_hrtf_data(&HRTF_data))).c_str());


	if (vs_call_check(_vsengine_query_hrtf_hash_mempool_size(&HRTF_data, num_sample_per_buffer, &bytes_to_allocate),
		"RS3DListener::finialize_listener: _vsengine_query_hrtf_hash_mempool_size"))
		return;

	LOGD(std::string("HRTF hash bytes to alloc: " + SSTR(bytes_to_allocate)).c_str());

#ifdef USE_EXTERNAL_MEMORY
	external_HRTF_hash_mem_ptr = (unsigned char*)RS3D_ALLOC(bytes_to_allocate);
	if (!external_HRTF_hash_mem_ptr) {
		LOGD("RS3DListener::finialize_HRTF: external_HRTF_hash_mem_ptr MALLOC FAILURE");
		LOGM(std::string("RealSpace3D Plugin HRTF hash malloc failure (" + SSTR(bytes_to_allocate) + " bytes): Reverting to system malloc. Please expand Wwise memory pool.").c_str());
	}

#endif

	if (vs_call_check(_vsengine_prepare_hrtf_hash(&HRTF_data, num_sample_per_buffer, &HRTF_hash, external_HRTF_hash_mem_ptr),
		"RS3DListener::finialize_listener: _vsengine_prepare_hrtf_hash"))
		return;
	
	LOGD(std::string("RS3DListener::finialize_HRTF _vsengine_verify_hrtf_hash " + SSTR(_vsengine_verify_hrtf_hash(&HRTF_hash))).c_str());

	is_HRTF_finalized = true;
	LOGD("RS3DListener::finialize_HRTF SUCCESS");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::update_position(const AkVector & _position, float learning_rate) {

	learning_rate = (learning_rate > 1 ? 1 : (learning_rate < 0 ? 0 : learning_rate));		//Clip to [0, 1]
	position.X += learning_rate * (_position.X - position.X);
	position.Y += learning_rate * (_position.Y - position.Y);
	position.Z += learning_rate * (_position.Z - position.Z);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DListener::update_orientation(const AkVector & _orientation, float learning_rate) {
	//TODO: Interpolation of either spherical coordinates or Quaternion is not simply leaky update
	//For now, set equal
	orientation = _orientation;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DListenerKey	RS3DListener::get_listener_key() const {
	return listener_key;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DListener::get_position() const {
	return position;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
AkVector RS3DListener::get_orientation() const {
	return orientation;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_hrtf_data RS3DListener::get_HRTF_data() const {
	if (!is_HRTF_finalized) {
		LOGE("RS3DListener::get_HRTF_data: FAIL, HRTF not finalized");
		return _hrtf_data();
	}else
		return HRTF_data;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_hrtf_data* RS3DListener::get_HRTF_data_ptr() const {
	if (!is_HRTF_finalized) {
		LOGE("RS3DListener::get_HRTF_data_ptr: FAIL, HRTF not finalized");
		return NULL;
	}
	else
		return (_hrtf_data*) &HRTF_data;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_hrtf_hash RS3DListener::get_HRTF_hash() const {
	if (!is_HRTF_finalized) {
		LOGE("RS3DListener::get_HRTF_hash: FAIL, HRTF not finalized");
		return _hrtf_hash();
	}
	else
		return HRTF_hash;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
_hrtf_hash* RS3DListener::get_HRTF_hash_ptr() const {
	if (!is_HRTF_finalized) {
		LOGE("RS3DListener::get_HRTF_hash_ptr: FAIL, HRTF not finalized");
		return NULL;
	}
	else
		return (_hrtf_hash*) &HRTF_hash;
}

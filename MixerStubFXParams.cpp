/***********************************************************************
  The content of this file includes source code for the sound engine
  portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
  Two Source Code" as defined in the Source Code Addendum attached
  with this file.  Any use of the Level Two Source Code shall be
  subject to the terms and conditions outlined in the Source Code
  Addendum and the End User License Agreement for Wwise(R).

  Version: <VERSION>  Build: <BUILDNUMBER>
  Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
 ***********************************************************************/

#include "MixerStubFXParams.h"
#include <AK/Tools/Common/AkBankReadHelpers.h>

// Constructor/destructor.
MixerStubFXParams::MixerStubFXParams( )
{
}

MixerStubFXParams::~MixerStubFXParams( )
{
}

// Copy constructor.
MixerStubFXParams::MixerStubFXParams( const MixerStubFXParams & in_rCopy )
{
	m_params = in_rCopy.m_params;
	m_paramChangeHandler.SetAllParamChanges();
}

// Create duplicate.
AK::IAkPluginParam * MixerStubFXParams::Clone( AK::IAkPluginMemAlloc * in_pAllocator )
{
	AKASSERT( in_pAllocator != NULL );
	return AK_PLUGIN_NEW( in_pAllocator, MixerStubFXParams( *this ) );
}

// Init/Term.
AKRESULT MixerStubFXParams::Init(	AK::IAkPluginMemAlloc *	in_pAllocator,									   
										const void *			in_pParamsBlock, 
										AkUInt32				in_ulBlockSize )
{
	if ( in_ulBlockSize == 0)
	{
		// Init default parameters.
		m_params.bypassBus = false;
		m_params.crossCancel = false;

		m_params.headSize = 10.0f;		m_params.torsoSize = 23.0f;		m_params.neckSize = 5.0f;
		m_params.HRTF_type_state = 0;
		m_params.scaleFac = 1;

		return AK_Success;
	}
	return SetParamsBlock( in_pParamsBlock, in_ulBlockSize );
}

AKRESULT MixerStubFXParams::Term( AK::IAkPluginMemAlloc * in_pAllocator )
{
	AKASSERT( in_pAllocator != NULL );
	AK_PLUGIN_DELETE( in_pAllocator, this );
	return AK_Success;
}

// Blob set.
AKRESULT MixerStubFXParams::SetParamsBlock(	const void * in_pParamsBlock, 
											AkUInt32 in_ulBlockSize
											)
{  
	AKRESULT eResult = AK_Success;
	
	AkUInt8 * pParamsBlock = (AkUInt8 *)in_pParamsBlock;

	m_params.bypassBus = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);
	m_params.crossCancel = READBANKDATA(bool, pParamsBlock, in_ulBlockSize);

	m_params.headSize = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.torsoSize = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.neckSize = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);
	m_params.HRTF_type_state = READBANKDATA(AkInt32, pParamsBlock, in_ulBlockSize);
	m_params.scaleFac = READBANKDATA(AkReal32, pParamsBlock, in_ulBlockSize);

	CHECKBANKDATASIZE(in_ulBlockSize, eResult);

	m_paramChangeHandler.SetAllParamChanges();

	if (eResult != AK_Success) {
		LOGE(std::string("MixerStubFXParams::SetParamsBlock failure with " + SSTR(eResult) + ", in_ulBlockSize " + SSTR(in_ulBlockSize) ).c_str());
	}
	else {
		//LOGE(std::string("bypassBus " + SSTR(m_params.bypassBus) + 
		//	"crossCancel " + SSTR(m_params.crossCancel) + 
		//	"headSize " + SSTR(m_params.headSize) + 
		//	"torsoSize " + SSTR(m_params.torsoSize) + 
		//	"neckSize " + SSTR(m_params.neckSize) + 
		//	"HRTF_type_state " + SSTR(m_params.HRTF_type_state) + 
		//	"scaleFac " + SSTR(m_params.scaleFac)).c_str());
	}
	
	return eResult;
}

// Update one parameter.
AKRESULT MixerStubFXParams::SetParam(	AkPluginParamID in_ParamID,
										const void * in_pValue, 
										AkUInt32 in_ulParamSize )
{
	AKASSERT( in_pValue != NULL );
	if ( in_pValue == NULL )
	{
		return AK_InvalidParameter;
	}
	AKRESULT eResult = AK_Success;

	switch (in_ParamID)
	{
	case MIXER_STUB_BBYPASS:
//		m_params.bypassBus = (*reinterpret_cast<const RTPC_BOOL*>(in_pValue) != 0);		//For RTPC
		m_params.bypassBus = (*(RTPC_BOOL*)(in_pValue) ) != 0;					//For RTPC

		//MessageBoxA(NULL, std::string(std::to_string((*(AkReal32*)(in_pValue))) + " " + std::to_string(m_params.bBypass)).c_str(), "DEBUG", MB_OK);
		break;
	case MIXER_STUB_CROSSCANCEL:
	//	m_params.crossCancel = (*reinterpret_cast<const AkReal32*>(in_pValue) != 0);	//For RTPC
		m_params.crossCancel = (*(RTPC_BOOL*)(in_pValue)) != 0;							//For RTPC
		break;
	case MIXER_STUB_HEADSIZE:
//		m_params.headSize = *reinterpret_cast<const float*>(in_pValue);
		m_params.headSize = *(float*)(in_pValue);
		break;
	case MIXER_STUB_TORSOSIZE:
//		m_params.torsoSize = *reinterpret_cast<const float*>(in_pValue);
		m_params.torsoSize = *(float*)(in_pValue);
		break;
	case MIXER_STUB_NECKSIZE:
//		m_params.neckSize = *reinterpret_cast<const float*>(in_pValue);
		m_params.neckSize = *(float*)(in_pValue);
		break;
	case MIXER_STUB_HRTFTYPE:
//		m_params.HRTF_type_state = *reinterpret_cast<const unsigned int*>(in_pValue);
		m_params.HRTF_type_state = *(AkInt32*)(in_pValue);
		break;
	case MIXER_STUB_SCALEFAC:
//		m_params.scaleFac = *reinterpret_cast<const float*>(in_pValue);
		m_params.scaleFac = *(float*)(in_pValue);
		break;
	default:
		AKASSERT(!"Invalid parameter.");
		eResult = AK_InvalidParameter;
	}

	m_paramChangeHandler.SetParamChange( in_ParamID );

	if (eResult != AK_Success) {
		LOGE(std::string("MixerStubFXParams::SetParam failure with " + SSTR(eResult) + ", in_ParamID " + SSTR(in_ParamID) + ", in_ulParamSize " + SSTR(in_ulParamSize)).c_str());
	}


	return eResult;

}

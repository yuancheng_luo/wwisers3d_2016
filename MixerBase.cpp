/***********************************************************************
The content of this file includes source code for the sound engine
portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
Two Source Code" as defined in the Source Code Addendum attached
with this file.  Any use of the Level Two Source Code shall be
subject to the terms and conditions outlined in the Source Code
Addendum and the End User License Agreement for Wwise(R).

Version: <VERSION>  Build: <BUILDNUMBER>
Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerStub.cpp
//
// Mixer Plugin stub implementation.
// 
//
// Copyright 2006 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#include "MixerBase.h"
#include <AK/Tools/Common/AkAssert.h>
#include <AK/AkWwiseSDKVersion.h>

using namespace MIXERSTUB;

// Constructor.
MixerBase::MixerBase()
{
	m_customMixBuffer.Clear();
}

// Destructor.
MixerBase::~MixerBase()
{

}

// Initializes and allocate memory for the effect
AKRESULT MixerBase::Init(
	AK::IAkPluginMemAlloc *		in_pAllocator,				///< Interface to memory allocator to be used by the effect
	AK::IAkMixerPluginContext *	in_pMixerPluginContext,		///< Interface to mixer plug-in's context
	AK::IAkPluginParam *		in_pParams,					///< Interface to plug-in parameters
	AkAudioFormat &				in_rFormat					///< Audio data format of the input/output signal.
	)
{
	// Save format internally
	m_uSampleRate = in_rFormat.uSampleRate;
	m_uMaxProcessingBlockSize = in_pMixerPluginContext->GlobalContext()->GetMaxBufferLength();

	m_pAllocator = in_pAllocator;
	m_pEffectContext = in_pMixerPluginContext;

	// Inputs map.
	m_mapInputs.Init(in_pAllocator);

	// Custom mix buffer.
	m_uCustomMixBufferSize = m_uMaxProcessingBlockSize * in_rFormat.channelConfig.uNumChannels * sizeof(AkReal32);
	AkReal32 * pSamplesBuffer = (AkReal32*)AK_PLUGIN_ALLOC(in_pAllocator, m_uCustomMixBufferSize);
	if (!pSamplesBuffer)
		return AK_Fail;

	m_customMixBuffer.AttachContiguousDeinterleavedData(pSamplesBuffer, (AkUInt16)m_uMaxProcessingBlockSize, 0, in_rFormat.channelConfig);

	return AK_Success;
}

// Terminates.
AKRESULT MixerBase::Term(AK::IAkPluginMemAlloc * in_pAllocator)
{
	m_mapInputs.Term();

	void * pSampleBuffer = m_customMixBuffer.DetachContiguousDeinterleavedData();
	if (pSampleBuffer)
	{
		AK_PLUGIN_FREE(in_pAllocator, pSampleBuffer);
	}

	// Effect's deletion
	AK_PLUGIN_DELETE(in_pAllocator, this);
	return AK_Success;
}

// Reset or seek to start.
AKRESULT MixerBase::Reset()
{
	// Reset only called after Init().
	memset(m_customMixBuffer.GetChannel(0), 0, m_uCustomMixBufferSize);
	return AK_Success;
}

// Effect info query.
AKRESULT MixerBase::GetPluginInfo(AkPluginInfo & out_rPluginInfo)
{
	out_rPluginInfo.eType = AkPluginTypeMixer;
	out_rPluginInfo.bIsInPlace = true;
	out_rPluginInfo.bIsAsynchronous = false;
	out_rPluginInfo.uBuildVersion = AK_WWISESDK_VERSION_COMBINED;
	return AK_Success;
}

void MixerBase::OnInputConnected(AK::IAkMixerInputContext * in_pInput)
{
	// In this demo, mono inputs in 3D positioning mode will be considered for custom processing, while all other inputs are passed through. 
	// However, channel configuration is not known at this time.
}

void MixerBase::OnInputDisconnected(AK::IAkMixerInputContext * in_pInput)
{
	// Remove input from map. It has been stored in map if and only if user data was added to the input context.

	// Ensure that samples buffer is freed if it exists.
	Input * pInputData = (Input*)in_pInput->GetUserData();
	if (pInputData)
	{
		pInputData->Term(m_pAllocator);
		m_mapInputs.RemoveInput(in_pInput);
	}
}

void MixerBase::ConsumeInput(
	AK::IAkMixerInputContext *	in_pInputContext,	///< Context for this input. Carries non-audio data.
	AkRamp					in_baseVolume,		///< Base volume to apply to this input (prev corresponds to the beginning, next corresponds to the end of the buffer). This gain is agnostic of emitter-listener pair-specific contributions (such as distance level attenuation).
	AkRamp					in_emitListVolume,	///< Emitter-listener pair-specific gain. When there are multiple emitter-listener pairs, this volume equals 1, and pair gains are applied directly on the channel volume matrix (accessible via IAkMixerInputContext::GetSpatializedVolumes()). For custom processing of emitter-listener pairs, one should query each pair volume using IAkMixerInputContext::Get3DPosition(), then AkEmitterListenerPair::GetGainForConnectionType().
	AkAudioBuffer *			io_pInputBuffer,	///< Input audio buffer data structure. Plugins should avoid processing data in-place.
	AkAudioBuffer *			io_pMixBuffer		///< Output audio buffer data structure. Stored until call to GetOutput().
	)
{
	AKASSERT(io_pInputBuffer->uValidFrames == io_pMixBuffer->MaxFrames());

	// In this demo, mono inputs in 3D positioning mode are considered for custom processing, while all other inputs are passed through. 
	// A mono input may switch to 3D at any given time. Once it does, we compute panning gains ourselves and ignore gains computed by the sound engine, 
	// so we need to maintain previous volumes on our side in order to implement proper interpolation between frames. If the mode changes back to 2D, 
	// during the first frame following this, previous volumes will be invalid. There are two ways of handling this: either we implement a state machine and 
	// wait one more frame before using the engine's previous volumes, or we continue to manage them on our side. Here we choose the latter.

	// Among inputs that qualify for custom processing, each emitter position is added as a separate 3D object. Objects in Multiposition_MultiDirection mode, 
	// which is typically used to simulate wide sources, are therefore excluded. Proper processing of these objects would typically imply a constant power mix 
	// of all emitter-listener pairs ( sqrt(sum(xi^2)/N) ).
	// Note: Currently, this plugin does not deal (properly) with multiple listeners. 


	// See if this input qualifies for custom processing.
	bool bCustomProcessing = RequiresCustomProcessing(in_pInputContext, io_pInputBuffer);

	// Get user data that we attached in OnInputConnected().
	Input * pInputData = (Input*)in_pInputContext->GetUserData();
	if (bCustomProcessing
		&& !pInputData)
	{
		// Until now, this input has been using default spatialization as computed by the sound engine. Allocate data for handling it on our side.
		pInputData = m_mapInputs.AddInput(in_pInputContext);
		if (pInputData)
		{
			// If we didn't need this data in OnMixDone(), we could instead have allocated an "Input" structure and attached it to the input. 
			// Here, we attach it just to find it more efficiently in ConsumeInput().
			pInputData->Init(m_pAllocator);
			in_pInputContext->SetUserData(pInputData);
		}
		else
		{
			// Failed allocating data for custom processing. Fallback on default processing.
			bCustomProcessing = false;
		}
	}

	if (bCustomProcessing && in_pInputContext->GetPannerType() == Ak3D)
	{
		// Custom processing of 3D sounds:
		// Example: 
		// Let's manually replicate the MultiPositionType_SinglePosition and MultiPositionType_MultiSources modes in Wwise for the first listener (the multi-listener case is more complicated
		// and is ignored in this sample). To do so, obtain 3D panning gains for each emitter, scale with the appropriate pair-specific gain and mix into m_customMixBuffer. 
		// Use the fallback path for Multiposition_MultiDirection or when there are multiple listeners.

		AkUInt32 uNumRays = in_pInputContext->GetNum3DPositions();

		// Get first ray's listener.
		AkUInt32 uRay0Listener = 0;
		if (uNumRays > 0)
		{
			AkEmitterListenerPair ray;
			AKVERIFY(in_pInputContext->Get3DPosition(0, ray) == AK_Success);
			uRay0Listener = ray.ListenerMask();
		}

		// Ensure we can store those emitter-listener pairs.
		// Note: at the moment there is no way to match previous pairs with new ones if the game decided to reorder them, or destroy some of them.
		// Simply match according to the order. Destroyed pairs will cut abruptly.
		if (!pInputData->arPairs.Resize(uNumRays))
			return;

		for (AkUInt32 uRay = 0; uRay < uNumRays; uRay++)
		{
			AkEmitterListenerPair ray;
			AKVERIFY(in_pInputContext->Get3DPosition(uRay, ray) == AK_Success);
			// Bail out when listener changes.
			if (ray.ListenerMask() != uRay0Listener)
				break;

			AkUInt32 uPanningGainsSize = AK::SpeakerVolumes::Matrix::GetRequiredSize(io_pInputBuffer->GetChannelConfig().uNumChannels, io_pMixBuffer->NumChannels());
			AK::SpeakerVolumes::MatrixPtr volumesNext = (AK::SpeakerVolumes::MatrixPtr)AkAlloca(uPanningGainsSize);

			Compute3DPanningGains(in_pInputContext, ray, uRay, io_pInputBuffer->GetChannelConfig(), io_pMixBuffer->GetChannelConfig(), volumesNext);

			// Mix all input channels onto output channels of our custom mix buffer. 
			// The base volume is the input's volume * the pair-specific volume.
			EmitterListenerPairInfo & matchingStoredRay = pInputData->arPairs[uRay];
			AkReal32 fNextRayVolume = ray.GetGainForConnectionType(in_pInputContext->GetConnectionType());
			AkRamp baseVolume = in_baseVolume;
			baseVolume.fNext *= fNextRayVolume;

			// First time? Or silent in previous frame (see not in OnMixDone()). If it is, allocate a matrix of previous volumes on our input, and copy next volumes over.
			if (!matchingStoredRay.mxPrevVolumes)
			{
				matchingStoredRay.mxPrevVolumes = (AK::SpeakerVolumes::MatrixPtr)AK_PLUGIN_ALLOC(m_pAllocator, uPanningGainsSize);
				if (!matchingStoredRay.mxPrevVolumes)
				{
					// Failed allocating previous volumes. Bail out.
					continue;
				}
				AK::SpeakerVolumes::Matrix::Copy(matchingStoredRay.mxPrevVolumes, volumesNext, io_pInputBuffer->GetChannelConfig().uNumChannels, io_pMixBuffer->GetChannelConfig().uNumChannels);

				matchingStoredRay.fPrevRayVolume = fNextRayVolume;
			}

			baseVolume.fPrev *= matchingStoredRay.fPrevRayVolume;

			m_pEffectContext->GlobalContext()->MixNinNChannels(
				io_pInputBuffer,			// Input buffer.
				&m_customMixBuffer,			// Buffer with which the input buffer is mixed.
				baseVolume.fPrev,
				baseVolume.fNext,
				matchingStoredRay.mxPrevVolumes,
				volumesNext);

			// Store previous volume values for next frame.
			matchingStoredRay.fPrevRayVolume = fNextRayVolume;
			AK::SpeakerVolumes::Matrix::Copy(matchingStoredRay.mxPrevVolumes, volumesNext, io_pInputBuffer->GetChannelConfig().uNumChannels, io_pMixBuffer->GetChannelConfig().uNumChannels);
		}
	}
	else
	{
		// Fallback. Use spatialization values as computed by the sound engine. As was mentioned above, let's use our own previous values if we decided to handle them on our side.

		// Compute base volume for mixing (see definition of in_baseVolume and in_emitListVolume).
		AkRamp baseVolume = in_baseVolume * in_emitListVolume;
		
		// Get prev and next volume gains.
		AkUInt32 uAllocSize = AK::SpeakerVolumes::Matrix::GetRequiredSize(io_pInputBuffer->NumChannels(), io_pMixBuffer->NumChannels());
		AK::SpeakerVolumes::MatrixPtr volumesPrev = (AK::SpeakerVolumes::MatrixPtr)AkAlloca(uAllocSize);
		AK::SpeakerVolumes::MatrixPtr volumesNext = (AK::SpeakerVolumes::MatrixPtr)AkAlloca(uAllocSize);
		
		ComputeFallbackPanningGains(
			in_pInputContext,	// Context for this input. Carries non-audio data.
			io_pInputBuffer->GetChannelConfig(),	// Channel config of the input.
			io_pMixBuffer->GetChannelConfig(),		// Channel config of the output (mix).
			volumesPrev,							// Returned volume matrix.
			volumesNext								// Returned volume matrix.
			); 
		
		// Override previous volumes with stored volumes (using first pair) if applicable.
		AK::SpeakerVolumes::MatrixPtr storedPrevVolumes = (pInputData && pInputData->arPairs.Length() > 1) ? pInputData->arPairs[0].mxPrevVolumes : NULL;
		if (storedPrevVolumes)
			AK::SpeakerVolumes::Matrix::Copy(volumesPrev, storedPrevVolumes, io_pInputBuffer->NumChannels(), io_pMixBuffer->NumChannels());

		// Mix.
		m_pEffectContext->GlobalContext()->MixNinNChannels(
			io_pInputBuffer,		// Input buffer.
			io_pMixBuffer,			// Buffer with which the input buffer is mixed.
			baseVolume.fPrev,
			baseVolume.fNext,
			volumesPrev,
			volumesNext);

		// Store volumes for next frame if applicable.
		if (storedPrevVolumes)
			AK::SpeakerVolumes::Matrix::Copy(storedPrevVolumes, volumesNext, io_pInputBuffer->NumChannels(), io_pMixBuffer->NumChannels());
	}

	// Indicate that we have visited this input. Inputs that are below volume threshold are not mixed (ConsumeInput() is not called). 
	if (pInputData)
		pInputData->bVisited = true;

	// At least one input has been mixed. Set number of valid output samples.
	io_pMixBuffer->uValidFrames = io_pInputBuffer->uValidFrames;
}

void MixerBase::OnMixDone(
	AkAudioBuffer *	io_pMixBuffer		// Output audio buffer data structure. Stored across calls to ConsumeInput().
	)
{
	// Mixing is done. Do whatever post-processing needed before handing out io_pMixBuffer.

	// In this example, we mix the custom mix buffer with the rest.
	for (AkUInt32 uChannel = 0; uChannel < io_pMixBuffer->GetChannelConfig().uNumChannels; uChannel++)
	{
		m_pEffectContext->GlobalContext()->MixChannel(m_customMixBuffer.GetChannel(uChannel), io_pMixBuffer->GetChannel(uChannel), 1.f, 1.f, io_pMixBuffer->uValidFrames);
	}

	memset(m_customMixBuffer.GetChannel(0), 0, m_uCustomMixBufferSize);

	// Inputs that are below volume threshold are not mixed (ConsumeInput() is not called).
	// Cycle through the list of inputs that required custom processing, and clear the previous volumes of those that have not been visited during this frame.
	// This way. we will not interpolate their panning gains once they become audible again.
	InputBuffersMap::Iterator it = m_mapInputs.Begin();
	while (it != m_mapInputs.End())
	{
		Input * pInput = (*it).pUserData;
		AKASSERT(pInput);
		if (!pInput->bVisited)
			pInput->ClearEmitListPairs(m_pAllocator);
		pInput->bVisited = false;
		++it;
	}
}

void MixerBase::OnEffectsProcessed(
	AkAudioBuffer *				/*io_pMixBuffer*/	///< Output audio buffer data structure. Stored across calls to ConsumeInput().
	)
{
}

void MixerBase::OnFrameEnd(
	AkAudioBuffer *				/*io_pMixBuffer*/,	///< Output audio buffer data structure. Stored across calls to ConsumeInput().
	AK::IAkMetering *			in_pMetering		///< Interface for retrieving metering data computed on io_pMixBuffer. 
	)
{
	/// Test: Metering from mixer plugins.
	if (in_pMetering)
	{
		AK::SpeakerVolumes::ConstVectorPtr peak = in_pMetering->GetPeak();
		AK::SpeakerVolumes::ConstVectorPtr truePeak = in_pMetering->GetTruePeak();
		AK::SpeakerVolumes::ConstVectorPtr rms = in_pMetering->GetRMS();
		AkReal32 fKPower = in_pMetering->GetKWeightedPower();
	}

	if (0)
	{
		AkMeteringFlags eFlags = AK_EnableBusMeter_KPower;
		eFlags = AK_NoMetering;
		eFlags = AK_EnableBusMeter_Peak;
		eFlags = AK_EnableBusMeter_TruePeak;
		eFlags = AK_EnableBusMeter_RMS;
		m_pEffectContext->EnableMetering(eFlags);
	}
}

// See if this input qualifies for custom processing  (3D + mono or 1.1, not Multiposition_MultiDirection).
bool MixerBase::RequiresCustomProcessing(
	AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
	AkAudioBuffer *				io_pInputBuffer		// Channel config of the input.
	)
{
	// Mono?
	AkChannelConfig channelConfigNoLfe = io_pInputBuffer->GetChannelConfig().RemoveLFE();
	bool bCustomProcessing = (channelConfigNoLfe.uNumChannels == 1);
	if (bCustomProcessing)
	{
		// See if we are not bypassed, as per custom effect parameters.
		bCustomProcessing = !IsCustomProcessingBypassed(in_pInputContext);
	}

	// 3D?
	bCustomProcessing = bCustomProcessing &&
		(in_pInputContext->IsSpatializationEnabled() && in_pInputContext->GetPannerType() == Ak3D);

	// In this sample, global busses and Multiposition_MultiDirection game objects do not support custom processing. 
	if (bCustomProcessing)
	{
		AK::IAkVoicePluginInfo * pGameObject = in_pInputContext->GetVoiceInfo();
		if (!pGameObject)
			bCustomProcessing = false;
		else
			bCustomProcessing = (pGameObject->GetGameObjectMultiPositionType() != AK::SoundEngine::MultiPositionType_MultiDirections);
	}

	return bCustomProcessing;
}

void MixerBase::ComputeFallbackPanningGains(
	AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
	AkChannelConfig				in_channelConfigIn,	// Channel config of the input.
	AkChannelConfig				in_channelConfigOut,// Channel config of the output (mix).
	AK::SpeakerVolumes::MatrixPtr out_mxPrevVolumes,// Returned volume matrix.
	AK::SpeakerVolumes::MatrixPtr out_mxNextVolumes	// Returned volume matrix.
	)
{
	// Get volume gains computed by Wwise.
	in_pInputContext->GetSpatializedVolumes(out_mxPrevVolumes, out_mxNextVolumes);
}

#pragma once

#include <math.h>
#include "AK/SoundEngine/Common/AkTypes.h"

#include "RS3DCommon.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//AkVectorExtensions
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct AkVectorExt {
	static AkVector cross(AkVector a, AkVector b) {
		AkVector c;	c.X = a.Y*b.Z - a.Z*b.Y;	c.Y = a.Z*b.X - a.X*b.Z;	c.Z = a.X*b.Y - a.Y*b.X;	return c;
	}
	static AkVector convertToStd(AkVector a, AkVector b1, AkVector b2, AkVector b3) {//convert vector a along basis (b1,b2,b3) to standard basis 
																					 //usv3 = sX * listener.position.OrientationFront + sZ * listener.position.OrientationTop + sY * Cross(listener.position.OrientationFront, listener.position.OrientationTop);
		AkVector c1, c2, c3;	c1 = AkVectorExt::scalar_mult(b1, a.X);	c2 = AkVectorExt::scalar_mult(b2, a.Y); c3 = AkVectorExt::scalar_mult(b3, a.Z); return AkVectorExt::add(AkVectorExt::add(c1, c2), c3);
	}
	static AkVector add(AkVector a, AkVector b) {
		AkVector c;	c.X = a.X + b.X;	c.Y = a.Y + b.Y;	c.Z = a.Z + b.Z;	return c;
	}
	static AkVector subtract(AkVector a, AkVector b) {
		AkVector c;	c.X = a.X - b.X;	c.Y = a.Y - b.Y;	c.Z = a.Z - b.Z;	return c;
	}
	static AkVector scalar_mult(AkVector a, AkReal32 b) {									//scalar multiplication
		AkVector c;	c.X = a.X * b;	c.Y = a.Y * b; c.Z = a.Z * b;	return c;
	}
	static AkReal32 norm(AkVector a) {
		return sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
	}
	static AkVector normalize(AkVector a) {
		AkReal32 n = AkVectorExt::norm(a);
		AkVector c;	c.X = a.X / n;	c.Y = a.Y / n; c.Z = a.Z / n;	return c;
	}
	static AkVector computeAzimuthElevationDistZXY(AkVector a) {	//Azimuth, elevation are in physics spherical coordinate system, roll-pitch-yaw is spun about Z-X-Y axes
		AkVector c;
		c.X = atan2(a.Z, a.X);					//Azimuth
		c.Z = AkVectorExt::norm(a);				//Dist
		c.Y = _M_PI / 2 - atan2(a.Y, c.Z);		//Elevation
		return c;
	}

	static AkVector convert2Ambisonics(AkVector a) {
		AkVector b;
		b.X = a.Z;
		b.Y = -a.X;
		b.Z = a.Y;
		return b;
	}
};

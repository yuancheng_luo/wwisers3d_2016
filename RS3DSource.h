#pragma once

#include <AK/SoundEngine/Common/AkTypes.h>
#include "RS3DCommon.h"

class RS3DSourceKey {
public:
	RS3DSourceKey(AkGameObjectID _Ak_game_object_ID = 0, int _sub_object_ID = 0);

	AkGameObjectID Ak_game_object_ID;
	int			   sub_object_ID;

	bool operator<(const RS3DSourceKey& RS3D_source_key) const;
	bool operator==(const RS3DSourceKey& RS3D_source_key) const;
};

class RS3DSource {
public:
	RS3DSource();
	RS3DSource(RS3DSourceKey _RS3D_source_key);
	void init();

	RS3DSourceKey		get_RS3D_source_key() const;

	void		update_position(const AkVector & _position, float learning_rate);										//Should be called in audio-thread, learning_rate [0, 1] range
	AkVector	get_position() const;

	void		update_min_dist(double _min_dist);
	void		update_max_dist(double _max_dist);

	double		get_min_dist() const;
	double		get_max_dist() const;


private:
	AkVector position;
	double min_dist;
	double max_dist;

	RS3DSourceKey RS3D_source_key;
};
#pragma once

#include <map>
#include <complex>

#ifdef NO_THREAD_MUTEX
#else
#include <thread>
#include <mutex>
#endif

#include <AK/SoundEngine/Common/AkTypes.h>

#include "RS3DAllocator.h"

#include "RS3DSource.h"
#include "RS3DListener.h"
#include "RS3DRoom.h"

#include "RS3DSpatializer.h"


#include "RS3DGameBlob.h"

#include "RS3DCommon.h"

////////////////////////////////////////////////////////////
typedef int RS3DTailKey;			//Room ID

#define RS3DGameDataRoomMap std::map<RS3DTailKey, RS3DGameDataRoom*, std::less<RS3DTailKey>, EXTERN_ALLOCATOR<std::pair<const RS3DTailKey, RS3DGameDataRoom*> > >
#define RS3DTailMap std::map<RS3DTailKey, RS3DTail*, std::less<RS3DTailKey>, EXTERN_ALLOCATOR<std::pair<const RS3DTailKey, RS3DTail*> > >

////////////////////////////////////////////////////////////
class RS3DTail {
public:
	RS3DTail();		//Dummy constructor
	RS3DTail(const RS3DGameDataRoom & _RS3D_game_data_room, const _source_information & _source_info, const _source_work_area & _work_area);

	void setTail(const RS3DGameDataRoom & _RS3D_game_data_room, const _source_information & _source_info, const _source_work_area & _work_area);
	void getTailBlob(unsigned char *& tail_blob, int & tail_blob_size);	//Allocate tail_blob and format it for vsengine hash

	RS3DGameDataRoom RS3D_game_data_room;


private:

	struct TailDescriptor {
		int	_data_chunk_size;
		int	_rir_length;
		int	_rir_realtime_order;
		int	_rir_fixdtail_order;
		float	_listnr_ixyz[3];
		float	_roomxe_size[3];
		float	_rflctn_coef[6];
	};

	TailDescriptor tail_descriptor;
	RS3DVector(std::complex<float>) tail_data;
};
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
class RS3DTailMgr {
public:
	RS3DTailMgr(const RS3DListener & _RS3D_listener = RS3DListener(),
		int _num_samples_in_buffer = 1024,
		int _sample_rate = 48000,
		float _unit_scale_factor = 1);

	~RS3DTailMgr();

	//Mutex locking functions
	//bool addTail(	RS3DTailKey RS3D_tail_key,
	//				const RS3DRoom & RS3D_room);	//Add tail to hash map if key doesn't exist, update tail's room descriptor differs
	//bool removeTail(RS3DTailKey RS3D_tail_key);		//Remove tail from hash map if it exists

	bool setTailsToGameBlobRooms(const RS3DGameBlob & RS3D_game_blob);	//Uses game blob's room ID for RS3DTailKey, sets tail hashmap to game blob rooms

	bool getTailBlob(RS3DTailKey RS3D_tail_key, unsigned char *& tail_blob, int & tail_blob_size);		//if key found, pack tail into a blob allocated on the heap 

private:

#ifdef NO_THREAD_MUTEX
#else
	std::thread * create_thread;
	std::mutex mgr_mutex;
	bool thread_active;
#endif

	void threadedAddTailToMap();
	RS3DTail * threadedCreateTail(RS3DTailKey RS3D_tail_key);



	const RS3DListener & RS3D_listener;

	int num_samples_per_buffer;
	int sample_rate;
	float unit_scale_factor;

	bool update_tails;

	RS3DGameDataRoomMap RS3D_game_data_room_map;
	
	RS3DGameDataRoomMap threaded_RS3D_game_data_room_map;
	RS3DTailMap threaded_RS3D_tail_map;

	void delete_game_data_room_map();
};
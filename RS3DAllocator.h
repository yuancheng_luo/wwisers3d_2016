#pragma once

#include <stdlib.h>
#include <new>
#include <limits>

#include <vector>
#include <complex>

#include "fftw3.h"
#include "RS3DCommon.h"

#include "AK/SoundEngine/Common/IAkPluginMemAlloc.h"

extern AK::IAkPluginMemAlloc *		global_plugin_mem_alloc;

//Choose allocator class
#ifdef USE_EXTERNAL_MEMORY
	#define EXTERN_ALLOCATOR				mmap_allocator
	#define RS3D_NEW(_what)					AK_PLUGIN_NEW(global_plugin_mem_alloc, _what)    
	#define RS3D_DELETE(_what)				AK_PLUGIN_DELETE(global_plugin_mem_alloc, _what)
	#define RS3D_ALLOC(_size)				AK_PLUGIN_ALLOC(global_plugin_mem_alloc, _size)
	#define RS3D_FREE(_what)				AK_PLUGIN_FREE(global_plugin_mem_alloc, _what)
#else
	#define EXTERN_ALLOCATOR				std::allocator
	#define RS3D_NEW(_what)					new _what	 
	#define RS3D_DELETE(_what)				delete _what 
	#define RS3D_ALLOC(_size)				malloc(_size)
	#define RS3D_FREE(_what)				free(_what)
#endif



//Vector class
#define RS3DFloatVector  std::vector<float, EXTERN_ALLOCATOR<float> >
#define RS3DComplexVector  std::vector<std::complex<float>, EXTERN_ALLOCATOR<std::complex<float> > >
#define RS3DVector(T)		std::vector<T, EXTERN_ALLOCATOR<T> >



//Global function calls to 
extern "C"{
	void *AkPlugin_malloc(size_t size);
	void AkPlugin_free(void *ptr);
}



//Allocator class 
template <typename T>
class mmap_allocator : public std::allocator<T>
{
public:
	typedef size_t size_type;
	typedef T* pointer;
	typedef const T* const_pointer;

	template<typename _Tp1>
	struct rebind
	{
		typedef mmap_allocator<_Tp1> other;
	};

	pointer allocate(size_type n, const void *hint = 0)
	{
		if (global_plugin_mem_alloc)
			return reinterpret_cast<pointer>(AK_PLUGIN_ALLOC(global_plugin_mem_alloc, n * sizeof(T)));
		else
			return std::allocator<T>::allocate(n, hint);
	}

	void deallocate(pointer p, size_type n)
	{
		if (global_plugin_mem_alloc)
			AK_PLUGIN_FREE(global_plugin_mem_alloc, p);
		else
			return std::allocator<T>::deallocate(p, n);
	}

	mmap_allocator() throw() : std::allocator<T>() {  }
	mmap_allocator(const mmap_allocator &a) throw() : std::allocator<T>(a) { }
	template <class U>
	mmap_allocator(const mmap_allocator<U> &a) throw() : std::allocator<T>(a) { }
	~mmap_allocator() throw() { }
};

#include "RS3DGameBlob.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RS3DGameBlob::RS3DGameBlob() {
	RS3D_game_data_state_header.version_number = 1.0;
	RS3D_game_data_state_header.tick = 0;
	RS3D_game_data_state_header.num_rooms = 0;
	RS3D_game_data_state_header.num_occluders = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::addRoom(const RS3DGameDataRoom & _RS3D_game_data_room) {
	RS3D_game_data_rooms.push_back(_RS3D_game_data_room);
	RS3D_game_data_state_header.num_rooms = RS3D_game_data_rooms.size();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::addSingleTriangleOccluder(const RS3DGameDataSingleTriangleOccluder & _RS3D_game_data_single_triangle_occluder) {
	RS3D_game_data_single_triangle_occluders.push_back(_RS3D_game_data_single_triangle_occluder);
	RS3D_game_data_state_header.num_occluders = RS3D_game_data_single_triangle_occluders.size();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::updateRoom(const RS3DGameDataRoom & _RS3D_game_data_room, int idx) {
	if (idx < RS3D_game_data_rooms.size())
		RS3D_game_data_rooms[idx] = _RS3D_game_data_room;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::updateSingleTriangleOccluder(const RS3DGameDataSingleTriangleOccluder & _RS3D_game_data_single_triangle_occluder, int idx) {
	if (idx < RS3D_game_data_single_triangle_occluders.size())
		RS3D_game_data_single_triangle_occluders[idx] = _RS3D_game_data_single_triangle_occluder;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::sendData(void *& ptr, int & bytes_size) {

	//Compute total size
	bytes_size = RS3D_game_data_state_header.computeExpectedBlobSize();

	if (data_buffer.size() < bytes_size)
		data_buffer.resize(bytes_size);

	void * p = &data_buffer[0];

	//Write header
	RS3D_game_data_state_header.tick++;	//Increment tick
	BLOB_WRITER<RS3DGameDataStateHeader>(p, RS3D_game_data_state_header);

	//Write room data
	for (int i = 0; i < RS3D_game_data_state_header.num_rooms; ++i)
		BLOB_WRITER<RS3DGameDataRoom>(p, RS3D_game_data_rooms[i]);

	//Write occluder data
	for (int i = 0; i < RS3D_game_data_state_header.num_occluders; ++i)
		BLOB_WRITER<RS3DGameDataSingleTriangleOccluder>(p, RS3D_game_data_single_triangle_occluders[i]);

	ptr = &data_buffer[0];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline AkVector matrixVecProd4x4(const AkVector & vec, float A[4][4]) {
	AkVector out;
	out.X = A[0][0] * vec.X + A[0][1] * vec.Y + A[0][2] * vec.Z + A[0][3];
	out.Y = A[1][0] * vec.X + A[1][1] * vec.Y + A[1][2] * vec.Z + A[1][3];
	out.Z = A[2][0] * vec.X + A[2][1] * vec.Y + A[2][2] * vec.Z + A[2][3];
	return out;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void RS3DGameBlob::sendData(void *& ptr, int & bytes_size, float mtxTransform[4][4]) {
	//Compute total size
	bytes_size = RS3D_game_data_state_header.computeExpectedBlobSize();

	if (data_buffer.size() < bytes_size)
		data_buffer.resize(bytes_size);

	void * p = &data_buffer[0];

	//Write header
	RS3D_game_data_state_header.tick++;	//Increment tick
	BLOB_WRITER<RS3DGameDataStateHeader>(p, RS3D_game_data_state_header);

	//Write room data
	for (int i = 0; i < RS3D_game_data_state_header.num_rooms; ++i) {
		RS3DGameDataRoom transformed_room = RS3D_game_data_rooms[i];
		transformed_room.center = matrixVecProd4x4(transformed_room.center, mtxTransform);

		transformed_room.dimensions = matrixVecProd4x4(transformed_room.dimensions, mtxTransform);
		transformed_room.dimensions.X = std::abs(transformed_room.dimensions.X);
		transformed_room.dimensions.Y = std::abs(transformed_room.dimensions.Y);
		transformed_room.dimensions.Z = std::abs(transformed_room.dimensions.Z);

		BLOB_WRITER<RS3DGameDataRoom>(p, transformed_room);
	}

	//Write occluder data
	for (int i = 0; i < RS3D_game_data_state_header.num_occluders; ++i) {
		RS3DGameDataSingleTriangleOccluder transformed_occluder = RS3D_game_data_single_triangle_occluders[i];
		transformed_occluder.vertex_position[0] = matrixVecProd4x4(transformed_occluder.vertex_position[0], mtxTransform);
		transformed_occluder.vertex_position[1] = matrixVecProd4x4(transformed_occluder.vertex_position[1], mtxTransform);
		transformed_occluder.vertex_position[2] = matrixVecProd4x4(transformed_occluder.vertex_position[2], mtxTransform);

		BLOB_WRITER<RS3DGameDataSingleTriangleOccluder>(p, transformed_occluder);
	}
	ptr = &data_buffer[0];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RS3DGameBlob::getData(void * ptr, int out_rDataSize) {
	//Read header
	RS3DGameDataStateHeader tmp_game_data_state_header;
	BLOB_READER<RS3DGameDataStateHeader>(ptr, tmp_game_data_state_header);

	if (tmp_game_data_state_header.tick != RS3D_game_data_state_header.tick) {
		//Update only if header tick changes

		RS3D_game_data_state_header = tmp_game_data_state_header;

		int expected_bytes_size = RS3D_game_data_state_header.computeExpectedBlobSize();
		assert(out_rDataSize == expected_bytes_size);

		//Read room data
		RS3D_game_data_rooms.resize(RS3D_game_data_state_header.num_rooms);
		for (int i = 0; i < RS3D_game_data_state_header.num_rooms; ++i)
			BLOB_READER<RS3DGameDataRoom>(ptr, RS3D_game_data_rooms[i]);

		//Read occluder data
		RS3D_game_data_single_triangle_occluders.resize(RS3D_game_data_state_header.num_occluders);
		for (int i = 0; i < RS3D_game_data_state_header.num_occluders; ++i)
			BLOB_READER<RS3DGameDataSingleTriangleOccluder>(ptr, RS3D_game_data_single_triangle_occluders[i]);
	}
	else
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline int RS3DGameDataStateHeader::computeExpectedBlobSize() {
	return sizeof(RS3DGameDataStateHeader) + sizeof(RS3DGameDataRoom) * num_rooms + sizeof(RS3DGameDataSingleTriangleOccluder) * num_occluders;
}

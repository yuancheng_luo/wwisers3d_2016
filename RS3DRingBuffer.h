#pragma once

#include <vector>

#include "RS3DCommon.h"

#include "RS3DAllocator.h"

template <class T>
class RS3DRingBuffer {
public:
	RS3DRingBuffer(uint32 _buffer_length = 0, uint32 _write_idx = 0, uint32 _read_idx = 0);
	void write(const RS3DVector(T) & input_buffer);															//Write to dat, increment write_idx by input_buffer len and modulo buffer_length
	void writeAdd(const RS3DVector(T) & input_buffer);														//Above but adds values of input_buffer to values of dat
	void writeAdd(const RS3DVector(T) & input_buffer, uint32 _write_idx_offset);							//Above but next write_idx = write_idx  + _write_idx_offset modulo buffer_length
	void read(uint32 num_samples, RS3DVector(T) & output_buffer);											//Read num_samples of dat starting from internal read_idx modulo buffer_length into output_buffer, moves read_idx
	void readAndClear(uint32 num_samples, RS3DVector(T) & output_buffer);									//Above but 0's values from dat as it reads
	void readFromIdx(uint32 _read_idx, uint32 num_samples, RS3DVector(T) & output_buffer) const;			//Read num_samples of dat starting from read_idx modulo buffer_length into output_buffer
	void readSamplesBeforeWriteIdx(uint32 offset, uint32 num_samples, RS3DVector(T) & output_buffer) const;	//Read num_samples of dat starting from writeIdx - offset
	void readAndClearSamplesBeforeWriteIdx(uint32 offset, uint32 num_samples, RS3DVector(T) & output_buffer);//Above but 0's values from dat as it reads

private:
	uint32 buffer_length;
	RS3DVector(T) dat;
	uint32 write_idx;
	uint32 read_idx;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
RS3DRingBuffer<T>::RS3DRingBuffer(uint32 _buffer_length,
	uint32 _write_idx, uint32 _read_idx) {

	buffer_length = _buffer_length;
	dat.resize(buffer_length, (T)0);
	write_idx = _write_idx % buffer_length;
	read_idx = _read_idx % buffer_length;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::write(const RS3DVector(T) & input_buffer) {
	uint32 input_buffer_len = input_buffer.size();
	uint32 _write_idx = write_idx;
	for (uint32 i = 0; i < input_buffer_len; ++i) {
		dat[_write_idx++] = input_buffer[i];
		_write_idx = (_write_idx < buffer_length ? _write_idx : 0);
	}
	write_idx = _write_idx;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::writeAdd(const RS3DVector(T) & input_buffer) {
	uint32 input_buffer_len = input_buffer.size();
	uint32 _write_idx = write_idx;
	for (uint32 i = 0; i < input_buffer_len; ++i) {
		dat[_write_idx++] += input_buffer[i];
		_write_idx = (_write_idx < buffer_length ? _write_idx : 0);
	}
	write_idx = _write_idx;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::writeAdd(const RS3DVector(T) & input_buffer, uint32 _write_idx_offset) {
	uint32 input_buffer_len = input_buffer.size();
	uint32 _write_idx = write_idx;
	for (uint32 i = 0; i < input_buffer_len; ++i) {
		dat[_write_idx++] += input_buffer[i];
		_write_idx = (_write_idx < buffer_length ? _write_idx : 0);
	}
	write_idx = (write_idx + _write_idx_offset) % buffer_length;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::read(uint32 num_samples, RS3DVector(T) & output_buffer) {
	if (output_buffer.size() < num_samples)
		output_buffer.resize(num_samples);

	for (uint32 i = 0; i < num_samples; ++i) {
		output_buffer[i] = dat[read_idx++];
		read_idx = (read_idx < buffer_length ? read_idx : 0);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::readAndClear(uint32 num_samples, RS3DVector(T) & output_buffer) {
	if (output_buffer.size() < num_samples)
		output_buffer.resize(num_samples);

	for (uint32 i = 0; i < num_samples; ++i) {
		output_buffer[i] = dat[read_idx];
		dat[read_idx] = (T)0;
		++read_idx;
		read_idx = (read_idx < buffer_length ? read_idx : 0);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::readFromIdx(uint32 _read_idx, uint32 num_samples, RS3DVector(T) & output_buffer) const {
	if (output_buffer.size() < num_samples)
		output_buffer.resize(num_samples);

	_read_idx = _read_idx % buffer_length;
	for (uint32 i = 0; i < num_samples; ++i) {
		output_buffer[i] = dat[_read_idx++];
		_read_idx = (_read_idx < buffer_length ? _read_idx : 0);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::readSamplesBeforeWriteIdx(uint32 offset, uint32 num_samples, RS3DVector(T) & output_buffer) const {
	if (output_buffer.size() < num_samples)
		output_buffer.resize(num_samples);

	uint32 _read_idx = (write_idx + (buffer_length - (offset % buffer_length))) % buffer_length;
	for (uint32 i = 0; i < num_samples; ++i) {
		output_buffer[i] = dat[_read_idx++];
		_read_idx = (_read_idx  < buffer_length ? _read_idx : 0);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void RS3DRingBuffer<T>::readAndClearSamplesBeforeWriteIdx(uint32 offset, uint32 num_samples, RS3DVector(T) & output_buffer){
	if (output_buffer.size() < num_samples)
		output_buffer.resize(num_samples);

	uint32 _read_idx = (write_idx + (buffer_length - (offset % buffer_length))) % buffer_length;
	for (uint32 i = 0; i < num_samples; ++i) {
		output_buffer[i] = dat[_read_idx];
		dat[_read_idx] = (T)0;
		++_read_idx;
		_read_idx = (_read_idx  < buffer_length ? _read_idx : 0);
	}
}

/***********************************************************************
The content of this file includes source code for the sound engine
portion of the AUDIOKINETIC Wwise Technology and constitutes "Level
Two Source Code" as defined in the Source Code Addendum attached
with this file.  Any use of the Level Two Source Code shall be
subject to the terms and conditions outlined in the Source Code
Addendum and the End User License Agreement for Wwise(R).

Version: <VERSION>  Build: <BUILDNUMBER>
Copyright (c) <COPYRIGHTYEAR> Audiokinetic Inc.
***********************************************************************/

//////////////////////////////////////////////////////////////////////
//
// MixerBase.h
//
// Mixer Plugin base implementation.
//
// Copyright 2013 Audiokinetic Inc. / All Rights Reserved
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <AK/SoundEngine/Common/IAkPlugin.h>
#include <AK/SoundEngine/Common/IAkMixerPlugin.h>
#include <AK/Tools/Common/AkArray.h>
#include <AK/Plugin/PluginServices/AkMixerInputMap.h>

//-----------------------------------------------------------------------------
// Name: class MixerBase
// Base implementation for mixers
//-----------------------------------------------------------------------------

// This base class is a sample. If you use it as is, at least change the namespace to avoid linking clashes with other mixer plugins using this base class.
namespace MIXERSTUB
{
	class MixerBase : public AK::IAkMixerEffectPlugin
	{
	public:

		// Constructor/destructor
		MixerBase();
		~MixerBase();

		// Allocate memory needed by effect and other initializations
		virtual AKRESULT Init(
			AK::IAkPluginMemAlloc *		in_pAllocator,				///< Interface to memory allocator to be used by the effect
			AK::IAkMixerPluginContext *	in_pMixerPluginContext,		///< Interface to mixer plug-in's context
			AK::IAkPluginParam *		in_pParams,					///< Interface to plug-in parameters
			AkAudioFormat &				in_rFormat					///< Audio data format of the input/output signal.
			);

		// Free memory used by effect and effect termination
		virtual AKRESULT Term(AK::IAkPluginMemAlloc * in_pAllocator);

		// Reset or seek to start (looping).
		virtual AKRESULT Reset();

		// Effect type query.
		virtual AKRESULT GetPluginInfo(AkPluginInfo & out_rPluginInfo);

		virtual void OnInputConnected(
			AK::IAkMixerInputContext * in_pInput
			);
		virtual void OnInputDisconnected(
			AK::IAkMixerInputContext * in_pInput
			);
		virtual void ConsumeInput(
			AK::IAkMixerInputContext *	in_pInputContext,	///< Context for this input. Carries non-audio data.
			AkRamp						in_baseVolume,		///< Base volume to apply to this input (prev corresponds to the beginning, next corresponds to the end of the buffer). This gain is agnostic of emitter-listener pair-specific contributions (such as distance level attenuation).
			AkRamp						in_emitListVolume,	///< Emitter-listener pair-specific gain. When there are multiple emitter-listener pairs, this volume equals 1, and pair gains are applied directly on the channel volume matrix (accessible via IAkMixerInputContext::GetSpatializedVolumes()). For custom processing of emitter-listener pairs, one should query each pair volume using IAkMixerInputContext::Get3DPosition(), then AkEmitterListenerPair::GetGainForConnectionType().
			AkAudioBuffer *				io_pInputBuffer,	///< Input audio buffer data structure. Plugins should avoid processing data in-place.
			AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored until call to GetOutput().
			);
		virtual void OnMixDone(
			AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored across calls to ConsumeInput().
			);
		virtual void OnEffectsProcessed(
			AkAudioBuffer *				io_pMixBuffer		///< Output audio buffer data structure. Stored across calls to ConsumeInput().
			);
		virtual void OnFrameEnd(
			AkAudioBuffer *				io_pMixBuffer,		///< Output audio buffer data structure.
			AK::IAkMetering *			in_pMetering		///< Interface for retrieving metering data computed on io_pMixBuffer. May be NULL if metering is not enabled.
			) = 0;

	protected:

		// Interface definition for implementations based on this one.

		// Returns true if "custom" processing is bypassed for this input.
		virtual bool IsCustomProcessingBypassed(
			AK::IAkMixerInputContext *	in_pInputContext	///< Context for this input. Carries non-audio data.
			) = 0;

		// See if this input qualifies for custom processing  (3D + mono or 1.1, not Multiposition_MultiDirection).
		virtual bool RequiresCustomProcessing(
			AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
			AkAudioBuffer	*			io_pInputBuffer		// Channel config of the input.
			);

		virtual void Compute3DPanningGains(
			AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
			AkEmitterListenerPair &		in_emitListPair,	// Emitter-listener pair.
			AkUInt32					in_uPairIndex,		// Index of the emitter-listener pair.
			AkChannelConfig				in_channelConfigIn,	// Channel config of the input.
			AkChannelConfig				in_channelConfigOut,// Channel config of the output (mix).
			AK::SpeakerVolumes::MatrixPtr out_mxVolumes		// Returned volume matrix.
			) = 0;

		virtual void ComputeFallbackPanningGains(
			AK::IAkMixerInputContext *	in_pInputContext,	// Context for this input. Carries non-audio data.
			AkChannelConfig				in_channelConfigIn,	// Channel config of the input.
			AkChannelConfig				in_channelConfigOut,// Channel config of the output (mix).
			AK::SpeakerVolumes::MatrixPtr out_mxPrevVolumes,// Returned volume matrix.
			AK::SpeakerVolumes::MatrixPtr out_mxNextVolumes	// Returned volume matrix.
			);

	protected:

		AK::IAkPluginMemAlloc *		m_pAllocator;
		AK::IAkMixerPluginContext * m_pEffectContext;

		// Audio format information
		AkUInt32 m_uSampleRate;
		AkUInt32 m_uMaxProcessingBlockSize;

		// Bookkeeping of emitter-listener pairs.
		struct EmitterListenerPairInfo
		{
			EmitterListenerPairInfo()
			: id(0)	// currently unused
			, fPrevRayVolume(0.f)
			, mxPrevVolumes(NULL)
			{}
			~EmitterListenerPairInfo()
			{
				AKASSERT(mxPrevVolumes == NULL);
			}
			inline void Free(AK::IAkPluginMemAlloc * in_pAllocator)
			{
				if (mxPrevVolumes)
				{
					AK_PLUGIN_FREE(in_pAllocator, mxPrevVolumes);
					mxPrevVolumes = NULL;
				}
			}

			AkUInt32	id;
			AkReal32	fPrevRayVolume;
			AK::SpeakerVolumes::MatrixPtr	mxPrevVolumes;	// Previous volume matrix. 
		};
		typedef AkArray<EmitterListenerPairInfo, const EmitterListenerPairInfo&, AkPluginArrayAllocator> StoredEmitterListenerPairs;

		// Bookkeeping of inputs that qualify for custom processing.
		struct Input
		{
			Input() : bVisited(false) {}
			~Input() { arPairs.Term(); }
			inline void Init(AK::IAkPluginMemAlloc * in_pAllocator)
			{
				arPairs.Init(in_pAllocator);
			}
			inline void Term(AK::IAkPluginMemAlloc * in_pAllocator)
			{
				ClearEmitListPairs(in_pAllocator);
			}
			inline void ClearEmitListPairs(AK::IAkPluginMemAlloc * in_pAllocator)
			{
				for (StoredEmitterListenerPairs::Iterator it = arPairs.Begin(); it != arPairs.End(); ++it)
					(*it).Free(in_pAllocator);

				arPairs.RemoveAll();
			}

			bool							bVisited;		// Set each frame this input undergoes special processing. Reset at the end of the frame (in OnMixDone).
			StoredEmitterListenerPairs		arPairs;		// Array of emitter-listener pair data.
		};
		typedef AkMixerInputMap<Input> InputBuffersMap;
		InputBuffersMap m_mapInputs;

		// Mix buffer for custom processing of inputs.
		AkAudioBuffer m_customMixBuffer;
		AkUInt32 m_uCustomMixBufferSize;

	};
}

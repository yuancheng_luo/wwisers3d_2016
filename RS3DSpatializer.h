#pragma once

#include <algorithm>
#include <vector>

#ifdef NO_THREAD_MUTEX
#else
#include <thread>
#include <mutex>
#endif

#include <AK/SoundEngine/Common/AkTypes.h>

#include "RS3DAllocator.h"

#include "RS3DSource.h"
#include "RS3DRoom.h"
#include "RS3DListener.h"

//#include "RS3DTailMgr.h"

#include "RS3DGameBlob.h"

#include "RS3DCommon.h"
#include "vsengine.h"

class RS3DTailMgr;

//Every RS3DSpatializer object maps to exactly one of these keys
class RS3DSpatializerKey{
public:
	RS3DSpatializerKey();

	RS3DSpatializerKey(	RS3DSourceKey _source_key,
						RS3DRoomKey _room_key,
						RS3DListenerKey _listener_key);

	RS3DSpatializerKey(const RS3DSource & RS3D_source,
					   const RS3DRoom & RS3D_room, 
					   const RS3DListener & RS3D_listener);

	RS3DSourceKey source_key;
	RS3DRoomKey room_key;
	RS3DListenerKey listener_key;

	bool operator<(const RS3DSpatializerKey& RS3D_spatializer_key) const;

};


//Class that binds RS3DSource, RS3DRoom, and RS3DListener into structs for calls to vsengine
//Performs coordinate transform to VisiSonics engine coordinates
class RS3DSpatializer {
public:
	RS3DSpatializer(const RS3DSource & _RS3D_source = RS3DSource(),
					const RS3DRoom & _RS3D_room = RS3DRoom(),
					const RS3DListener & _RS3D_listener = RS3DListener(),
					RS3DTailMgr *_RS3D_tail_mgr = NULL,
					int _num_samples_in_buffer = 1024,
				    int _sample_rate = 48000,
					eRenderMode _render_mode = REGULAR,
					bool _smooth_doppler = false,
					double _unit_scale_factor = 1);
	
	~RS3DSpatializer();

	void update(eRenderMode _render_mode, bool _smooth_doppler); 	//Update vsengine data struct from RS3D objects
	void process(const AkReal32 * input_buffer_mono, 
		RS3DFloatVector & output_buffer_left,
		RS3DFloatVector & output_buffer_right);				//Spatializers input_buffer_mono into left-right output buffers

	RS3DSpatializerKey get_key() const;

	void clear_occluders();
	void add_occluders(const RS3DVector(RS3DGameDataSingleTriangleOccluder) & occluder_list, int _occluder_tick_ID);		//Clears spatializer linked-list, adds any occluders with one or more vertex that intersects room (not doing full intersection check)

	_source_information get_copy_active_source_info();
	_source_work_area get_copy_active_work_area();

	enum ATTENUATION_FUNC {ATTENUATE_ONE_OVER_R, ATTENUATE_ONE_OVER_SQRT_R, ATTENUATE_NONE};

protected:
	const RS3DSpatializerKey RS3D_spatializer_key;							//Uniquely identifies spatializer using ID's from source, room, and listener

	const RS3DSource & RS3D_source;
	const RS3DRoom & RS3D_room;
	const RS3DListener & RS3D_listener;

	RS3DTailMgr * RS3D_tail_mgr;

	const int num_samples_in_buffer;
	const int sample_rate;
	const double unit_scale_factor;

	bool first_create_vsengine_source;	

	//Buffers
	RS3DFloatVector  left_channel_output_buffer;
	RS3DFloatVector  right_channel_output_buffer;

private:
	////////vsengine specific
	eRenderMode render_mode;
	bool smooth_doppler;

	double	compute_attenuation() const;						//Compute from RS3DSource and RS3Dlistener
	double	compute_attenuation(const _source_information & source_info_ref) const;						//Compute from RS3DSource and RS3Dlistener

	double	compute_attenuation_dist_to_box(ATTENUATION_FUNC attenuation_func) const;
	double	compute_attenuation_dist_to_box(const _source_information & source_info_ref, ATTENUATION_FUNC attenuation_func) const;

	double	compute_source_listener_distance_m() const; 
	
	double	compute_box_listener_distance_m(const _source_information & source_info_ref) const;
	double	compute_box_listener_distance_m() const;

	int occluder_tick_ID;

	//VisiSonics engine related, refactor below 2 chunks later
	_source_information			source_info_A;
	_source_work_area			source_work_area_A;
	unsigned char * external_work_area_ptr_A;
	bool source_initialized_A;

	_source_information			source_info_B;
	_source_work_area			source_work_area_B;
	unsigned char * external_work_area_ptr_B;
	bool source_initialized_B;

	enum RS3D_SOURCE_AB{SOURCE_A, SOURCE_B};
	RS3D_SOURCE_AB active_source;

	int source_AB_fade_frames;

#ifdef NO_THREAD_MUTEX
#else
	std::thread * create_thread;
	std::mutex  create_vsengine_source_mutex;
	bool thread_active;
	void threaded_create_vsengine_source();
	bool active_threaded_source_swapped;		
	void swap_active_threaded_sources();
#endif

	void update_vsengine_source_info_source(_source_information & source_info_ref);
	void update_vsengine_source_info_room(_source_information & source_info_ref);
	void update_vsengine_source_info_listener(_source_information & source_info_ref);

	bool vsengine_source_comparator(const _source_information & curr_source_info, const _source_information & prev_source_info);
	bool listener_in_room(const _source_information & curr_source_info);

	void prepare_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref);
	void create_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref);
	
	void destroy_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref);
	void clear_vsengine_source(_source_information & source_info_ref, _source_work_area & source_work_area_ref, unsigned char *& external_work_area_ptr_ref);		//0's source, workarea and external ptr

	void clear_occluders(_source_information & source_info_ref);

	enum RS3D_SOURCE_FADE{NO_FADE, FADE_IN, FADE_OUT};

	void process_source_ref(
		_source_information & source_info_ref, _source_work_area & source_work_area_ref,
		RS3D_SOURCE_FADE fade_mode, int fade_frame, int max_fade_frames,
		bool bypass_output,
		const AkReal32 * input_buffer_mono,
		RS3DFloatVector & output_buffer_left,
		RS3DFloatVector & output_buffer_right);				//Spatializers input_buffer_mono into left-right output buffers
};
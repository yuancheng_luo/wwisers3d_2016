#pragma once

#include <vector>
#include <string>

#include "fftw3.h"

#include "RS3DCommon.h"

#include "RS3DAllocator.h"
#include "RS3DRingBuffer.h"

#include "libresample.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
enum BACCH_STEREO_TYPE {BACCH_STEREO_FRONT, BACCH_STEREO_BACK};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RS3DBACCHBusKey {
public:
	RS3DBACCHBusKey(AkUniqueID _Ak_unique_ID = 0, int _channel_ID = 0);

	AkGameObjectID Ak_unique_ID;
	int			   channel_ID;

	bool operator<(const RS3DBACCHBusKey & RS3D_BACCH_key) const;
	bool operator==(const RS3DBACCHBusKey & RS3D_BACCH_key) const;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct BACCHFilter {
	int num_samples_in_filter;
	int num_samples_in_resampled_filter;
	int sampling_rate;

	RS3DComplexVector left_2_left_filter;
	RS3DComplexVector right_2_left_filter;
	RS3DComplexVector left_2_right_filter;
	RS3DComplexVector right_2_right_filter;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define RS3DBACCHBusRingBufferMap std::map<RS3DBACCHBusKey, RS3DRingBuffer<float>*, std::less<RS3DBACCHBusKey>, EXTERN_ALLOCATOR<std::pair<const RS3DBACCHBusKey, RS3DRingBuffer<float>*> > >

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Loads BACCH filters from file, filters output
class RS3DBACCH {
public:
	RS3DBACCH();

	RS3DBACCH(int _num_samples_in_buffer,					//number of samples in the input buffer to be convolved with BACCH filter
		int _sampling_rate,
		float _gain_multiplier,
		void * data_chunk);


	~RS3DBACCH();


	void process(	const RS3DFloatVector & left_channel_input_buffer,
					const RS3DFloatVector  & right_channel_input_buffer,
					RS3DRingBuffer<float>  & left_channel_output_ring_buffer,
					RS3DRingBuffer<float>  & right_channel_output_ring_buffer);

	int get_num_samples_in_output() const ;

private:
	bool ready_to_process;

	int num_samples_in_buffer;
	int num_samples_in_output;

	BACCHFilter BACCH_filter;

	fftwf_plan plan_forward;
	fftwf_plan plan_backward;
	RS3DFloatVector real_scratch;
	RS3DComplexVector complex_scratch;
	
	RS3DComplexVector complex_scratch_left, complex_scratch_right;

	void complexScratchToRingBuffer(RS3DRingBuffer<float>  & ring_buffer);// add ifft(complex_scratch)/num_samples_in_output to ring_buffer

	
};


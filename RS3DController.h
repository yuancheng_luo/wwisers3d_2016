#pragma once

#include <vector>
#include <map>
#include <string>

#include "AK/SoundEngine/Common/IAkPlugin.h"

#include "MixerStubFXParams.h"
#include "MixerStubFXAttachmentParams.h"

#include "RS3DAllocator.h"

#include "RS3DTailMgr.h"

#include "RS3DListener.h"
#include "RS3DSource.h"
#include "RS3DRoom.h"

#include "RS3DSpatializer.h"

#ifdef USE_BACCH
#include "RS3DBACCH.h"
#endif

#include "AkVectorExt.h"

#include "RS3DGameBlob.h"

#include "RS3DAmbisonics.h"

#include "RS3DCommon.h"


#define RS3DSourceMap std::map<RS3DSourceKey, RS3DSource*, std::less<RS3DSourceKey>, EXTERN_ALLOCATOR<std::pair<const RS3DSourceKey, RS3DSource*> > >
#define RS3DRoomMap std::map<RS3DRoomKey, RS3DRoom*, std::less<RS3DRoomKey>, EXTERN_ALLOCATOR<std::pair<const RS3DRoomKey, RS3DRoom*> > >
#define RS3DListenerMap std::map<RS3DListenerKey, RS3DListener*,std::less<RS3DListenerKey>, EXTERN_ALLOCATOR<std::pair<const RS3DListenerKey, RS3DListener*> > >
#define RS3DSpatializerMap std::map<RS3DSpatializerKey, RS3DSpatializer*, std::less<RS3DSpatializerKey>, EXTERN_ALLOCATOR<std::pair<const RS3DSpatializerKey, RS3DSpatializer*> > >

#define RS3DAmbisonicsMap std::map<RS3DSourceKey, RS3DAmbisonics*, std::less<RS3DSourceKey>, EXTERN_ALLOCATOR<std::pair<const RS3DSourceKey, RS3DAmbisonics*> > >


#ifdef USE_STATE_MAP
#define IAkMixerPluginContextKey uintptr_t
#define RS3DMixerInputContextStatesMap std::map<IAkMixerPluginContextKey, RS3DSpatializerKey*, std::less<IAkMixerPluginContextKey>, EXTERN_ALLOCATOR<std::pair<const IAkMixerPluginContextKey, RS3DSpatializerKey*> > >
#define RS3DMixerInputContextStatesAmbMap std::map<IAkMixerPluginContextKey, RS3DSourceKey*, std::less<IAkMixerPluginContextKey>, EXTERN_ALLOCATOR<std::pair<const IAkMixerPluginContextKey, RS3DSourceKey*> > >
#endif

//Interfaces with MixerStubFX
class RS3DController {
public:
	RS3DController();

	RS3DController(	AK::IAkMixerPluginContext *	_mixer_plugin_context,
					const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
					AkAudioFormat &	 _Ak_audio_format);
	~RS3DController();

	void process_voice(	AK::IAkMixerInputContext *	_mixer_input_context,
					const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
					const AkReal32 * input_buffer_mono,
					AkReal32 * output_buffer_left,
					AkReal32 * output_buffer_right);

	void process_ambisonics(AK::IAkMixerInputContext *	_mixer_input_context,
							const MixerStubFXParamsBus & _mixer_stub_FX_params_bus,
							const RS3DVector(const AkReal32 *) & input_buffer_list,
							AkReal32 * output_buffer_left,
							AkReal32 * output_buffer_right);

#ifdef USE_BACCH
	void process_stereo_bus_inplace(
						AkUniqueID bus_ID, int channel_left_ID, int channel_right_ID, BACCH_STEREO_TYPE BACCH_stereo_type,
						AkReal32 * input_output_buffer_left,
						AkReal32 * input_output_buffer_right);
#endif

	void destroy_source_room_spatializer(AK::IAkMixerInputContext *	_mixer_input_context);
	void destroy_ambisonics_source(AK::IAkMixerInputContext *	_mixer_input_context);

	AkVector get_prev_listener_source_ray();
	AkVector get_prev_listener_orientation();

private:
	AK::IAkMixerPluginContext *	mixer_plugin_context;
	AkAudioFormat Ak_audio_format;

	std::string license_key;

	MixerStubFXParamsBus mixer_stub_FX_params_bus;			
	_MixerStubFXAttachmentParams mixer_stub_FX_attachment_params;
	int num_samples_per_buffer;
	int sample_rate;

	//Common intermediate buffer
	RS3DFloatVector left_channel_output;
	RS3DFloatVector right_channel_output;

	
	AK::IAkMixerInputContext *	mixer_input_context;		//Last used input context, treat as a key
	AK::IAkVoicePluginInfo *	voice_info;					//Last used voice
	AkVector prev_listener_source_ray;
	AkVector prev_listener_orientation;
#ifdef USE_STATE_MAP
	RS3DMixerInputContextStatesMap RS3D_mixer_input_context_states_map;
	RS3DMixerInputContextStatesAmbMap RS3D_mixer_input_context_states_amb_map;
#endif
	//Maps of objects
	RS3DSourceMap RS3D_source_map;
	RS3DRoomMap RS3D_room_map;
	RS3DListenerMap RS3D_listener_map;
	RS3DSpatializerMap RS3D_spatializer_map;

	RS3DAmbisonicsMap RS3D_ambisonics_map;

	RS3DTailMgr *RS3D_tail_mgr;

	//Is voice in editor
	bool voice_in_editor;

	//Game-object intermediate
	RS3DGameBlob RS3D_game_blob;
	bool game_blob_initialized;

#ifdef USE_BACCH
	//BACCH System
	RS3DBACCH * RS3D_BACCH_FRONT_LEFT_RIGHT;
	RS3DBACCH * RS3D_BACCH_BACK_LEFT_RIGHT;
	RS3DBACCHBusRingBufferMap  RS3D_BACCH_bus_ring_buffer_map;

	void process_BACCH_system_stereo_bus(RS3DBACCHBusKey RS3D_BACCH_bus_key_left, RS3DBACCHBusKey RS3D_BACCH_bus_key_right, BACCH_STEREO_TYPE BACCH_stereo_type); //Overrides left_channel_output and right_channel_output with cross-cancellation filtered left and right outputs
#endif

	//Generate key based on current input context and voice
	RS3DSourceKey generate_source_key();
	RS3DListenerKey generate_listener_key();
	RS3DRoomKey generate_room_key();							
	RS3DSpatializerKey generate_spatializer_key(RS3DSourceKey  RS3D_source_key, RS3DRoomKey  RS3D_room_key, RS3DListenerKey  RS3D_listener_key);

	//Update objects in maps based on current input context and voice
	void update_source_map(RS3DSourceKey RS3D_source_key);
	void update_room_map(RS3DRoomKey  RS3D_room_key);
	void update_listener_map(RS3DListenerKey  RS3D_listener_key);

	void update_spatializer_map(	RS3DSourceKey  RS3D_source_key,
									RS3DRoomKey  RS3D_room_key,
									RS3DListenerKey  RS3D_listener_key,
									RS3DSpatializerKey RS3D_spatializer_key);

	void update_ambisonics_map(	int ambisonics_order,
								RS3DSourceKey  RS3D_source_key, 
								RS3DListenerKey  RS3D_listener_key);
	

	void process_params(AK::IAkMixerInputContext *	_mixer_input_context,	const MixerStubFXParamsBus & _mixer_stub_FX_params_bus);

	bool is_voice_in_editor();		//Use to determine if in editor
};



